#/bin/bash

#copiar archivos

ssh leu@sites.leu.ubiobio.cl pg_dump plamip -U postgres -f /tmp/dump.sql
scp leu@sites.leu.ubiobio.cl:/tmp/dump.sql /tmp/dump.sql

psql -U postgres -c "drop schema public cascade; create schema public" plamip
psql -U postgres -f /tmp/dump.sql plamip
