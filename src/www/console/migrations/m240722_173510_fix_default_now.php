<?php

use yii\db\Migration;

class m240722_173510_fix_default_now extends Migration
{
  public function safeUp()
  {
    $this->execute("
        ALTER TABLE cuestionario ALTER COLUMN fecha_creacion TYPE TIMESTAMP,
        ALTER COLUMN fecha_creacion SET DEFAULT NOW(),
        ALTER COLUMN fecha_creacion SET NOT NULL");
    $this->execute("
        ALTER TABLE respuestas ALTER COLUMN fecha_creacion TYPE TIMESTAMP,
        ALTER COLUMN fecha_creacion SET DEFAULT NOW(),
        ALTER COLUMN fecha_creacion SET NOT NULL");
    $this->execute("
        ALTER TABLE respuesta ALTER COLUMN fecha_creacion TYPE TIMESTAMP,
        ALTER COLUMN fecha_creacion SET DEFAULT NOW(),
        ALTER COLUMN fecha_creacion SET NOT NULL");
    $this->execute("
        ALTER TABLE respuesta_opcion ALTER COLUMN fecha_creacion TYPE TIMESTAMP,
        ALTER COLUMN fecha_creacion SET DEFAULT NOW(),
        ALTER COLUMN fecha_creacion SET NOT NULL");
    $this->execute("
        ALTER TABLE archivo ALTER COLUMN fecha_creacion TYPE TIMESTAMP,
        ALTER COLUMN fecha_creacion SET DEFAULT NOW(),
        ALTER COLUMN fecha_creacion SET NOT NULL");
    $this->execute("
        ALTER TABLE archivo ALTER COLUMN fecha_edicion TYPE TIMESTAMP,
        ALTER COLUMN fecha_edicion SET DEFAULT NOW(),
        ALTER COLUMN fecha_edicion SET NOT NULL");
    $this->execute("
        ALTER TABLE pregunta ALTER COLUMN fecha_creacion TYPE TIMESTAMP,
        ALTER COLUMN fecha_creacion SET DEFAULT NOW(),
        ALTER COLUMN fecha_creacion SET NOT NULL");
    $this->execute("
        ALTER TABLE opcion ALTER COLUMN fecha_creacion TYPE TIMESTAMP,
        ALTER COLUMN fecha_creacion SET DEFAULT NOW(),
        ALTER COLUMN fecha_creacion SET NOT NULL");
    $this->execute("
        ALTER TABLE seccion ALTER COLUMN fecha_creacion TYPE TIMESTAMP,
        ALTER COLUMN fecha_creacion SET DEFAULT NOW(),
        ALTER COLUMN fecha_creacion SET NOT NULL");
    $this->execute("
        ALTER TABLE tipo_parametro ALTER COLUMN fecha_creacion TYPE TIMESTAMP,
        ALTER COLUMN fecha_creacion SET DEFAULT NOW(),
        ALTER COLUMN fecha_creacion SET NOT NULL");
    $this->execute("
        ALTER TABLE parametro ALTER COLUMN fecha_creacion TYPE TIMESTAMP,
        ALTER COLUMN fecha_creacion SET DEFAULT NOW(),
        ALTER COLUMN fecha_creacion SET NOT NULL");
  }

  public function safeDown()
  {
    return true;
  }
}
