<?php

use common\models\Usuario;
use console\components\Console;
use yii\db\Migration;

class m230819_141401_usuario extends Migration
{
  public function safeUp()
  {
    $this->createTable('usuario', [
      'id' => $this->primaryKey(),
      'estado' => $this->smallInteger()->notNull()->defaultValue(1),
      'tipo' => $this->integer(),

      'nombre' => $this->string(32)->notNull(),
      'apellido' => $this->string(32),

      'usuario' => $this->string(32)->unique()->notNull(),
      'contrasena' => $this->string()->notNull(),
      'correo_electronico' => $this->string(254)->unique()->notNull(),

      'auth_key' => $this->string(32)->notNull(),

      'fecha_creacion' => $this->datetime()->notNull(),
      'fecha_edicion' => $this->datetime()->notNull(),
    ]);

    $usuario = new Usuario();
    $usuario->nombre = 'Bárbara';
    $usuario->apellido = 'Ojeda';
    $usuario->correo_electronico = 'ojedabar@gmail.com';
    $usuario->contrasena = '1!2"3#';
    $usuario->estado = Usuario::ESTADO_ACTIVO;
    $usuario->tipo = Usuario::ADMIN;
    if ($usuario->save()) {
      echo "    > Usuario ojedabar@gmail.com/1!2\"3# creado exitosamente\n";
    } else {
      echo "    > No se pudo crear el usuario admin";
      Console::dump($usuario->errors);
      return false;
    }
  }

  public function safeDown()
  {
    $this->dropTable('usuario');
  }
}
