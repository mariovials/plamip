<?php

use yii\db\Migration;

class m240524_050914_resultados extends Migration
{
  public function safeUp()
  {
    $this->createTable('rango', [
      'id' => $this->primaryKey(),
      'tipo_parametro_id' => $this->integer()->notNull(),
      'nombre' => $this->string(64),
      'desde' => $this->decimal(5, 2)->notNull()->defaultValue(0), // 100.00
      'hasta' => $this->decimal(5, 2)->notNull()->defaultValue(0), // 100.00
    ]);
    // agrega clave foranea en rango.tipo_parametro_id hacia tipo_parametro.id
    $this->addForeignKey (
      'fk-rango-tipo_parametro_id-tipo_parametro-id',
      'rango',
      'tipo_parametro_id',
      'tipo_parametro',
      'id',
      'CASCADE', // si se borra tipo_parametro
      'CASCADE');

    $this->createTable('parametro_rango', [
      'id' => $this->primaryKey(),
      'parametro_id' => $this->integer()->notNull(),
      'rango_id' => $this->integer()->notNull(),
      'texto' => $this->text(),
    ]);
    // agrega clave foranea en parametro_rango.rango_id hacia rango.id
    $this->addForeignKey (
      'fk-parametro_rango-rango_id-rango-id',
      'parametro_rango',
      'rango_id',
      'rango',
      'id',
      'CASCADE', // si se borra rango
      'CASCADE');
    // agrega clave foranea en parametro_rango.parametro_id hacia parametro.id
    $this->addForeignKey (
      'fk-parametro_rango-parametro_id-parametro-id',
      'parametro_rango',
      'parametro_id',
      'parametro',
      'id',
      'CASCADE', // si se borra parametro
      'CASCADE');
  }

  public function safeDown()
  {
    $this->dropTable('parametro_rango');
    $this->dropTable('rango');
  }
}
