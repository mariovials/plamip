<?php

use yii\db\Migration;

/**
 * Class m240806_172120_institucion
 */
class m240806_172120_institucion extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('usuario', 'apellido', 'institucion');
        $this->alterColumn('usuario', 'nombre', $this->string(256));
        $this->alterColumn('usuario', 'institucion', $this->string(256));
        $this->execute("UPDATE usuario SET nombre = nombre || ' ' || institucion WHERE tipo = 1");
        $this->execute("UPDATE usuario SET institucion = null WHERE tipo = 1");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('usuario', 'institucion', 'apellido');
        $this->alterColumn('usuario', 'nombre', $this->string(32));
        $this->alterColumn('usuario', 'apellido', $this->string(32));
    }
}
