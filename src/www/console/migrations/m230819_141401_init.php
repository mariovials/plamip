<?php

use yii\db\Migration;

class m230819_141401_init extends Migration
{
  public function safeUp()
  {
    $dbName = getDbName(Yii::$app->db->dsn);
    $this->execute("ALTER DATABASE $dbName SET datestyle TO 'ISO, YMD'");
  }

  public function safeDown()
  {
    $dbName = getDbName(Yii::$app->db->dsn);
    $this->execute("ALTER DATABASE $dbName SET datestyle TO 'ISO, MDY'");
  }
}

function getDbName($dsn) {
  if (preg_match('/dbname=([^;]*)/', $dsn, $match)) {
    return $match[1];
  } else {
    return null;
  }
}
