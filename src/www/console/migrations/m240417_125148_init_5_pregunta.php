<?php

use yii\db\Migration;

/**
 * Class m240417_125148_init
 */
class m240417_125148_init_5_pregunta extends Migration
{

  public function safeUp()
  {
    $this->createTable('pregunta', [
      'id'=> $this->primaryKey(),
      'cuestionario_id' => $this->integer()->notNull(),
      'seccion_id' => $this->integer(),

      'orden' => $this->integer()->notNull()->defaultValue(1),

      'nombre' => $this->text()->notNull(),
      'numero' => $this->string(5),
      'descripcion' => $this->text(),
      'ayuda' => $this->text(),

      'tipo' => $this->integer()->notNull()->defaultValue(1),
      'multiple' => $this->boolean(),
      'numerico' => $this->boolean(),

      'estado' => $this->integer(),
      'puntaje' => $this->decimal(5, 2)->notNull()->defaultValue(0), // 100.00

      'dependencia' => $this->integer(),

      'fecha_creacion' => $this->dateTime()->notNull()->defaultExpression('NOW()'),
      'fecha_edicion' => $this->dateTime(),
    ]);

    // agrega clave foranea en pregunta.cuestionario_id hacia cuestionario.id
    $this->addForeignKey (
      'fk-pregunta-cuestionario_id-cuestionario-id',
      'pregunta',
      'cuestionario_id',
      'cuestionario',
      'id',
      'CASCADE', // si se borra cuestionario
      'CASCADE');

  }

  public function safeDown()
  {
    $this->dropTable('pregunta');
  }
}
