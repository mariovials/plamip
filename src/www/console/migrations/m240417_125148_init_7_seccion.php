<?php

use yii\db\Migration;

/**
 * Class m240417_125148_init
 */
class m240417_125148_init_7_seccion extends Migration
{

  public function safeUp()
  {
    $this->createTable('seccion', [
      'id'=> $this->primaryKey(),
      'cuestionario_id' => $this->integer()->notNull(),

      'orden' => $this->integer()->notNull()->defaultValue(1),

      'nombre' => $this->text(),
      'descripcion' => $this->text(),

      'fecha_creacion' => $this->dateTime()->notNull()->defaultExpression('NOW()'),
      'fecha_edicion' => $this->dateTime(),
    ]);

    // agrega clave foranea en pregunta.seccion_id hacia seccion.id
    $this->addForeignKey (
      'fk-pregunta-seccion_id-seccion-id',
      'pregunta',
      'seccion_id',
      'seccion',
      'id',
      'RESTRICT', // si se borra seccion
      'CASCADE');
  }

  public function safeDown()
  {
    $this->execute('DROP TABLE seccion CASCADE');
  }
}
