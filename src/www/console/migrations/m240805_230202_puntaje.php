<?php

use yii\db\Migration;

/**
 * Class m240805_230202_puntaje
 */
class m240805_230202_puntaje extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('respuestas', 'puntaje', $this->decimal(5, 2));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('respuestas', 'puntaje');
    }
}
