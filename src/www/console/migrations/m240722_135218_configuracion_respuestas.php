<?php

use yii\db\Migration;

/**
 * Class m240722_135218_configuracion_respuestas
 */
class m240722_135218_configuracion_respuestas extends Migration
{
  public function safeUp()
  {
    $this->addColumn('cuestionario', 'respuesta_unica', $this->boolean());
    $this->addColumn('cuestionario', 'cerrado_para_edicion', $this->boolean());
    $this->addColumn('cuestionario', 'cerrado_para_respuestas', $this->boolean());

    $this->addColumn('cuestionario', 'descripcion_larga', $this->text());

    $this->dropIndex(
      'respuestas-cuestionario_id-usuario_id-index',
      'respuestas');


  }

  public function safeDown()
  {
    $this->dropColumn('cuestionario', 'respuesta_unica');
    $this->dropColumn('cuestionario', 'cerrado_para_edicion');
    $this->dropColumn('cuestionario', 'cerrado_para_respuestas');

    $this->dropColumn('cuestionario', 'descripcion_larga');

    $this->createIndex(
      'respuestas-cuestionario_id-usuario_id-index',
      'respuestas',
      ['cuestionario_id', 'usuario_id'],
      true); // unique
  }
}
