<?php

use yii\db\Migration;

/**
 * Class m240624_210311_resultado
 */
class m240624_210311_resultado extends Migration
{

  public function safeUp()
  {
    $this->createTable('resultado', [
      'id' => $this->primaryKey(),
      'cuestionario_id' => $this->integer()->notNull(),
      'nombre' => $this->string(64),
      'desde' => $this->decimal(5, 2)->notNull()->defaultValue(0), // 100.00
      'hasta' => $this->decimal(5, 2)->notNull()->defaultValue(0), // 100.00
      'texto' => $this->text(),
    ]);

  }

  public function safeDown()
  {
    $this->dropTable('resultado');
  }
}
