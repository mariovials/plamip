<?php

use yii\db\Migration;

/**
 * Class m240806_184617_rango_texto
 */
class m240806_184617_rango_texto extends Migration
{

  public function safeUp()
  {
    $this->addColumn('rango', 'texto', $this->text());
    $this->addColumn('resultado', 'texto_promedio', $this->text());
  }

  public function safeDown()
  {
    $this->dropColumn('rango', 'texto');
    $this->dropColumn('resultado', 'texto_promedio');
  }
}
