<?php

use yii\db\Migration;

class m240807_200806_puntaje_parametro extends Migration
{
  public function safeUp()
  {
    $this->createTable('puntaje_parametro', [
      'id' => $this->primaryKey(),
      'respuestas_id' => $this->integer(),
      'parametro_id' => $this->integer(),
      'puntaje' => $this->decimal(5, 2),
      'maximo' => $this->decimal(5, 2),
    ]);

    // agrega clave foranea en puntaje_parametro.respuestas_id hacia respuestas.id
    $this->addForeignKey (
      'fk-puntaje_parametro-respuestas_id-respuestas-id',
      'puntaje_parametro',
      'respuestas_id',
      'respuestas',
      'id',
      'CASCADE', // si se borra respuestas
      'CASCADE');

    // agrega clave foranea en puntaje_parametro.parametro_id hacia parametro.id
    $this->addForeignKey (
      'fk-puntaje_parametro-parametro_id-parametro-id',
      'puntaje_parametro',
      'parametro_id',
      'parametro',
      'id',
      'RESTRICT', // si se borra parametro
      'CASCADE');

    $this->createIndex(
      'puntaje_parametro-respuestas_id-parametro_id-index',
      'puntaje_parametro',
      ['respuestas_id', 'parametro_id'],
      true); // unique


  }

  public function safeDown()
  {
    $this->dropTable('puntaje_parametro');
  }
}
