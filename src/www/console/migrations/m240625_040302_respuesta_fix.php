<?php

use yii\db\Migration;

/**
 * Class m240625_040302_respuesta_fix
 */
class m240625_040302_respuesta_fix extends Migration
{
  public function safeUp()
  {
    $this->alterColumn('respuesta', 'valor', $this->text());
  }

  public function safeDown()
  {
    $this->alterColumn('respuesta', 'valor', $this->string(255));
  }
}
