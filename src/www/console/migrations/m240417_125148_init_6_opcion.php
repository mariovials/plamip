<?php

use yii\db\Migration;

/**
 * Class m240417_125148_init
 */
class m240417_125148_init_6_opcion extends Migration
{

  public function safeUp()
  {
    $this->createTable('opcion', [
      'id'=> $this->primaryKey(),
      'pregunta_id' => $this->integer()->notNull(),

      'orden' => $this->integer()->notNull()->defaultValue(1),

      'nombre' => $this->text(),
      'descripcion' => $this->text(),
      'editable' => $this->boolean(),
      'numero' => $this->boolean(),
      'puntaje' => $this->decimal(5, 2)->notNull()->defaultValue(0), // 100.00

      'fecha_creacion' => $this->dateTime()->notNull()->defaultExpression('NOW()'),
      'fecha_edicion' => $this->dateTime(),
    ]);

    // agrega clave foranea en opcion.pregunta_id hacia pregunta.id
    $this->addForeignKey (
      'fk-opcion-pregunta_id-pregunta-id',
      'opcion',
      'pregunta_id',
      'pregunta',
      'id',
      'CASCADE', // si se borra pregunta
      'CASCADE');

    $this->createIndex(
      'opcion-pregunta_id-orden-index',
      'opcion',
      ['pregunta_id', 'orden'],
      true); // unique

  }

  public function safeDown()
  {
    $this->dropTable('opcion');
  }
}
