<?php

use yii\db\Migration;

/**
 * Class m240515_194520_pregunta_parametro
 */
class m240515_194520_pregunta_parametro extends Migration
{
  public function safeUp()
  {
    $this->createTable('pregunta_parametro', [
      'id' => $this->primaryKey(),
      'pregunta_id' => $this->integer()->notNull(),
      'parametro_id' => $this->integer()->notNull(),
    ]);

    $this->createIndex(
      'pregunta_parametro-pregunta_id-parametro_id-index',
      'pregunta_parametro',
      ['pregunta_id', 'parametro_id'],
      true); // unique

    // agrega clave foranea en pregunta_parametro.pregunta_id hacia pregunta.id
    $this->addForeignKey (
      'fk-pregunta_parametro-pregunta_id-pregunta-id',
      'pregunta_parametro',
      'pregunta_id',
      'pregunta',
      'id',
      'CASCADE', // si se borra pregunta
      'CASCADE');

    // agrega clave foranea en pregunta_parametro.parametro_id hacia parametro.id
    $this->addForeignKey (
      'fk-pregunta_parametro-parametro_id-parametro-id',
      'pregunta_parametro',
      'parametro_id',
      'parametro',
      'id',
      'CASCADE', // si se borra parametro
      'CASCADE');
  }

  public function safeDown()
  {
    $this->dropTable('pregunta_parametro');
  }
}
