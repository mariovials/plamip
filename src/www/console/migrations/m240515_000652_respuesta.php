<?php

use yii\db\Migration;

class m240515_000652_respuesta extends Migration
{
  public function safeUp()
  {
    $this->createTable('respuestas', [
      'id' => $this->primaryKey(),
      'cuestionario_id' => $this->integer(),
      'usuario_id' => $this->integer()->notNull(),

      'fecha_creacion' => $this->dateTime()->notNull()->defaultExpression('NOW()'),
      'fecha_edicion' => $this->dateTime(),
      'fecha_termino' => $this->dateTime(),
    ]);
    // agrega clave foranea en respuestas.cuestionario_id hacia usuario.id
    $this->addForeignKey (
      'fk-respuestas-cuestionario_id-cuestionario-id',
      'respuestas',
      'cuestionario_id',
      'cuestionario',
      'id',
      'CASCADE', // si se borra cuestionario
      'CASCADE');
    // agrega clave foranea en respuestas.usuario_id hacia usuario.id
    $this->addForeignKey (
      'fk-respuestas-usuario_id-usuario-id',
      'respuestas',
      'usuario_id',
      'usuario',
      'id',
      'RESTRICT', // si se borra usuario
      'CASCADE');
    $this->createIndex(
      'respuestas-cuestionario_id-usuario_id-index',
      'respuestas',
      ['cuestionario_id', 'usuario_id'],
      true); // unique

    $this->createTable('respuesta', [
      'id' => $this->primaryKey(),
      'respuestas_id' => $this->integer(),
      'pregunta_id' => $this->integer(),
      'orden' => $this->integer()->notNull()->defaultValue(1),
      'puntaje' => $this->decimal(5, 2)->notNull()->defaultValue(0), // 100.00
      'valor' => $this->string(),

      'fecha_creacion' => $this->dateTime()->notNull()->defaultExpression('NOW()'),
      'fecha_edicion' => $this->dateTime(),
    ]);
    // agrega clave foranea en respuesta.respuestas_id hacia respuestas.id
    $this->addForeignKey (
      'fk-respuesta-respuestas_id-respuestas-id',
      'respuesta',
      'respuestas_id',
      'respuestas',
      'id',
      'CASCADE', // si se borra respuestas
      'CASCADE');
    // agrega clave foranea en respuesta.pregunta_id hacia pregunta.id
    $this->addForeignKey (
      'fk-respuesta-pregunta_id-pregunta-id',
      'respuesta',
      'pregunta_id',
      'pregunta',
      'id',
      'CASCADE', // si se borra pregunta
      'CASCADE');
    $this->createIndex(
      'respuesta-respuestas_id-pregunta_id-index',
      'respuesta',
      ['respuestas_id', 'pregunta_id'],
      true); // unique

    $this->createTable('respuesta_opcion', [
      'id' => $this->primaryKey(),
      'respuesta_id' => $this->integer(),
      'opcion_id' => $this->integer(),
      'orden' => $this->integer()->notNull()->defaultValue(1),
      'puntaje' => $this->decimal(5, 2)->notNull()->defaultValue(0), // 100.00
      'valor' => $this->string(),

      'fecha_creacion' => $this->dateTime()->notNull()->defaultExpression('NOW()'),
      'fecha_edicion' => $this->dateTime(),
    ]);
    // agrega clave foranea en respuesta_opcion.respuesta_id hacia respuesta.id
    $this->addForeignKey (
      'fk-respuesta_opcion-respuesta_id-respuesta-id',
      'respuesta_opcion',
      'respuesta_id',
      'respuesta',
      'id',
      'CASCADE', // si se borra respuesta
      'CASCADE');
    // agrega clave foranea en respuesta_opcion.opcion_id hacia opcion.id
    $this->addForeignKey (
      'fk-respuesta_opcion-opcion_id-opcion-id',
      'respuesta_opcion',
      'opcion_id',
      'opcion',
      'id',
      'CASCADE', // si se borra opcion
      'CASCADE');
    $this->createIndex(
      'respuesta_opcion-respuesta_id-opcion_id-index',
      'respuesta_opcion',
      ['respuesta_id', 'opcion_id'],
      true); // unique
  }

  public function safeDown()
  {
    $this->dropTable('respuesta_opcion');
    $this->dropTable('respuesta');
    $this->dropTable('respuestas');
  }
}
