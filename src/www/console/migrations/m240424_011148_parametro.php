<?php

use yii\db\Migration;

/**
 * Class m240424_011148_parametro
 */
class m240424_011148_parametro extends Migration
{
  public function safeUp()
  {
    $this->createTable('tipo_parametro', [
      'id' => $this->primaryKey(),
      'cuestionario_id' => $this->integer()->notNull(),
      'tipo_parametro_id' => $this->integer(),
      'parametro_id' => $this->integer(),

      'comun' => $this->boolean()->notNull()->defaultValue(false),

      'nombre' => $this->string(64)->notNull(),
      'descripcion' => $this->string(512),

      'puntaje_maximo' => $this->decimal(5,2), // 100.00

      'color' => $this->string(7)->notNull()->defaultValue('#607d8b'),
      'icono' => $this->string(64)->notNull()->defaultValue('chart-donut'),

      'fecha_creacion' => $this->dateTime()->notNull()->defaultExpression('NOW()'),
      'fecha_edicion' => $this->dateTime(),
    ]);
    // agrega clave foranea en tipo_parametro.cuestionario_id hacia cuestionario.id
    $this->addForeignKey (
      'fk-tipo_parametro-cuestionario_id-cuestionario-id',
      'tipo_parametro',
      'cuestionario_id',
      'cuestionario',
      'id',
      'CASCADE', // si se borra cuestionario
      'CASCADE');
    // agrega clave foranea en tipo_parametro.tipo_parametro_id hacia tipo_parametro.id
    $this->addForeignKey (
      'fk-tipo_parametro-tipo_parametro_id-tipo_parametro-id',
      'tipo_parametro',
      'tipo_parametro_id',
      'tipo_parametro',
      'id',
      'CASCADE', // si se borra tipo_parametro
      'CASCADE');

    $this->createTable('parametro', [
      'id' => $this->primaryKey(),
      'tipo_parametro_id' => $this->integer()->notNull(),
      'parametro_id' => $this->integer(),

      'nombre' => $this->string(64)->notNull(),
      'descripcion' => $this->string(512),

      'puntaje_maximo' => $this->decimal(5,2), // 100.00

      'color' => $this->string(7)->notNull()->defaultValue('#607d8b'),
      'icono' => $this->string(64)->notNull()->defaultValue('chart-donut'),

      'fecha_creacion' => $this->dateTime()->notNull()->defaultExpression('NOW()'),
      'fecha_edicion' => $this->dateTime(),
    ]);
    // agrega clave foranea en parametro.tipo_parametro_id hacia tipo_parametro.id
    $this->addForeignKey (
      'fk-parametro-tipo_parametro_id-tipo_parametro-id',
      'parametro',
      'tipo_parametro_id',
      'tipo_parametro',
      'id',
      'CASCADE', // si se borra tipo_parametro
      'CASCADE');
    // agrega clave foranea en parametro.parametro_id hacia parametro.id
    $this->addForeignKey (
      'fk-parametro-parametro_id-parametro-id',
      'parametro',
      'parametro_id',
      'parametro',
      'id',
      'CASCADE', // si se borra parametro
      'CASCADE');

    // agrega clave foranea en tipo_parametro_id.parametro_id hacia parametro.id
    $this->addForeignKey (
      'fk-tipo_parametro-parametro_id-parametro-id',
      'tipo_parametro',
      'parametro_id',
      'parametro',
      'id',
      'CASCADE', // si se borra parametro
      'CASCADE');


  }

  public function safeDown()
  {
    $this->dropForeignKey ('fk-tipo_parametro-cuestionario_id-cuestionario-id', 'tipo_parametro');
    $this->dropForeignKey ('fk-tipo_parametro-tipo_parametro_id-tipo_parametro-id', 'tipo_parametro');
    $this->dropForeignKey ('fk-tipo_parametro-parametro_id-parametro-id', 'tipo_parametro');
    $this->dropForeignKey ('fk-parametro-tipo_parametro_id-tipo_parametro-id', 'parametro');
    $this->dropForeignKey ('fk-parametro-parametro_id-parametro-id', 'parametro');

    $this->dropTable('tipo_parametro');
    $this->dropTable('parametro');
  }
}
