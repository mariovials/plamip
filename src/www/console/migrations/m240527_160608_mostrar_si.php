<?php

use yii\db\Migration;

/**
 * Class m240527_160608_mostrar_si
 */
class m240527_160608_mostrar_si extends Migration
{
  public function safeUp()
  {
    $this->createTable('mostrar', [
      'id' => $this->primaryKey(),
      'opcion_id' => $this->integer()->notNull(),
      'pregunta_id' => $this->integer()->notNull(),
    ]);

    $this->createIndex(
      'mostrar-opcion_id-pregunta_id-index',
      'mostrar',
      ['opcion_id', 'pregunta_id'],
      true); // unique

    // agrega clave foranea en mostrar.opcion_id hacia opcion.id
    $this->addForeignKey (
      'fk-mostrar-opcion_id-opcion-id',
      'mostrar',
      'opcion_id',
      'opcion',
      'id',
      'CASCADE', // si se borra opcion
      'CASCADE');

    // agrega clave foranea en mostrar.pregunta_id hacia pregunta.id
    $this->addForeignKey (
      'fk-mostrar-pregunta_id-pregunta-id',
      'mostrar',
      'pregunta_id',
      'pregunta',
      'id',
      'CASCADE', // si se borra pregunta
      'CASCADE');

  }

  public function safeDown()
  {
    $this->dropTable('mostrars');
  }
}
