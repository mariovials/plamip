<?php

use yii\db\Migration;

/**
 * Class m240417_125148_init
 */
class m240417_125148_init_1_cuestionario extends Migration
{

  public function safeUp()
  {
    $this->createTable('cuestionario', [
      'id'=> $this->primaryKey(),

      'nombre' => $this->string(64)->notNull(),
      'tipo' => $this->integer(),
      'descripcion' => $this->string(512),

      'fecha_creacion' => $this->dateTime()->notNull()->defaultExpression('NOW()'),
      'fecha_edicion' => $this->dateTime(),
    ]);

  }

  public function safeDown()
  {
    $this->dropTable('cuestionario');
  }
}
