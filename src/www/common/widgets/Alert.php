<?php

namespace common\widgets;

use Yii;
use common\helpers\Html;
use common\assets\AlertAsset;
use \yii\base\InvalidConfigException;

/**
 * @author Mario Vial <mvial@ubiobio.cl> 2024/07/11 22:51
 */
class Alert extends \yii\base\Widget
{
  public $tipos = [
    'success',
    'info',
    'warning',
    'error',
  ];

  public function init()
  {
    parent::init();
    $view = $this->getView();
    AlertAsset::register($view);
    $view->registerJs("$('#flashes li .cerrar').on('click', function() {
      $(this).parent().remove()
    })");
  }

  public function run()
  {
    $flashes = Yii::$app->session->getAllFlashes();

    if ($flashes) {

      echo Html::beginTag('ul', ['id' => 'flashes']);

      foreach ($flashes as $type => $flash) {

        if (!in_array($type, $this->tipos)) {
          throw new InvalidConfigException("Tipo de alerta '$type' no es válido");
        }

        foreach ($flash as $message) {
          $mdi = Html::tag('i', '', ['class' => 'mdi mdi-close']);
          $boton = Html::tag('button', $mdi, ['class' => 'cerrar']);
          echo Html::tag('li', $message . $boton, ['class' => $type]);
        }

      }

      echo Html::endTag('ul');
    }

  }
}
