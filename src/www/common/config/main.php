<?php
return [
  'language' => 'es',
  'sourceLanguage' => 'es-CL',
  'timeZone' => 'America/Santiago',
  'aliases' => [
    '@bower' => '@vendor/bower-asset',
    '@npm'   => '@vendor/npm-asset',
  ],
  'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
  'components' => yii\helpers\ArrayHelper::merge([

    'cache' => yii\caching\FileCache::class,

    'user' => [
      'class' => frontend\components\User::class,
      'identityClass' => common\models\Usuario::class,
      'loginUrl' => '/plamip/sistema/ingresar',
      'enableAutoLogin' => true,
      'identityCookie' => [
        'name' => '_identity',
        'httpOnly' => true,
      ],
    ],

    'formatter' => [
      'class' => common\components\Formatter::class,
      'timeZone' => 'America/Santiago',
      'defaultTimeZone' => 'America/Santiago',
      'dateFormat' => 'dd/MM/yyyy',
      'datetimeFormat' => 'dd/MM/yyyy HH:mm:ss',
      'decimalSeparator' => ',',
      'thousandSeparator' => '.',
      'currencyCode' => 'CLP',
    ],

    'i18n' => [
      'translations' => [
        'app' => [
          'class' => yii\i18n\PhpMessageSource::class,
          'basePath' => '@common/messages',
        ],
      ],
    ],

    'log' => [
      'traceLevel' => YII_DEBUG ? 3 : 0,
      'targets' => [
        [
          'class' => \yii\log\FileTarget::class,
          'levels' => ['error', 'warning'],
        ],
      ],
    ],
  ],

  require __DIR__ . '/urlsManager.php',
)];
