<?php
/**
 * FRONTEND_HOST http://molecvet.local
 * BACKEND_HOST  http://admin.molecvet.local
 * /src/www/common/config/bootstrap-local.php
 */

$commonRules = [
  '<controller>/<id:\d+>' => '<controller>/ver',
  '<controller>/<id:\d+>/<action>' => '<controller>/<action>',
  '<module>/<controller>/<id:\d+>' => '<module>/<controller>/ver',
  '<module>/<controller>/<id:\d+>/<action>' => '<module>/<controller>/<action>',
];
$urlsManager = [
  'frontend' => [
    'class' => 'yii\web\UrlManager',
    'baseUrl' => '/plamip', // para que funcione con alias /admin
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'rules' => array_merge([
    ], $commonRules),
  ],
  'backend' => [
    'class' => 'yii\web\UrlManager',
    'baseUrl' => '/plamip/admin', // para que funcione en alias /admin
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'rules' =>  array_merge([
    ], $commonRules),
  ],
];
$componentsUrlsManager = [];
foreach ($urlsManager as $id => $urlManager) {
  $componentsUrlsManager['urlManager' . ucfirst($id)] = $urlManager;
  if (ID_APPLICATION == $id) {
    $componentsUrlsManager['urlManager'] = $urlManager;
  }
}
return $componentsUrlsManager;
