<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * @author Mario Vial <mvial@ubiobio.cl> 2024/07/11 23:19
 */
class AlertAsset extends AssetBundle
{
  public $publishOptions = [
    'forceCopy' => YII_ENV === 'dev',
  ];
  public $sourcePath = '@common/resources/css';
  public $css = [
    'alert.css?v=1',
  ];
}
