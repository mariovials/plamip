<?php
namespace common\helpers;
/**
 * Html
 */
class Html extends \yii\helpers\Html
{
  public static function activeCheckbox($model, $attribute, $options = [])
  {
    return static::activeBooleanInput('checkbox', $model, $attribute, $options);
  }

  public static function activeRadioList($model, $attribute, $items, $options = [])
  {
    return static::activeListInput('radioList', $model, $attribute, $items, $options);
  }

  public static function radio($name, $checked = false, $options = [])
  {
    return static::booleanInput('radio', $name, $checked, $options);
  }

  protected static function booleanInput($type, $name, $checked = false, $options = [])
  {
    // 'checked' option has priority over $checked argument
    if (!isset($options['checked'])) {
      $options['checked'] = (bool) $checked;
    }
    $value = array_key_exists('value', $options) ? $options['value'] : '1';
    if (isset($options['uncheck'])) {
      // add a hidden field so that if the checkbox is not selected, it still submits a value
      $hiddenOptions = [];
      if (isset($options['form'])) {
        $hiddenOptions['form'] = $options['form'];
      }
      // make sure disabled input is not sending any value
      if (!empty($options['disabled'])) {
        $hiddenOptions['disabled'] = $options['disabled'];
      }
      $hidden = static::hiddenInput($name, $options['uncheck'], $hiddenOptions);
      unset($options['uncheck']);
    } else {
      $hidden = '';
    }
    $options['class'][] = 'with-gap';
    if (isset($options['label'])) {
      if ($type == 'radio') {
        $label = Html::tag('span', $options['label']);
      } else {
        $label =  $options['label'];
      }
      $labelOptions = isset($options['labelOptions']) ? $options['labelOptions'] : [];
      unset($options['label'], $options['labelOptions']);
      $content = static::label(static::input($type, $name, $value, $options) . ' ' . $label, null, $labelOptions);
      return $hidden . $content;
    }
    return $hidden . static::input($type, $name, $value, $options);
  }

  protected static function activeListInput($type, $model, $attribute, $items, $options = [])
  {
    $name = ArrayHelper::remove($options, 'name', static::getInputName($model, $attribute));
    $selection = ArrayHelper::remove($options, 'value', static::getAttributeValue($model, $attribute));
    if (!array_key_exists('unselect', $options)) {
      $options['unselect'] = '';
    }
    if (!array_key_exists('id', $options)) {
      $options['id'] = static::getInputId($model, $attribute);
    }
    return static::$type($name, $selection, $items, $options);
  }

  public static function radioList($name, $selection = null, $items = [], $options = [])
  {
    if (ArrayHelper::isTraversable($selection)) {
      $selection = array_map('strval', ArrayHelper::toArray($selection));
    }
    $formatter = ArrayHelper::remove($options, 'item');
    $itemOptions = ArrayHelper::remove($options, 'itemOptions', []);
    $encode = ArrayHelper::remove($options, 'encode', true);
    $separator = ArrayHelper::remove($options, 'separator', "\n");
    $tag = ArrayHelper::remove($options, 'tag', 'div');
    $strict = ArrayHelper::remove($options, 'strict', false);

    $hidden = '';
    if (isset($options['unselect'])) {
      // add a hidden field so that if the list box has no option being selected, it still submits a value
      $hiddenOptions = [];
      // make sure disabled input is not sending any value
      if (!empty($options['disabled'])) {
        $hiddenOptions['disabled'] = $options['disabled'];
      }
      $hidden =  static::hiddenInput($name, $options['unselect'], $hiddenOptions);
      unset($options['unselect'], $options['disabled']);
    }

    $lines = [];
    $index = 0;
    foreach ($items as $value => $label) {
      $checked = $selection !== null &&
        (!ArrayHelper::isTraversable($selection) && !strcmp($value, $selection)
          || ArrayHelper::isTraversable($selection) && ArrayHelper::isIn((string)$value, $selection, $strict));
      if ($formatter !== null) {
        $lines[] = call_user_func($formatter, $index, $label, $name, $checked, $value);
      } else {
        $lines[] = static::radio($name, $checked, array_merge([
          'value' => $value,
          'label' => $encode ? static::encode($label) : $label,
        ], $itemOptions));
      }
      $index++;
    }
    $visibleContent = implode($separator, $lines);

    if ($tag === false) {
      return $hidden . $visibleContent;
    }
    $options['role'] = 'radiogroup';
    return $hidden . static::tag($tag, $visibleContent, $options);
  }

  protected static function activeBooleanInput($type, $model, $attribute, $options = [])
  {
    $name = isset($options['name']) ? $options['name'] : static::getInputName($model, $attribute);
    $value = static::getAttributeValue($model, $attribute);

    if (!array_key_exists('value', $options)) {
      $options['value'] = '1';
    }
    if (!array_key_exists('uncheck', $options)) {
      $options['uncheck'] = '0';
    } elseif ($options['uncheck'] === false) {
      unset($options['uncheck']);
    }
    if (!array_key_exists('label', $options)) {
      $options['label'] = static::encode($model->getAttributeLabel(static::getAttributeName($attribute)));
    } elseif ($options['label'] === false) {
      unset($options['label']);
    }
    $options['label'] = '<div class="box"></div>'
      . '<div class="label">' . $options['label'] . '</div>';
    $checked = "$value" === "{$options['value']}";
    if (!array_key_exists('id', $options)) {
      $options['id'] = static::getInputId($model, $attribute);
    }
    return static::$type($name, $checked, $options);
  }


  public static function activeDatalist($model, $attribute, $items, $options = [])
  {
    if (!array_key_exists('id', $options)) {
      $options['id'] = static::getInputId($model, $attribute);
    }
    if (!array_key_exists('list', $options)) {
      $options['list'] = $options['id'] . '-list';
    }
    $input = static::input('text', static::getInputName($model, $attribute), $model->$attribute, $options);
    $datalist_options = '';
    foreach ($items as $value => $text) {
      $datalist_options .= Html::tag('option', null, ['value' => $text]);
    }
    // @todo hacer datalist no active
    return $input
      . '<datalist id="' . $options['list'] . '">'
      . $datalist_options
      . '</datalist>';
  }

}
