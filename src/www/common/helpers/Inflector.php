<?php

/**
 * @link https://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license https://www.yiiframework.com/license/
 */

namespace common\helpers;

use Yii;

/**
 * Inflector en español
 * @author Mario Vial <mvial@ubiobio.cl> 2024/04/25 11:25
 */
class Inflector extends \yii\helpers\Inflector
{
  /**
   * @var array the rules for converting a word into its plural form.
   * The keys are the regular expressions and the values are the corresponding replacements.
   */
  public static $plurals = [
    '/(.*)(\d+$)/i' => '\1\2',
    '/(.*)ón$/i' => '\1ones',
    '/^([a|i|o|u])$/i' => '\1es',
    '/^([e])$/i' => '\1ses',
    '/(.*[a|e|o|u])$/i' => '\1s',
    '/(.*[i])$/i' => '\1s',
    '/^(\w)$/i' => '\1s',
    '/$\w+/' => '\1es',
    '/$/' => 's',
  ];

  /**
   * @var array the rules for converting a word into its singular form.
   * The keys are the regular expressions and the values are the corresponding replacements.
   */
  public static $singulars = [
  ];

  /**
   * @var array the special rules for converting a word between its plural form and singular form.
   * The keys are the special words in singular form, and the values are the corresponding plural form.
   */
  public static $specials = [
  ];
}
