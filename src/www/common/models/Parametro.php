<?php

namespace common\models;

use backend\components\ActiveRecord;

class Parametro extends ActiveRecord
{

  public static function tableName()
  {
    return 'parametro';
  }

  public function rules()
  {
    return [
      [['nombre', 'puntaje_maximo', 'color'], 'required'],
      [['tipo_parametro_id', 'parametro_id'], 'default', 'value' => null],
      [['tipo_parametro_id', 'parametro_id'], 'integer'],
      [['puntaje_maximo'], 'number'],
      [['fecha_creacion', 'fecha_edicion'], 'safe'],
      [['nombre', 'icono'], 'string', 'max' => 64],
      [['color'], 'string', 'max' => 7],
      [['descripcion'], 'string', 'max' => 512],
      [['tipo_parametro_id'], 'exist', 'skipOnError' => true,
        'targetClass' => TipoParametro::class,
        'targetAttribute' => ['tipo_parametro_id' => 'id']],
      [['parametro_id'], 'exist', 'skipOnError' => true,
        'targetClass' => Parametro::class,
        'targetAttribute' => ['parametro_id' => 'id']],
      [['color'], 'match', 'pattern' => '/^#(?:[0-9a-fA-F]{3}){1,2}$/'],

      [['puntaje_maximo'], 'validarPuntajeMaximo',
        'when' => function($model) { return !!$model->tipo_parametro_id; }],
      [['nombre'], 'filter', 'filter' => 'trim'],
    ];
  }

  function validarPuntajeMaximo($attribute, $params, $validator) {
    $sum = $this->tipoParametro
      ->getParametros()
      ->andFilterWhere(['AND',
        ['<>', 'id', $this->id],
        ['tipo_parametro_id' => $this->tipo_parametro_id],
        ['parametro_id' => $this->parametro_id],
      ])
      ->sum('puntaje_maximo') + $this->puntaje_maximo + 0;
    $max = $this->tipoParametro->puntaje_maximo + 0;
    if ($sum > $max) {
      $this->addError($attribute, "La suma de puntajes ($sum) supera el máximo ($max)");
    }
  }

  public function attributeLabels()
  {
    return [
      'tipo_parametro_id' => 'Tipo de parámetro',
      'parametro_id' => 'Parámetro',
      'nombre' => 'Nombre',
      'descripcion' => 'Descripción',
      'puntaje_maximo' => 'Puntaje máximo',
      'fecha_creacion' => 'Fecha de creación',
      'fecha_edicion' => 'Fecha de edición',
    ];
  }

  public function icono()
  {
    return '<span class="mdi mdi-' . $this->icono . '"></span>';
  }

  public function beforeSave($insert)
  {
    if ($insert) {
      $this->icono = $this->tipoParametro->icono;
    }
    return parent::beforeSave($insert);
  }

  public function afterSave($insert, $changedAttributes)
  {
    $this->tipoParametro->revisarRangosParametro();
    parent::afterSave($insert, $changedAttributes);
  }

  public function getTipoParametro()
  {
    return $this->hasOne(TipoParametro::class, ['id' => 'tipo_parametro_id']);
  }

  public function getParametro()
  {
    return $this->hasOne(Parametro::class, ['id' => 'parametro_id']);
  }

  public function getParametros()
  {
    return $this->hasMany(Parametro::class, ['parametro_id' => 'id']);
  }

  public function getPreguntas()
  {
    return $this->hasMany(Pregunta::class, ['id' => 'pregunta_id'])
      ->via('preguntasParametro');
  }

  public function getRespuestas($respuestas_id = null)
  {
    $q = $this->hasMany(Respuesta::class, ['pregunta_id' => 'id'])
      ->via('preguntas');
    if ($respuestas_id) {
      $q->andWhere(['respuestas_id' => $respuestas_id]);
    } else {
      $q->andWhere(['respuestas_id' => Respuestas::find()
        ->where('fecha_termino IS NOT NULL')
        ->select('id')
        ->column()]);
    }
    return $q;
  }

  public function getParametroRango($rango_id)
  {
    return $this->getParametroRangos()
      ->andWhere(['rango_id' => $rango_id])
      ->one();
  }

  public function getParametroRangos()
  {
    return $this->hasMany(ParametroRango::class, ['parametro_id' => 'id']);
  }

  public function getPreguntasParametro()
  {
    return $this->hasMany(PreguntaParametro::class, ['parametro_id' => 'id']);
  }

  public function getPuntajeParametro($respuestas_id)
  {
    return $this->hasMany(PuntajeParametro::class, ['parametro_id' => 'id'])
      ->where(['respuestas_id' => $respuestas_id])
      ->one();
  }

  public function getTiposParametro()
  {
    $query = TipoParametro::find()->where(['OR',
      ['tipo_parametro_id' => $this->tipo_parametro_id],
      ['parametro_id' => $this->id],
    ]);
    $query->multiple = true;
    return $query;
  }

  public function resultado($respuestas_id)
  {
    $puntajeParametro = $this->getPuntajeParametro($respuestas_id);
    if ($puntajeParametro) {
      return [$puntajeParametro->puntaje, $puntajeParametro->maximo];
    } else {
      $puntaje_total = $this->getRespuestas($respuestas_id)->sum('puntaje') + 0;
      $maximo_total = $this->puntaje_maximo + 0;
      foreach ($this->tiposParametro as $tipoParametro) {
        list($puntaje, $maximo) = $tipoParametro->resultado($respuestas_id, $this->id);
        $puntaje_total += $puntaje;
        $maximo_total += $maximo;
      }
      $puntajeParametro = new PuntajeParametro();
      $puntajeParametro->parametro_id = $this->id;
      $puntajeParametro->respuestas_id = $respuestas_id;
      $puntajeParametro->puntaje = $puntaje_total;
      $puntajeParametro->maximo = $maximo_total;
      $puntajeParametro->save();
      return [$puntaje_total, $maximo_total];
    }
  }
}
