<?php

namespace common\models;
use backend\components\ActiveRecord;

use Yii;

/**
 * This is the model class for table "parametro_rango".
 *
 * @property int $id
 * @property int $parametro_id
 * @property int $rango_id
 * @property string|null $texto
 *
 * @property Parametro $parametro
 * @property Rango $rango
 */
class ParametroRango extends ActiveRecord
{
  public static function tableName()
  {
    return 'parametro_rango';
  }

  public function rules()
  {
    return [
      [['parametro_id', 'rango_id'], 'required'],
      [['parametro_id', 'rango_id'], 'default', 'value' => null],
      [['parametro_id', 'rango_id'], 'integer'],
      [['texto'], 'string'],
      [['parametro_id'], 'exist', 'skipOnError' => true,
        'targetClass' => Parametro::class,
        'targetAttribute' => ['parametro_id' => 'id']],
      [['rango_id'], 'exist', 'skipOnError' => true,
        'targetClass' => Rango::class,
        'targetAttribute' => ['rango_id' => 'id']],
    ];
  }

  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'parametro_id' => 'Parametro ID',
      'rango_id' => 'Rango ID',
      'texto' => 'Texto',
    ];
  }

  public function getParametro()
  {
    return $this->hasOne(Parametro::class, ['id' => 'parametro_id']);
  }

  public function getRango()
  {
    return $this->hasOne(Rango::class, ['id' => 'rango_id']);
  }
}
