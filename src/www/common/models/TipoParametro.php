<?php

namespace common\models;

use backend\components\ActiveRecord;

class TipoParametro extends ActiveRecord
{

  public static function tableName()
  {
    return 'tipo_parametro';
  }

  public function rules()
  {
    return [
      [['cuestionario_id', 'nombre', 'puntaje_maximo', 'icono'], 'required'],
      [['cuestionario_id', 'tipo_parametro_id', 'parametro_id'], 'default', 'value' => null],
      [['cuestionario_id', 'tipo_parametro_id', 'parametro_id'], 'integer'],
      [['puntaje_maximo'], 'number'],
      [['fecha_creacion', 'fecha_edicion'], 'safe'],
      [['nombre', 'icono'], 'string', 'max' => 64],
      [['color'], 'string', 'max' => 7],
      [['descripcion'], 'string', 'max' => 512],
      [['cuestionario_id'], 'exist', 'skipOnError' => true,
        'targetClass' => Cuestionario::class,
        'targetAttribute' => ['cuestionario_id' => 'id']],
      [['tipo_parametro_id'], 'exist', 'skipOnError' => true,
        'targetClass' => TipoParametro::class,
        'targetAttribute' => ['tipo_parametro_id' => 'id']],
      [['parametro_id'], 'exist', 'skipOnError' => true,
        'targetClass' => Parametro::class,
        'targetAttribute' => ['parametro_id' => 'id']],

      [['color'], 'match', 'pattern' => '/^#(?:[0-9a-fA-F]{3}){1,2}$/'],

      // [['puntaje_maximo'], 'validarPuntajeMaximo',
      //   'when' => function($model) { return !!$model->tipo_parametro_id; }]
    ];
  }

  // function validarPuntajeMaximo($attribute, $params, $validator) {
  //   $sum = $this->tipoParametro
  //     ->getParametros()
  //     ->andFilterWhere(['<>', 'id', $this->id])
  //     ->sum('puntaje_maximo') + $this->puntaje_maximo + 0;
  //   $max = $this->parametro->puntaje_maximo + 0;
  //   if ($sum > $max) {
  //     $this->addError($attribute, "La suma de puntajes ($sum) supera el máximo ($max)");
  //   }
  // }

  public function attributeLabels()
  {
    return [
      'cuestionario_id' => 'Cuestionario',
      'tipo_parametro_id' => 'Tipo de Parámetro',
      'nombre' => 'Nombre del Tipo de parámetro',
      'descripcion' => 'Descripción',
      'puntaje_igual' => 'No cambiar puntaje',
      'puntaje_maximo' => 'Puntaje total',
      'fecha_creacion' => 'Fecha de creación',
      'fecha_edicion' => 'Fecha de edición',
      'comun' => 'Se aplica a todos los parámetros',
    ];
  }

  public function attributeHints()
  {
    return [
      'nombre' => 'En singular',
      'color' => 'Color por defecto para los elementos de este tipo de parámetro',
      'icono' => 'Icono por defecto',
      'puntaje_maximo' => 'Puntaje máximo de la suma de los elementos de este tipo de parámetro',
    ];
  }

  public function icono()
  {
    return '<span class="mdi mdi-' . $this->icono . '"></span>';
  }

  public function afterSave($insert, $changedAttributes)
  {
    parent::afterSave($insert, $changedAttributes);
  }

  public function getCuestionario()
  {
    return $this->hasOne(Cuestionario::class, ['id' => 'cuestionario_id']);
  }

  public function getTipoParametro()
  {
    return $this->hasOne(TipoParametro::class, ['id' => 'tipo_parametro_id'])
      ->orderBy('nombre');
  }

  public function getParametro()
  {
    return $this->hasOne(Parametro::class, ['id' => 'parametro_id']);
  }

  public function getRangos()
  {
    return $this->hasMany(Rango::class, ['tipo_parametro_id' => 'id'])
      ->orderBy('desde');
  }

  public function getTiposParametro()
  {
    $query = TipoParametro::find()->where(['OR',
      ['tipo_parametro_id' => $this->id],
      ['parametro_id' => $this->getParametros()->select('id')],
    ])->orderBy('nombre');
    $query->multiple = true;
    return $query;
  }

  public function getParametros($parametro_id = null)
  {
    return $this->hasMany(Parametro::class, ['tipo_parametro_id' => 'id'])
      ->andFilterWhere(['parametro.parametro_id' => $parametro_id])
      ->orderBy('nombre');
  }

  public function revisarRangosParametro()
  {
    foreach ($this->rangos as $rango) {
      $rango->revisarRangosParametro();
    }
  }

  public function resultado($respuestas_id, $parametro_id = null)
  {
    $puntaje_total = 0;
    $maximo_total = 0;
    foreach ($this->getParametros($parametro_id)->all() as $parametro) {
      list($puntaje, $maximo) = $parametro->resultado($respuestas_id);
      $puntaje_total += $puntaje;
      $maximo_total += $maximo;
    }
    return [$puntaje_total, $maximo_total];
  }
}
