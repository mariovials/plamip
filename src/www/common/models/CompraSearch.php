<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Parametro;

/**
 * CompraSearch represents the model behind the search form of `common\models\Parametro`.
 */
class CompraSearch extends Parametro
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'cuestionario_id', 'parametro_id'], 'integer'],
            [['nombre', 'descripcion', 'fecha_creacion', 'fecha_edicion'], 'safe'],
            [['puntaje_maximo'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Parametro::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'cuestionario_id' => $this->cuestionario_id,
            'parametro_id' => $this->parametro_id,
            'puntaje_maximo' => $this->puntaje_maximo,
            'fecha_creacion' => $this->fecha_creacion,
            'fecha_edicion' => $this->fecha_edicion,
        ]);

        $query->andFilterWhere(['ilike', 'nombre', $this->nombre])
            ->andFilterWhere(['ilike', 'descripcion', $this->descripcion]);

        return $dataProvider;
    }
}
