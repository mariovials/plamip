<?php

namespace common\models;
use backend\components\ActiveRecord;

use Yii;

class Resultado extends ActiveRecord
{
  public static function tableName()
  {
    return 'resultado';
  }

  public function rules()
  {
    return [
      [['cuestionario_id'], 'required'],
      [['cuestionario_id'], 'default', 'value' => null],
      [['cuestionario_id'], 'integer'],
      [['nombre'], 'string', 'max' => 64],
      [['desde', 'hasta'], 'number'],
      [['desde'], 'number', 'min' => 0],
      [['hasta'], 'number', 'max' => 100],
      [['cuestionario_id'], 'exist', 'skipOnError' => true,
        'targetClass' => Cuestionario::class,
        'targetAttribute' => ['cuestionario_id' => 'id']],
      [['desde'], 'validar'],
      [['texto'], 'string'],
      [['texto_promedio'], 'string'],
    ];
  }


  public function validar()
  {
    if ($this->hasta <= $this->desde) {
      $this->addError('hasta', "Debe ser mayor a $this->desde.");
    }
  }

  public function getNombreRango()
  {
    $desde = $this->desde + 0;
    $hasta = $this->hasta + 0;
    return $this->nombre ?: "$desde% a $hasta%";
  }

  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'cuestionario_id' => 'Cuestionario',
      'desde' => 'Desde',
      'hasta' => 'Hasta',
      'texto' => 'Texto para resultado individual',
      'texto_promedio' => 'Texto para resultados generales',
    ];
  }

  public function attributeHints()
  {
    return [
      'nombre' => 'Alto, medio, bajo, suficiente, insuficiente..',
    ];
  }

  public function getCuestionario()
  {
    return $this->hasOne(Cuestionario::class, ['id' => 'cuestionario_id']);
  }
}
