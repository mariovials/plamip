<?php

namespace common\models;
use backend\components\ActiveRecord;
use common\behaviors\FechaBehavior;
use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "seccion".
 *
 * @property int $id
 * @property int $cuestionario_id
 * @property int $orden
 * @property string|null $nombre
 * @property string|null $descripcion
 * @property string $fecha_creacion
 * @property string|null $fecha_edicion
 */
class Seccion extends ActiveRecord
{

  public function behaviors()
  {
    return [
      FechaBehavior::class,
    ];
  }

  public static function tableName()
  {
    return 'seccion';
  }

  public function rules()
  {
    return [
      [['cuestionario_id', 'nombre'], 'required'],
      [['cuestionario_id', 'orden'], 'default', 'value' => 1],
      [['cuestionario_id', 'orden'], 'integer'],
      [['nombre', 'descripcion'], 'string'],
      [['fecha_creacion', 'fecha_edicion'], 'safe'],
    ];
  }

  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'cuestionario_id' => 'Cuestionario ID',
      'orden' => 'Orden',
      'nombre' => 'Nombre',
      'descripcion' => 'Descripción',
      'fecha_creacion' => 'Fecha de creación',
      'fecha_edicion' => 'Fecha de edición',
    ];
  }

  public function getUrl() {
    return Url::to(['/cuestionario/preguntas',
      'id' => $this->cuestionario_id,
      '#' => "seccion-$this->id"]);
  }

  public function beforeSave($insert)
  {
    if ($insert) {
      $this->orden = Seccion::find()
        ->where(['cuestionario_id' => $this->cuestionario_id])
        ->max('orden') + 1;
    }
    if ($this->isAttributeChanged('orden')) {
      $this->ordenar();
    }
    return parent::beforeSave($insert);
  }

  private function ordenar()
  {
    if ($this->cuestionario->getSecciones()->andWhere(['orden' => $this->orden])->exists()) {
      $orden = $this->orden;
      if ($orden == 1 || $this->oldAttributes['orden'] > $this->orden) {
        foreach ($this->cuestionario->getSecciones()->andWhere(['AND',
          ['<>', 'id', $this->id],
          ['>=', 'orden', $this->orden],
        ])->orderBy('orden ASC')->all() as $i => $seccion) {
          $seccion->updateAttributes(['orden' => $orden + $i + 1]);
        }
      } else {
        foreach ($this->cuestionario->getSecciones()->andWhere(['AND',
          ['<>', 'id', $this->id],
          ['<=', 'orden', $this->orden],
        ])->orderBy('orden DESC')->all() as $i => $seccion) {
          $seccion->updateAttributes(['orden' => $orden - $i - 1]);
        }
      }
    }
  }

  public function beforeDelete()
  {
    return parent::beforeDelete();
  }

  public function afterSave($insert, $changedAttributes)
  {
    parent::afterSave($insert, $changedAttributes);
  }

  public function getCuestionario()
  {
    return $this->hasOne(Cuestionario::class, ['id' => 'cuestionario_id']);
  }

  public function getPreguntas()
  {
    return $this->hasMany(Pregunta::class, ['seccion_id' => 'id']);
  }

  function renumerarPreguntas()
  {
    foreach ($this->getPreguntas()->orderBy('orden')->all() as $orden => $etiqueta) {
      $etiqueta->updateAttributes(['orden' => $orden + 1]);
    }
  }
}
