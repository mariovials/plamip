<?php

namespace common\models;
use backend\components\ActiveRecord;

use Yii;

/**
 * This is the model class for table "opcion".
 *
 * @property int $id
 * @property int $pregunta_id
 * @property int $orden
 * @property string|null $nombre
 * @property string|null $descripcion
 * @property bool|null $editable
 * @property string $fecha_creacion
 * @property string|null $fecha_edicion
 */
class Opcion extends ActiveRecord
{
  public $preguntas = null;

  public static function tableName()
  {
    return 'opcion';
  }

  public function rules()
  {
    return [
      [['preguntas'], 'safe'],
      [['pregunta_id', 'nombre'], 'required'],
      [['pregunta_id', 'orden'], 'default', 'value' => null],
      [['pregunta_id', 'orden'], 'integer'],
      [['nombre', 'descripcion'], 'string'],
      [['editable'], 'boolean'],
      [['fecha_creacion', 'fecha_edicion'], 'safe'],
      [['puntaje'], 'number', 'min' => 0, 'max' => $this->pregunta->puntaje + 0]
    ];
  }

  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'pregunta_id' => 'Pregunta ID',
      'orden' => 'Orden',
      'nombre' => 'Nombre',
      'descripcion' => 'Descripción',
      'editable' => 'Editable',
      'fecha_creacion' => 'Fecha de creación',
      'fecha_edicion' => 'Fecha de edición',
    ];
  }

  public function beforeSave($insert)
  {
    if ($insert) {
      $this->orden = Opcion::find()
        ->where(['pregunta_id' => $this->pregunta_id])
        ->max('orden') + 1;
      if ($this->pregunta->numerico) {
        $this->editable = true;
      }
    }
    return parent::beforeSave($insert);
  }

  public function afterSave($insert, $changedAttributes)
  {
    if ($this->preguntas !== null) {
      Mostrar::deleteAll(['opcion_id' => $this->id]);
      if (is_array($this->preguntas)) {
        foreach ($this->preguntas as $pregunta_id) {
          $mostrar = new Mostrar();
          $mostrar->pregunta_id = $pregunta_id;
          $mostrar->opcion_id = $this->id;
          $mostrar->save();
        }
      }
    }
    parent::afterSave($insert, $changedAttributes);
  }

  public function getPregunta()
  {
    return $this->hasOne(Pregunta::class, ['id' => 'pregunta_id']);
  }

  public function getMostrar()
  {
    return $this->hasMany(Mostrar::class, ['opcion_id' => 'id']);
  }
}
