<?php

namespace common\models;
use backend\components\ActiveRecord;

use Yii;

/**
 * This is the model class for table "rango".
 *
 * @property int $id
 * @property int $tipo_parametro_id
 * @property float $desde
 * @property float $hasta
 *
 * @property ParametroRango[] $parametroRangos
 * @property TipoParametro $tipoParametro
 */
class Rango extends ActiveRecord
{
  public static function tableName()
  {
    return 'rango';
  }

  public function rules()
  {
    return [
      [['tipo_parametro_id'], 'required'],
      [['tipo_parametro_id'], 'default', 'value' => null],
      [['tipo_parametro_id'], 'integer'],
      [['nombre'], 'string', 'max' => 64],
      [['texto'], 'string'],
      [['desde', 'hasta'], 'number'],
      [['desde'], 'number', 'min' => 0],
      [['hasta'], 'number', 'max' => 100],
      [['tipo_parametro_id'], 'exist', 'skipOnError' => true,
        'targetClass' => TipoParametro::class,
        'targetAttribute' => ['tipo_parametro_id' => 'id']],
      [['desde'], 'validar'],
    ];
  }


  public function validar()
  {
    if ($this->hasta <= $this->desde) {
      $this->addError('hasta', "Debe ser mayor a $this->desde.");
    }
  }

  public function getNombreRango()
  {
    $desde = $this->desde + 0;
    $hasta = $this->hasta + 0;
    return $this->nombre ?: "$desde% a $hasta%";
  }

  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'tipo_parametro_id' => 'Tipo Parametro ID',
      'desde' => 'Desde',
      'hasta' => 'Hasta',
      'texto' => 'Texto para resultado general',
    ];
  }

  public function attributeHints()
  {
    return [
      'nombre' => 'Alto, medio, bajo, suficiente, insuficiente..',
    ];
  }

  public function afterSave($insert, $changedAttributes)
  {
    $this->revisarRangosParametro();
    parent::afterSave($insert, $changedAttributes);
  }

  public function revisarRangosParametro()
  {
    foreach ($this->tipoParametro->parametros as $parametro) {
      if (!ParametroRango::find()->where([
        'rango_id' => $this->id,
        'parametro_id' => $parametro->id,
      ])->exists()) {
        $parametroRango = new ParametroRango();
        $parametroRango->rango_id = $this->id;
        $parametroRango->parametro_id = $parametro->id;
        $parametroRango->save();
      }
    }
  }

  public function getParametroRangos($parametro_id = null)
  {
    return $this->hasMany(ParametroRango::class, ['rango_id' => 'id']);
  }

  public function getTipoParametro()
  {
    return $this->hasOne(TipoParametro::class, ['id' => 'tipo_parametro_id']);
  }
}
