<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use backend\components\ActiveRecord;
use yii\web\IdentityInterface;
use common\behaviors\FechaBehavior;

/**
 * Usuario model
 */
class Usuario extends ActiveRecord implements IdentityInterface
{

  public static function tableName() {
    return 'usuario';
  }

  const ADMIN   = 1;
  const EXTERNO = 2;
  const VALIDADOR = 3;

  const ESTADO_BORRADO   = 0;
  const ESTADO_ACTIVO   =  1;
  const ESTADO_INACTIVO  = 2;
  const ESTADOS = [
    self::ESTADO_BORRADO   => 'Eliminado',
    self::ESTADO_ACTIVO    => 'Activo',
    self::ESTADO_INACTIVO  => 'Inactivo',
  ];

  public $confirmar_contrasena = '';

  public function estado()
  {
    return $this::ESTADOS[$this->estado];
  }

  public function behaviors()
  {
    return [
      FechaBehavior::class,
    ];
  }

  public function rules()
  {
    return [
      [['nombre', 'institucion', 'contrasena', 'correo_electronico'], 'required'],
      [['tipo', 'estado'], 'required', 'on' => ['agregar']],
      [['nombre', 'institucion', 'usuario'], 'string', 'max' => 256],
      [['estado'], 'default', 'value' => self::ESTADO_ACTIVO],
      [['contrasena'], 'string', 'min' => 6],

      [['correo_electronico'], 'unique'],
      [['correo_electronico'], 'email'],
      [['estado'], 'in', 'range' => [
        self::ESTADO_ACTIVO,
        self::ESTADO_INACTIVO,
        self::ESTADO_BORRADO]],

      [['confirmar_contrasena'], 'compare',
        'compareAttribute' => 'contrasena', 'on' => ['registro', 'agregar']],
      [['confirmar_contrasena'], 'required', 'on' => ['registro', 'agregar']],
      [['confirmar_contrasena'], 'required', 'on' => ['registro', 'agregar']],

      [['tipo'], 'integer'],
      [['tipo'], 'filter', 'filter' => 'intval'],
      [['nombre', 'institucion', 'usuario'], 'filter', 'filter' => 'trim'],
    ];
  }

  public function attributeLabels()
  {
    return [
      'correo_electronico' => 'Correo electrónico',
      'contrasena' => 'Contraseña',
      'confirmar_contrasena' => 'Repetir contraseña',
      'institucion' => 'Institución',
    ];
  }

  public function tipo()
  {
    return [
      '1' => 'Administrador',
      '2' => 'Usuario',
      '3' => 'Validador',
    ][$this->tipo];
  }

  public function esAdmin()
  {
    return $this->tipo == Usuario::ADMIN;
  }

  public function esValidador()
  {
    return $this->tipo == Usuario::VALIDADOR;
  }

  public function beforeSave($insert)
  {
    if ($insert || $this->isAttributeChanged('contrasena')) {
      $this->generateAuthKey();
      $this->contrasena = Yii::$app->security->generatePasswordHash($this->contrasena);
    }
    if ($insert) {
      if ($this->tipo == null) {
        $this->tipo = Usuario::EXTERNO;
        $this->estado = Usuario::ESTADO_ACTIVO;
      }
      if ($this->usuario == '') {
        $this->usuario = explode('@', $this->correo_electronico)[0];
      }
    }
    return parent::beforeSave($insert);
  }

  public static function findIdentity($id)
  {
    return static::findOne(['id' => $id, 'estado' => self::ESTADO_ACTIVO]);
  }

  public static function findIdentityByAccessToken($token, $type = null)
  {
    throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
  }

  public function getId()
  {
    return $this->getPrimaryKey();
  }

  public function getAuthKey()
  {
    return $this->auth_key;
  }

  public function validateAuthKey($authKey)
  {
    return $this->getAuthKey() === $authKey;
  }

  public function validarContrasena($password)
  {
    return Yii::$app->security->validatePassword($password, $this->contrasena);
  }

  public function generateAuthKey()
  {
    $this->auth_key = Yii::$app->security->generateRandomString();
  }

  public function getRespuestas()
  {
    return $this->hasMany(Respuestas::class, ['usuario_id' => 'id']);
  }

  public function beforeDelete()
  {
    if ($this->getRespuestas()->exists()) {
      Yii::$app->session->addFlash('error',
        "No se puede eliminar porque tiene respuestas");
      return false;
    }
    return parent::beforeDelete();
  }
}
