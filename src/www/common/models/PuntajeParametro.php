<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "puntaje_parametro".
 *
 * @property int $id
 * @property int|null $respuestas_id
 * @property int|null $parametro_id
 * @property float|null $puntaje
 *
 * @property Parametro $parametro
 * @property Respuestas $respuestas
 */
class PuntajeParametro extends \yii\db\ActiveRecord
{
  public static function tableName()
  {
    return 'puntaje_parametro';
  }

  public function rules()
  {
    return [
      [['respuestas_id', 'parametro_id'], 'default', 'value' => null],
      [['respuestas_id', 'parametro_id'], 'integer'],
      [['puntaje', 'maximo'], 'number'],
      [['respuestas_id', 'parametro_id'], 'unique', 'targetAttribute' => ['respuestas_id', 'parametro_id']],
      [['parametro_id'], 'exist', 'skipOnError' => true,
        'targetClass' => Parametro::class,
        'targetAttribute' => ['parametro_id' => 'id']],
      [['respuestas_id'], 'exist', 'skipOnError' => true,
        'targetClass' => Respuestas::class,
        'targetAttribute' => ['respuestas_id' => 'id']],
    ];
  }

  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'respuestas_id' => 'Respuestas ID',
      'parametro_id' => 'Parametro ID',
      'puntaje' => 'Puntaje',
    ];
  }

  public function getParametro()
  {
    return $this->hasOne(Parametro::class, ['id' => 'parametro_id']);
  }

  public function getRespuestas()
  {
    return $this->hasOne(Respuestas::class, ['id' => 'respuestas_id']);
  }
}
