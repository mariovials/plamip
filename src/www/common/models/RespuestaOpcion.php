<?php

namespace common\models;
use backend\components\ActiveRecord;

use Yii;

/**
 * This is the model class for table "respuesta_opcion".
 *
 * @property int $id
 * @property int|null $respuesta_id
 * @property int|null $opcion_id
 * @property int $orden
 * @property float $puntaje
 * @property string|null $valor
 * @property string $fecha_creacion
 * @property string|null $fecha_edicion
 *
 * @property Opcion $opcion
 * @property Respuesta $respuesta
 */
class RespuestaOpcion extends ActiveRecord
{
  public static function tableName()
  {
    return 'respuesta_opcion';
  }

  public function rules()
  {
    return [
      [['respuesta_id', 'opcion_id', 'orden'], 'default', 'value' => null],
      [['respuesta_id', 'opcion_id', 'orden'], 'integer'],
      [['puntaje'], 'number'],
      [['fecha_creacion', 'fecha_edicion'], 'safe'],
      [['valor'], 'string', 'max' => 255],
      [['opcion_id', 'respuesta_id'], 'unique', 'targetAttribute' => ['opcion_id', 'respuesta_id']],
      [['opcion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Opcion::class, 'targetAttribute' => ['opcion_id' => 'id']],
      [['respuesta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Respuesta::class, 'targetAttribute' => ['respuesta_id' => 'id']],
    ];
  }

  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'respuesta_id' => 'Respuesta ID',
      'opcion_id' => 'Opcion ID',
      'orden' => 'Orden',
      'puntaje' => 'Puntaje',
      'valor' => 'Valor',
      'fecha_creacion' => 'Fecha Creacion',
      'fecha_edicion' => 'Fecha Edicion',
    ];
  }

  public function beforeSave($insert)
  {
    $this->puntaje = $this->opcion->puntaje;
    if ($this->respuesta
      && $this->respuesta->pregunta
      && $this->respuesta->pregunta->tipo == Pregunta::TIPO_SELECCION
      && !$this->respuesta->pregunta->multiple) {
      RespuestaOpcion::updateAll(
        ['valor' => null],
        ['respuesta_id' => $this->respuesta_id ]);
    }
    return parent::beforeSave($insert);
  }

  public function afterSave($insert, $changedAttributes)
  {
    $opciones = Opcion::find()->where([
      'id' => RespuestaOpcion::find()->where(['AND',
        "valor IS NOT NULL",
        "valor <> ''",
        "valor <> '0'",
        ['respuesta_id' => $this->respuesta_id],
      ])
      ->select('opcion_id')
    ])->orderBy('orden');
    if ($this->respuesta) {
      $this->respuesta->updateAttributes([
        'valor' => implode(', ', $opciones->select('nombre')->column()),
        'puntaje' => $opciones->sum('puntaje') + 0,
      ]);
    }

    // limpiar las preguntas que no se muestran con esta opcion
    foreach ($this->opcion->pregunta->opciones as $opcion) {
      if ($opcion->id == $this->opcion_id) continue;
      foreach ($opcion->mostrar as $mostrar) {
        $mostrar->pregunta->limpiarRespuesta($this->respuesta->respuestas_id);
      }
    }

    parent::afterSave($insert, $changedAttributes);
  }

  public function getOpcion()
  {
    return $this->hasOne(Opcion::class, ['id' => 'opcion_id']);
  }

  public function getRespuesta()
  {
    return $this->hasOne(Respuesta::class, ['id' => 'respuesta_id']);
  }
}
