<?php

namespace common\models;
use backend\components\ActiveRecord;

use Yii;

/**
 * This is the model class for table "migration".
 *
 * @property string $version
 * @property int|null $apply_time
 */
class Migration extends ActiveRecord
{

  public static function tableName()
  {
    return 'migration';
  }

  public function rules()
  {
    return [
      [['version'], 'required'],
      [['apply_time'], 'default', 'value' => null],
      [['apply_time'], 'integer'],
      [['version'], 'string', 'max' => 180],
      [['version'], 'unique'],
    ];
  }

  public function attributeLabels()
  {
    return [
      'version' => 'Version',
      'apply_time' => 'Apply Time',
    ];
  }
}
