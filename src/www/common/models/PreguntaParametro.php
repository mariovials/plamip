<?php

namespace common\models;
use backend\components\ActiveRecord;

use Yii;

/**
 * This is the model class for table "pregunta_parametro".
 *
 * @property int $id
 * @property int $pregunta_id
 * @property int $parametro_id
 */
class PreguntaParametro extends ActiveRecord
{
  /**
   * {@inheritdoc}
   */
  public static function tableName()
  {
    return 'pregunta_parametro';
  }

  /**
   * {@inheritdoc}
   */
  public function rules()
  {
    return [
      [['pregunta_id', 'parametro_id'], 'required'],
      [['pregunta_id', 'parametro_id'], 'default', 'value' => null],
      [['pregunta_id', 'parametro_id'], 'integer'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'pregunta_id' => 'Pregunta ID',
      'parametro_id' => 'Parametro ID',
    ];
  }
}
