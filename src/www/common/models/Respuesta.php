<?php

namespace common\models;
use backend\components\ActiveRecord;

use Yii;

/**
 * This is the model class for table "respuesta".
 *
 * @property int $id
 * @property int|null $respuestas_id
 * @property int|null $pregunta_id
 * @property int $orden
 * @property float $puntaje
 * @property string|null $valor
 * @property string $fecha_creacion
 * @property string|null $fecha_edicion
 *
 * @property Pregunta $pregunta
 * @property RespuestaOpcion[] $respuestaOpcions
 * @property Respuestas $respuestas
 */
class Respuesta extends ActiveRecord
{
  /**
   * {@inheritdoc}
   */
  public static function tableName()
  {
    return 'respuesta';
  }

  /**
   * {@inheritdoc}
   */
  public function rules()
  {
    return [
      [['respuestas_id', 'pregunta_id', 'orden'], 'default', 'value' => null],
      [['respuestas_id', 'pregunta_id', 'orden'], 'integer'],
      [['puntaje'], 'number'],
      [['fecha_creacion', 'fecha_edicion'], 'safe'],
      [['valor'], 'string', 'max' => 255],
      [['respuestas_id', 'pregunta_id'], 'unique', 'targetAttribute' => ['respuestas_id', 'pregunta_id']],
      [['pregunta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pregunta::class, 'targetAttribute' => ['pregunta_id' => 'id']],
      [['respuestas_id'], 'exist', 'skipOnError' => true, 'targetClass' => Respuestas::class, 'targetAttribute' => ['respuestas_id' => 'id']],
    ];
  }

  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'respuestas_id' => 'Respuestas ID',
      'pregunta_id' => 'Pregunta ID',
      'orden' => 'Orden',
      'puntaje' => 'Puntaje',
      'valor' => 'Valor',
      'fecha_creacion' => 'Fecha Creacion',
      'fecha_edicion' => 'Fecha Edicion',
    ];
  }

  public function beforeSave($insert)
  {
    if ($this->valor) {
      $this->puntaje = $this->pregunta->puntaje;
    }
    return parent::beforeSave($insert);
  }

  public function getPregunta()
  {
    return $this->hasOne(Pregunta::class, ['id' => 'pregunta_id']);
  }

  public function getRespuestaOpciones()
  {
    return $this->hasMany(RespuestaOpcion::class, ['respuesta_id' => 'id']);
  }

  public function getRespuestas()
  {
    return $this->hasOne(Respuestas::class, ['id' => 'respuestas_id']);
  }
}
