<?php

namespace common\models;

use Yii;
use backend\components\ActiveRecord;

/**
 * This is the model class for table "respuestas".
 *
 * @property int $id
 * @property int|null $cuestionario_id
 * @property int $usuario_id
 * @property string $fecha_creacion
 * @property string|null $fecha_edicion
 * @property string|null $fecha_termino
 *
 * @property Cuestionario $cuestionario
 * @property Pregunta[] $preguntas
 * @property Respuesta[] $respuestas
 * @property Usuario $usuario
 */
class Respuestas extends ActiveRecord
{
  public static function tableName()
  {
    return 'respuestas';
  }

  public function rules()
  {
    return [
      [['cuestionario_id', 'usuario_id'], 'default', 'value' => null],
      [['cuestionario_id', 'usuario_id'], 'integer'],
      [['usuario_id'], 'required'],
      [['puntaje'], 'number'],
      [['fecha_creacion', 'fecha_edicion', 'fecha_termino'], 'safe'],
      [['cuestionario_id'], 'exist', 'skipOnError' => true,
        'targetClass' => Cuestionario::class,
        'targetAttribute' => ['cuestionario_id' => 'id']],
      [['usuario_id'], 'exist', 'skipOnError' => true,
        'targetClass' => Usuario::class,
        'targetAttribute' => ['usuario_id' => 'id']],
    ];
  }

  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'cuestionario_id' => 'Cuestionario ID',
      'usuario_id' => 'Usuario ID',
      'fecha_creacion' => 'Fecha Creacion',
      'fecha_edicion' => 'Fecha Edicion',
      'fecha_termino' => 'Fecha Termino',
    ];
  }

  public function beforeSave($insert)
  {
    if ($insert) {
      $this->fecha_creacion = date('Y-m-d H:i:s');
    }
    if ($this->isAttributeChanged('fecha_termino')) {
      $this->puntaje = $this->calcularPuntaje();
    }
    return parent::beforeSave($insert);
  }

  public function resultado()
  {
    $puntaje_total = 0;
    $maximo_total = 0;
    foreach ($this->cuestionario->tiposParametro as $tipoParametro) {
      list($puntaje, $maximo) = $tipoParametro->resultado($this->id);
      $puntaje_total += $puntaje;
      $maximo_total += $maximo;
    }
    return [$puntaje_total, $maximo_total];
  }

  public function calcularPuntaje()
  {
    list($puntaje_total, $maximo_total) = $this->resultado();
    return $maximo_total ? ceil($puntaje_total / $maximo_total * 100) : null;
  }

  public function getCuestionario()
  {
    return $this->hasOne(Cuestionario::class, ['id' => 'cuestionario_id']);
  }

  public function getPreguntas()
  {
    return $this->hasMany(Pregunta::class, ['id' => 'pregunta_id'])->viaTable('respuesta', ['respuestas_id' => 'id']);
  }

  public function getRespuestas()
  {
    return $this->hasMany(Respuesta::class, ['respuestas_id' => 'id']);
  }

  public function getPuntajes()
  {
    return $this->hasMany(PuntajeParametro::class, ['respuestas_id' => 'id']);
  }

  public function getUsuario()
  {
    return $this->hasOne(Usuario::class, ['id' => 'usuario_id']);
  }

  public function iniciar()
  {
    foreach ($this->cuestionario->preguntas as $pregunta) {
      $respuesta = Respuesta::find()->where([
        'respuestas_id' => $this->id,
        'pregunta_id' => $pregunta->id,
      ])->one();
      if (!$respuesta) {
        $respuesta = new Respuesta();
        $respuesta->respuestas_id = $this->id;
        $respuesta->pregunta_id = $pregunta->id;
        $respuesta->orden = $pregunta->orden;
        $respuesta->save();
      }
      foreach ($pregunta->opciones as $opcion) {
        $respuestaOpcion = new RespuestaOpcion();
        $respuestaOpcion->respuesta_id = $respuesta->id;
        $respuestaOpcion->opcion_id = $opcion->id;
        $respuestaOpcion->orden = $opcion->orden;
        $respuestaOpcion->save();
      }
    }
  }
}
