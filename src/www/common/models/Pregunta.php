<?php

namespace common\models;
use backend\components\ActiveRecord;

use Yii;

/**
 * This is the model class for table "pregunta".
 *
 * @property int $id
 * @property int $cuestionario_id
 * @property int $orden
 * @property string|null $nombre
 * @property string|null $numero
 * @property string|null $descripcion
 * @property int $estado
 * @property int $tipo
 * @property float|null $puntaje
 * @property int|null $dependencia
 * @property string $fecha_creacion
 * @property string|null $fecha_edicion
 */
class Pregunta extends ActiveRecord
{
  const TIPO_TEXTO     = 1;
  const TIPO_SELECCION = 2;
  const TIPOS = [
    Pregunta::TIPO_TEXTO     => 'Texto',
    Pregunta::TIPO_SELECCION => 'Selección',
  ];

  public $parametro = [];

  public static function tableName() {
    return 'pregunta';
  }

  public function rules()
  {
    return [
      [['cuestionario_id', 'nombre', 'tipo', 'seccion_id', 'puntaje'], 'required'],
      [['cuestionario_id', 'orden', 'estado', 'tipo', 'dependencia'],
        'default',
        'value' => null],
      [['cuestionario_id', 'orden', 'estado', 'tipo', 'dependencia'],
        'integer'],
      [['nombre', 'descripcion', 'ayuda'], 'string'],
      [['puntaje'], 'number'],
      [['multiple', 'numerico'], 'boolean'],
      [['fecha_creacion', 'fecha_edicion'], 'safe'],
      [['numero'], 'string', 'max' => 5],

      [['parametro'], 'required',
        'when' => function($model) {
          return $model->puntaje > 0;
        },
        'whenClient' => 'function() {
          console.log($("#pregunta-puntaje").val() > 0)
          return $("#pregunta-puntaje").val() > 0
        }',
      ],

      [['seccion_id', 'orden'], 'filter', 'filter' => 'intval'],
    ];
  }

  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'cuestionario_id' => 'Cuestionario',
      'orden' => 'Orden',
      'nombre' => 'Texto',
      'numero' => 'Numero',
      'descripcion' => 'Descripción',
      'estado' => 'Estado',
      'tipo' => 'Tipo',
      'puntaje' => 'Puntaje',
      'dependencia' => 'Dependencia',
      'fecha_creacion' => 'Fecha de creación',
      'fecha_edicion' => 'Fecha de edición',
      'numerico' => 'Numérico',
      'seccion_id' => 'Sección',
    ];
  }

  public function tipo() {
    return Pregunta::TIPOS[$this->tipo] ?? '';
  }

  public function getTipoDiferenciado() {
    if ($this->tipo == Pregunta::TIPO_TEXTO) {
      return $this->multiple ? 'texto-multiple' : 'texto';
    } else {
      return $this->multiple ? 'opcion-multiple' : 'opcion-unitaria';
    }
  }

  public function getIcono($html = true) {
    $codigo = 'radiobox-marked';
    if ($this->tipo == Pregunta::TIPO_TEXTO) {
      $codigo = 'form-textbox';
    }
    if ($this->multiple) {
      $codigo = 'checkbox-marked-outline';
    }
    if ($this->numerico) {
      $codigo = 'numeric-1-box-outline';
    }
    if ($html) {
      return '<span class="mdi mdi-' . $codigo . '"></span>';
    } else {
      return $codigo;
    }
  }

  public function beforeSave($insert)
  {
    if (!$this->orden) {
      $this->orden = Pregunta::find()
        ->where(['cuestionario_id' => $this->cuestionario_id])
        ->max('orden') + 1;
    }
    if (!$this->numero) {
      $this->numero = $this->orden;
    }
    if ($this->isAttributeChanged('orden')) {
      $this->ordenar();
      $this->numero = $this->orden;
    }
    return parent::beforeSave($insert);
  }

  public function afterSave($insert, $changedAttributes)
  {
    $this->parametro = array_filter($this->parametro);
    if ($this->parametro) {
      PreguntaParametro::deleteAll(['pregunta_id' => $this->id]);
      $sql = Yii::$app->db->createCommand()->batchInsert(
        'pregunta_parametro',
        ['pregunta_id', 'parametro_id'],
        array_map(function($value) { return [$this->id, $value]; }, $this->parametro))
        ->execute();
    }
    // reordena la sección a la que pertenecía
    if (array_key_exists('seccion_id', $changedAttributes)
      && $changedAttributes['seccion_id']) {
      Seccion::findOne($changedAttributes['seccion_id'])->renumerarPreguntas();
    }
    parent::afterSave($insert, $changedAttributes);
  }

  public function getCuestionario()
  {
    return $this->hasOne(Cuestionario::class, ['id' => 'cuestionario_id']);
  }

  public function getSeccion()
  {
    return $this->hasOne(Seccion::class, ['id' => 'seccion_id']);
  }

  public function getOpciones()
  {
    return $this->hasMany(Opcion::class, ['pregunta_id' => 'id']);
  }

  public function getMostrar()
  {
    return $this->hasMany(Mostrar::class, ['pregunta_id' => 'id']);
  }

  public function getPreguntaParametros()
  {
    return $this->hasMany(PreguntaParametro::class, ['pregunta_id' => 'id']);
  }

  public function getParametros()
  {
    return $this->hasMany(Parametro::class, ['id' => 'parametro_id'])
      ->via('preguntaParametros')
      ->orderBy('id');
  }

  public function afterDelete()
  {
    foreach($this->seccion
      ->getPreguntas()
      ->andWhere(['>', 'orden', $this->orden])
      ->orderBy('orden ASC')
      ->all() as $pregunta) {
      $pregunta->updateAttributes(['orden' => $pregunta->orden - 1]);
    }

    parent::afterDelete();
  }

  public function limpiarRespuesta($respuestas_id)
  {
    $respuesta = Respuesta::find()->where(['AND',
      ['respuestas_id' => $respuestas_id],
      ['pregunta_id' => $this->id],
    ])->one();
    if ($respuesta) {
      $respuesta->updateAttributes(['valor' => null, 'puntaje' => 0]);
      foreach ($this->opciones as $opcion) {
        foreach ($opcion->mostrar as $mostrar) {
          $mostrar->pregunta->limpiarRespuesta($respuestas_id);
        }
      }
    }
  }

  private function ordenar()
  {
    if ($this->seccion->getPreguntas()->andWhere(['orden' => $this->orden])->exists()) {
      $orden = $this->orden;
      if ($this->orden == 1
        || isset($this->dirtyAttributes['seccion_id'])
        || $this->oldAttributes['orden'] > $this->orden) {
        foreach ($this->seccion->getPreguntas()->andWhere(['AND',
          ['<>', 'id', $this->id],
          ['>=', 'orden', $this->orden],
        ])->orderBy('orden ASC')->all() as $i => $pregunta) {
          $pregunta->updateAttributes(['orden' => $orden + $i + 1]);
        }
      } else {
        foreach ($this->seccion->getPreguntas()->andWhere(['AND',
          ['<>', 'id', $this->id],
          ['<=', 'orden', $this->orden],
        ])->orderBy('orden DESC')->all() as $i => $pregunta) {
          $pregunta->updateAttributes(['orden' => $orden - $i - 1]);
        }
      }
    }
  }
}
