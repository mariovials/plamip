<?php

namespace common\models;
use backend\components\ActiveRecord;
use common\behaviors\FechaBehavior;
use yii\helpers\Url;

/**
 * This is the model class for table "cuestionario".
 *
 * @property int $id
 * @property string $nombre
 * @property int $tipo
 * @property string|null $descripcion
 * @property string $fecha_creacion
 * @property string|null $fecha_edicion
 */
class Cuestionario extends ActiveRecord
{

  public static function tableName()
  {
    return 'cuestionario';
  }

  public function behaviors()
  {
    return [
      FechaBehavior::class,
    ];
  }

  public function rules()
  {
    return [
      [['nombre'], 'required'],
      [['tipo'], 'default', 'value' => null],
      [['tipo'], 'integer'],
      [['fecha_creacion', 'fecha_edicion'], 'safe'],
      [['nombre'], 'string', 'max' => 64],
      [['descripcion'], 'string', 'max' => 512],
      [['descripcion_larga'], 'string'],

      [['respuesta_unica'], 'boolean'],
      [['cerrado_para_edicion'], 'boolean'],
      [['cerrado_para_respuestas'], 'boolean'],

      [['descripcion'], 'filter', 'filter' => function($value) {
        return preg_replace('/<p><br><\/p>$/', '', $value);
      }],
    ];
  }

  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'nombre' => 'Nombre',
      'tipo' => 'Tipo',
      'descripcion_larga' => 'Descripción',
      'fecha_creacion' => 'Fecha de creación',
      'fecha_edicion' => 'Fecha de edición',
      'respuesta_unica' => 'Límitar a una respuesta',
      'cerrado_para_edicion' => 'Cuestionario cerrado para edición de preguntas',
      'cerrado_para_respuestas' => 'Cuestionario cerrado para enviar respuestas',
    ];
  }

  public function afterSave($insert, $changedAttributes)
  {
    if ($insert) {
      $seccion = new Seccion([
        'nombre' => 'Preguntas',
        'cuestionario_id' => $this->id,
      ]);
      $seccion->save();
    }
    parent::afterFind($insert, $changedAttributes);
  }

  public function getPreguntas()
  {
    return $this->hasMany(Pregunta::class, ['cuestionario_id' => 'id']);
  }

  public function getSecciones()
  {
    return $this->hasMany(Seccion::class, ['cuestionario_id' => 'id']);
  }

  public function getRespuestas()
  {
    return $this->hasMany(Respuestas::class, ['cuestionario_id' => 'id']);
  }

  public function getResultados()
  {
    return $this->hasMany(Resultado::class, ['cuestionario_id' => 'id'])
      ->orderBy('desde');
  }

  public function getParametros()
  {
    return $this->hasMany(Parametro::class, ['tipo_parametro_id' => 'id'])
      ->via('tiposParametro')
      ->orderBy('nombre');
  }

  public function getTiposParametro()
  {
    return $this->hasMany(TipoParametro::class, ['cuestionario_id' => 'id'])
      ->andWhere(['AND', 'tipo_parametro_id IS NULL', 'parametro_id IS NULL'])
      ->orderBy('nombre');
  }
}
