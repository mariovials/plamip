<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\helpers\ArrayHelper;
use common\models\Pregunta;

/**
 * OrdenSearch represents the model behind the search form of `common\models\Pregunta`.
 */
class PreguntaSearch extends Pregunta
{
  public function rules()
  {
    return [
      [['cuestionario_id',
        'parametro',
        'seccion_id'], 'integer'],
      [['nombre'], 'string'],
    ];
  }

  public function attributeLabels()
  {
    return ArrayHelper::merge(parent::attributeLabels(), [
    ]);
  }

  public function scenarios()
  {
    return Model::scenarios();
  }

  public function search($params)
  {
    $query = Pregunta::find();
    $dataProvider = new ActiveDataProvider(['query' => $query]);
    $this->load($params);
    if (!$this->validate()) {
      $query->where('0=1');
    }
    if ($this->parametro) {
      $query->joinWith('preguntaParametros');
      $query->andFilterWhere(['parametro_id' => $this->parametro]);
    }
    $query->andFilterWhere(['AND',
      ['seccion_id' => $this->seccion_id],
      ['cuestionario_id' => $this->cuestionario_id],
      ['ILIKE', 'nombre', $this->nombre],
    ]);
    return $dataProvider;
  }
}
