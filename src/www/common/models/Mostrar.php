<?php

namespace common\models;
use backend\components\ActiveRecord;

use Yii;

/**
 * This is the model class for table "mostrar".
 *
 * @property int $id
 * @property int $opcion_id
 * @property int $pregunta_id
 *
 * @property Opcion $opcion
 * @property Pregunta $pregunta
 */
class Mostrar extends ActiveRecord
{
  public $preguntas = [];

  public static function tableName() {
    return 'mostrar';
  }

  public function rules()
  {
    return [
      [['opcion_id', 'pregunta_id'], 'required'],
      [['opcion_id', 'pregunta_id'], 'default', 'value' => null],
      [['opcion_id', 'pregunta_id'], 'integer'],
      [['opcion_id', 'pregunta_id'], 'unique', 'targetAttribute' => ['opcion_id', 'pregunta_id']],
      [['opcion_id'], 'exist', 'skipOnError' => true,
        'targetClass' => Opcion::class,
        'targetAttribute' => ['opcion_id' => 'id']],
      [['pregunta_id'], 'exist', 'skipOnError' => true,
        'targetClass' => Pregunta::class,
        'targetAttribute' => ['pregunta_id' => 'id']],
    ];
  }

  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'opcion_id' => 'Opción',
      'pregunta_id' => 'Pregunta',
    ];
  }

  public function getOpcion()
  {
    return $this->hasOne(Opcion::class, ['id' => 'opcion_id']);
  }

  public function getPregunta()
  {
    return $this->hasOne(Pregunta::class, ['id' => 'pregunta_id']);
  }
}
