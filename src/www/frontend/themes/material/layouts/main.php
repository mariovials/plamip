<?php

use frontend\assets\AppAsset;
use common\helpers\Html;
use yii\helpers\Url;

AppAsset::register($this);
$this->title = $this->title ? "$this->title | SMIP" : 'SMIP';
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
  <meta charset="<?= Yii::$app->charset ?>">
  <link rel="icon" type="image/x-icon" href="<?= Yii::getAlias('@web/favicon.ico') ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <?php $this->registerCsrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>
  <?php $this->head() ?>
</head>

<body>
<?php $this->beginBody() ?>

  <div id="pagina">

    <header id="header">
      <nav class="wrapper">

        <a id="logo" href="<?= Url::to(['/']) ?>">
          <img src="<?= Yii::getAlias('@web/img/logo.png') ?>" alt="">
        </a>

        <ul id="menu">

          <li>
            <a href="<?= Url::to(['/cuestionario']) ?>">Cuestionarios</a>
          </li>

          <li>
            <a href="<?= Url::to(['/resultados']) ?>">Resultados generales</a>
          </li>

          <?php if (Yii::$app->user->esValidador()): ?>
          <li>
            <a href="<?= Url::to(['/respuestas']) ?>">Resultados</a>
          </li>
          <?php endif ?>

          <?php if (!Yii::$app->user->isGuest): ?>
          <li>
            <a href="<?= Url::to(['/mis-resultados']) ?>">Mis resultados</a>
          </li>
          <?php endif ?>

          <?php if (Yii::$app->user->isGuest): ?>
          <li>
            <a href="<?= Url::to(['/sistema/ingresar']) ?>"
              class="btn" id="btn-ingresar">
              Ingresar
            </a>
          </li>
          <li style="margin: 0">
            <a href="<?= Url::to(['/usuario/registro']) ?>"
              class="btn-flat" id="btn-ingresar">
              Crear cuenta
            </a>
          </li>
          <?php else: ?>
          <li>
            <a href="#" id="btn-usuario">
              <?= Yii::$app->user->identity->nombre ?>
              <span class="mdi mdi-account-circle"></span>
            </a>
            <ul>
              <?php if (Yii::$app->user->esAdmin()): ?>
              <li>
                <a href="<?= Yii::$app->urlManagerBackend->createAbsoluteUrl(['/cuestionario']) ?>"
                  class="">
                  <span class="mdi mdi-cog"></span>
                  Administración
                </a>
              </li>
              <?php endif ?>
              <li>
                <a href="<?= Url::to(['/sistema/salir']) ?>"
                  class="" id="">
                  <span class="mdi mdi-logout"></span>
                  Salir
                </a>
              </li>
            </ul>
          </li>
          <?php endif ?>
        </ul>

      </nav>
    </header>

    <main id="main">
      <?= $content ?>
    </main>

    <footer id="footer">
      <div id="footer-content" class="wrapper">
        <div>
          <a href="https://www.ubiobio.cl" class="logo">
            <img src="<?= Yii::getAlias('@web/img/logo_ubb.png') ?>"
            alt="Logo Universidad del Bío-Bío">
            Universidad del Bío-Bío
          </a>
        </div>

        <div>
          <a href="https://www.labubb.cl/">LabUBB</a> ©
        </div>

        <div id="por">
          Por Mario Vial
        </div>

      </div>
    </footer>

  </div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage();
