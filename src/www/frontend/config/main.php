<?php
$params = array_merge(
  require __DIR__ . '/../../common/config/params.php',
  require __DIR__ . '/../../common/config/params-local.php',
  require __DIR__ . '/params.php',
  require __DIR__ . '/params-local.php'
);

return [
  'id' => 'frontend',
  'basePath' => dirname(__DIR__),
  'bootstrap' => ['log'],
  'controllerNamespace' => 'frontend\controllers',
  'defaultRoute' => 'sistema/portada',

  'components' => [

    'view' => [
      'theme' => [
        'basePath' => '@frontend/themes/material',
        'pathMap' => [
          '@frontend/views' => [
            '@frontend/themes/material',
            '@frontend/views',
          ],
        ],
      ],
    ],

    'request' => [
      'csrfParam' => '_csrf',
      'cookieValidationKey' => 'gFpvCwmTDAiSQ-Vio8lrfLIvMskzz4Ae',
    ],

    'errorHandler' => [
      'errorAction' => 'sistema/error',
    ],

  ],
  'params' => $params,
];
