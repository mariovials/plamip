$(function() {

  $('#btn-usuario').on('click', function(e) {
    menu = $(this).next()
    menu.toggle()
    $('body').off('click.menu-usuario')
    $('body').on('click.menu-usuario', function() {
      menu.hide()
      $('body').off('click.menu-usuario')
    })
    e.stopPropagation()
    e.preventDefault()
  })

})
