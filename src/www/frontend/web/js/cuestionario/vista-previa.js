$(function () {

  seccion = $('.seccion.activo')
  seccion.find('textarea').autoResize()

  $('.btn-siguiente').on('click', function() {
    seccion = $(this).closest('.seccion')
    seccion.hide()
    seccion.next().show()
    seccion.next().find('textarea').autoResize()
    setTimeout(function() {
      window.scrollTo({ top: 0 })
    }, 1)
  })

  $('.btn-atras').on('click', function() {
    seccion = $(this).closest('.seccion')
    seccion.hide()
    seccion.prev().show()
    seccion.prev().find('textarea').autoResize()
    setTimeout(function() {
      window.scrollTo({ top: 0 })
    }, 1)
  })

  registarRespuesta = function(pregunta, valor, opcion = null) {
    $.ajax({
      url: '/respuesta/registrar',
      data: {
        respuestas,
        pregunta,
        opcion,
        valor,
      },
    })
  }

  // textos
  // checkboxs
  $('#cuestionario :input.checkbox').on('change', function() {
    if ($(this).hasClass('editable')) {
      $(this).closest('label').next('.input').val('').toggle(this.checked).focus()
    }
  })
  // radios
  $('#cuestionario :input.radio').on('change', function() {
    editable = $(this).hasClass('editable')
    $(this).closest('.form-group').find('.input').val('').hide()
    if (editable) {
      $(this).closest('label').next('.input').show().focus();
    }
  })

})
