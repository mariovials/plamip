$(function () {

  seccion = $('.seccion.activo')
  seccion.find('textarea').autoResize()

  $.fn.validar = function() {
    this.each((i, e) => {
      tipo = $(e).find('.pregunta').data('tipo')
      switch (tipo) {
        case 'texto':
          val = $(e).find('textarea').val()
          break;
        case 'texto-multiple':
          var vacios = false;
          val = $(e).find('input').map(function() {
            if (this.value.trim() == '') {
              vacios = true
            }
            return $(this).val().trim() != ''
          }).get().join('')
          if (vacios) {
            val = ''
          }
          break;
        case 'opcion-unitaria':
          val = $(e).find('input:checked').map(function() { return $(this).next().text() }).get().join()
          break;
        case 'opcion-multiple':
          val = $(e).find('input:checked').map(function() { return $(this).parent().find('.label').text() }).get().join()
          break;
        default:
          break;
      }
      $(e).toggleClass('error', val.length == 0)
    })
  }

  $('.btn-terminar').on('click', function(e) {
    if (confirm("Ya no podrá editar sus respuestas.\nSe marcará como terminado el cuestionario.\n¿Está seguro?")) {
      seccion = $(this).closest('.seccion')
      seccion.find('.items .item:visible').validar()
      if (seccion.find('.items .item.error').length) {
        seccion.find('.items .item.error .mensaje-error').html('Debe contestar esta pregunta')
        seccion.find('.items .item.error').get(0).scrollIntoView({ behavior: 'smooth' })
        e.preventDefault()
      }
    } else {
      e.preventDefault()
    }
  })

  $('.btn-siguiente').on('click', function() {
    seccion = $(this).closest('.seccion')
    seccion.find('.items .item:visible').validar()
    if (seccion.find('.items .item.error').length) {
      seccion.find('.items .item.error .mensaje-error').html('Debe contestar esta pregunta')
      seccion.find('.items .item.error').get(0).scrollIntoView({ behavior: 'smooth' })
    } else {
      seccion.hide()
      seccion.next().show()
      seccion.next().find('textarea').autoResize()
      setTimeout(function() {
        window.scrollTo({ top: 0 })
      }, 1)
    }
  })

  $('.btn-atras').on('click', function() {
    seccion = $(this).closest('.seccion')
    seccion.hide()
    seccion.prev().show()
    seccion.prev().find('textarea').autoResize()
    setTimeout(function() {
      window.scrollTo({ top: 0 })
    }, 1)
  })

  registarRespuesta = function(pregunta, valor, opcion = null) {
    $.ajax({
      url: BASE_URL + 'respuesta/registrar',
      data: {
        respuestas,
        pregunta,
        opcion,
        valor,
      },
      success: function() {
        mostrarMensaje()
      }
    })
  }

  mensaje = $('#mensaje')
  function mostrarMensaje() {
    mensaje.show()
    setTimeout(() => {
      mensaje.hide()
    }, 1000)
  }

  // mostrar preguntas según opción
  $('.pregunta').each(function(i, pregunta) {
    pregunta = $(pregunta)
    mostrada = pregunta.data('mostrada') + ''
    if (mostrada.length) {
      opciones = mostrada.split(',')
      seMuestra = false
      for (const opcion_id of opciones) {
        opcion = $(`.opcion[data-id="${opcion_id}"] input`)
        if (opcion.is(':checked')) {
          pregunta.closest('.item').show()
          pregunta.find('textarea').autoResize()
          break
        } else {
          pregunta.closest('.item').hide()
        }
      }
    }
  })
  $('.opcion input').on('change', function() {
    respuesta = $(this).closest('.respuesta')
    var mostrar = []
    data = $(this).data('mostrar') + ''
    if (data.length) {
      mostrar = data.split(',')
      mostrar.forEach(function(pregunta_id) {
        pregunta = $(`.pregunta[data-id="${pregunta_id}"]`)
        pregunta.closest('.item').slideDown(200)

        pregunta.find(':input').val('')
        pregunta.find('input').prop('checked', false)

        pregunta.find('textarea').autoResize()
        pregunta.closest('.item')
          .find('.respuesta .opcion input:checked')
          .trigger('change')
      })
    }
    notChecked = respuesta.find('input').not(':checked')
    notChecked.each(function(i, input) {
      input = $(input)
      data = input.data('mostrar') + ''
      if (data.length) {
        data.split(',').forEach(function(pregunta_id) {
          if (!mostrar.includes(pregunta_id)) {
            pregunta = $(`.pregunta[data-id="${pregunta_id}"]`)
            pregunta.closest('.item').slideUp(100)

            pregunta.closest('.item').removeClass('error')
            pregunta.closest('.item').find('.respuesta .opcion input:checked').each(function(i, e) {
              ocultar = ($(e).data('mostrar') + '').split(',')
              if (ocultar.length) {
                ocultar.forEach(function(pregunta_id) {
                  pregunta = $(`.pregunta[data-id="${pregunta_id}"]`)
                  pregunta.closest('.item').slideUp(100)
                })
              }
            })
          }
        })
      }
    })
  })


  // textos
  $('#cuestionario textarea').on('input', function() {
    pregunta = $(this).closest('.pregunta').data('id')
    $(this).closest('.pregunta').find('.mensaje-error').html('')
    registarRespuesta(pregunta, $(this).val())
  })

  // checkboxs
  $('#cuestionario :input.checkbox').on('change', function() {
    $(this).closest('.pregunta').find('.mensaje-error').html('')
    if ($(this).hasClass('editable')) {
      $(this).closest('label').next('.input').val('').toggle(this.checked).focus()
    }
    pregunta = $(this).closest('.pregunta').data('id')
    opcion = $(this).closest('.opcion').data('id')
    valor = this.checked ? '1' : '0'
    registarRespuesta(pregunta, valor, opcion)
  })

  // radios
  $('#cuestionario :input.radio').on('change', function() {
    $(this).closest('.pregunta').find('.mensaje-error').html('')
    editable = $(this).hasClass('editable')
    $(this).closest('.form-group').find('.input').val('').hide()
    if (editable) {
      $(this).closest('label').next('.input').show().focus()
    }
    pregunta = $(this).closest('.pregunta').data('id')
    opcion = $(this).closest('.opcion').data('id')
    valor = this.checked ? '1' : '0'
    registarRespuesta(pregunta, valor, opcion)
  })

  // inputs
  $('#cuestionario :input.input').on('input', function() {
    $(this).closest('.pregunta').find('.mensaje-error').html('')
    pregunta = $(this).closest('.pregunta').data('id')
    opcion = $(this).data('opcion')
    valor = $(this).val()
    registarRespuesta(pregunta, valor, opcion)
  })

})
