<?php

namespace frontend\controllers;

use frontend\components\Controller;
use common\models\Usuario;
use Yii;

class UsuarioController extends Controller
{
  public function actionRegistro()
  {
    $model = new Usuario(['scenario' => 'registro']);
    if ($this->request->isPost) {
      if ($model->load($this->request->post()) && $model->save()) {
        return $this->redirect(['/sistema/ingresar',
          'to' => Yii::$app->request->get('to', ['/']),
        ]);
      }
    } else {
      $model->loadDefaultValues();
    }
    return $this->render('agregar', ['model' => $model]);
  }
}
