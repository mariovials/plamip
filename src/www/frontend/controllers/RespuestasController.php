<?php

namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use common\models\Respuestas;
use yii\filters\AccessControl;

class RespuestasController extends Controller
{

  public function behaviors()
  {
    return [
      'access' => [
        'only' => ['ver'],
        'class' => AccessControl::class,
        'rules' => [
          [
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
    ];
  }

  public function actionVer($id, $terminar = false)
  {
    $model = Respuestas::findOne($id);
    return $this->render('ver', [
      'model' => $model,
    ]);
  }

  public function actionPreguntas($id)
  {
    $model = Respuestas::findOne($id);
    return $this->render('preguntas', [
      'model' => $model,
    ]);
  }

  public function actionLista()
  {
    return $this->render('lista');
  }

  public function actionTerminar($id, $terminar = false)
  {
    $model = Respuestas::findOne($id);
    $model->fecha_termino = date('Y-m-d H:i:s');
    $model->save();
    return $this->redirect(['ver', 'id' => $model->id, 'terminar' => $terminar]);
  }
}
