<?php

namespace frontend\controllers;

use frontend\components\Controller;

class SobreController extends Controller
{
  public function actionLista()
  {
    return $this->render('lista');
  }
}
