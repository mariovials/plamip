<?php

namespace frontend\controllers;

use Yii;
use frontend\components\Controller;

class MisResultadosController extends Controller
{
  public function actionLista()
  {
    if (Yii::$app->user->isGuest)
      return $this->redirect(['/']);
    return $this->render('lista');
  }
}
