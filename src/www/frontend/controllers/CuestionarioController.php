<?php

namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use common\models\Cuestionario;
use common\models\Respuestas;
use yii\filters\AccessControl;

class CuestionarioController extends Controller
{

  public function behaviors()
  {
    return [
      'access' => [
        'only' => ['responder'],
        'class' => AccessControl::class,
        'rules' => [
          [
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
    ];
  }

  public function actionLista()
  {
    return $this->render('lista');
  }

  public function actionVer($id)
  {
    $model = Cuestionario::findOne($id);
    return $this->render('ver', [
      'model' => $model,
    ]);
  }

  public function actionResponder($id)
  {
    $model = Cuestionario::findOne($id);
    $respuestas = Respuestas::find()->where([
      'cuestionario_id' => $model->id,
      'usuario_id' => Yii::$app->user->id,
    ])
    ->orderBy('fecha_creacion DESC')
    ->one();
    $puede_responder = false;
    $nueva = false;
    if (!$model->cerrado_para_respuestas) {
      if ($respuestas) {
        if ($respuestas->fecha_termino) {
          $nueva = !$model->respuesta_unica;
        } else {
          $puede_responder = true;
        }
      } else {
        $nueva = true;
      }
    }
    if ($nueva) {
      $respuestas = new Respuestas();
      $respuestas->cuestionario_id = $model->id;
      $respuestas->usuario_id = Yii::$app->user->id;
      $respuestas->save();
      $puede_responder = true;
    }
    if ($puede_responder) {
      $respuestas->iniciar();
      return $this->render('responder', [
        'model' => $model,
        'respuestas' => $respuestas,
      ]);
    } else {
      return $this->render('pre_responder', [
        'model' => $model,
        'respuestas' => $respuestas,
      ]);
    }
  }

  public function actionVistaPrevia($id)
  {
    $model = Cuestionario::findOne($id);
    return $this->render('vista_previa', [
      'model' => $model,
    ]);
  }
}
