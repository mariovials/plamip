<?php

namespace frontend\controllers;

use frontend\components\Controller;

class ResultadosController extends Controller
{
  public function actionLista()
  {
    return $this->render('lista');
  }
}
