<?php

namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use common\models\Respuestas;
use common\models\Respuesta;
use common\models\RespuestaOpcion;
use common\models\Pregunta;
use common\models\Opcion;
use yii\filters\AccessControl;

class RespuestaController extends Controller
{

  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::class,
        'rules' => [
          [
            'actions' => ['registrar'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
    ];
  }

  public function actionRegistrar($respuestas, $pregunta, $valor, $opcion = null)
  {
    $respuestas = Respuestas::findOne($respuestas);
    $respuestas->updateAttributes(['fecha_edicion' => date('Y-m-d H:i:s')]);
    $pregunta = Pregunta::findOne($pregunta);
    $respuesta = Respuesta::findOne([
      'respuestas_id' => $respuestas->id,
      'pregunta_id' => $pregunta->id,
    ]);
    if (!$respuesta) {
      $respuesta = new Respuesta();
      $respuesta->respuestas_id = $respuestas->id;
      $respuesta->pregunta_id = $pregunta->id;
      $respuesta->orden = $pregunta->orden;
      $respuesta->puntaje = $pregunta->puntaje;
    }
    $respuesta->valor = $valor;
    $respuesta->save();
    $respuestas->updateAttributes(['fecha_edicion' => date('Y-m-d H:i:s')]);
    if ($opcion) {
      $opcion = Opcion::findOne($opcion);
      $respuestaOpcion = RespuestaOpcion::findOne([
        'respuesta_id' => $respuesta->id,
        'opcion_id' => $opcion->id,
      ]);
      if (!$respuestaOpcion) {
        $respuestaOpcion = new RespuestaOpcion();
        $respuestaOpcion->respuesta_id = $respuesta->id;
        $respuestaOpcion->opcion_id = $opcion->id;
        $respuestaOpcion->orden = $opcion->orden;
      }
      $respuestaOpcion->valor = $valor;
      $respuestaOpcion->save();
      return $this->asJson($respuestaOpcion->attributes);
    }
    return $this->asJson($respuesta->attributes);
  }
}
