<?php

use common\models\Cuestionario;
use common\models\Resultado;

$this->title = 'Resultados';

$cuestionarios = Cuestionario::find()->orderBy('id')->all();

?>

<div class="wrapper">

  <header>
    <h1 class="titulo"> Resultados generales </h1>
    <p>Representan el promedio de los cuestionarios aplicados </p>
  </header>

  <div id="resultados">
    <?php
    foreach ($cuestionarios as $cuestionario):
      $respuestas = $cuestionario
        ->getRespuestas()
        ->andWhere('fecha_termino IS NOT NULL')
        ->all();
      $total = 0;
      foreach ($respuestas as $respuesta) {
        if (!$respuesta->puntaje) {
          $respuesta->markAttributeDirty('fecha_termino');
          $respuesta->save();
        }
        $p = $respuesta->puntaje;
        $total += $p;
      }
      $promedio = ceil($total / count($respuestas));
      $resultado = Resultado::find()
        ->where(['AND',
          ['<=', 'desde', $promedio],
          ['>=', 'hasta', $promedio],
          ['cuestionario_id' => $cuestionario->id],
        ])
        ->orderBy('desde DESC')
        ->one();
    ?>
    <div class="cuestionario">

      <div class="header">
        <div class="informacion">
          <div class="nombre">
            <?= $cuestionario->nombre ?>
          </div>
        </div>
      </div>

      <div class="contenido">

        <div class="resultado-general">
          <div class="barra">
            <div class="pie animate" style="--p:<?= $promedio ?>;--c:#42A5F5"> <?= $promedio ?>% </div>
          </div>
          <div class="resultado">
            <?php if ($resultado): ?>
              <div style="font-size: 1.2em; font-weight: bold">
                <?= $resultado->nombre ?>
              </div>
              <div style="font-size: 1.02em">
                <?= nl2br($resultado->texto_promedio) ?>
              </div>
            <?php endif ?>
          </div>
        </div>

        <div class="parametros">


          <div class="header">
            <div class="informacion">
              <div class="nombre">
                Detalles por parámetro de medición
              </div>
            </div>
          </div>

          <ul>
            <?php foreach ($cuestionario->tiposParametro as $tipoParametro): ?>
              <?= $this->render('_li', [
                'cuestionario' => $cuestionario,
                'tipoParametro' => $tipoParametro,
                'parametro' => null,
                'profundidad' => 0,
                'n' => count($cuestionarios),
              ]) ?>
            <?php endforeach ?>
          </ul>
        </div>
      </div>
    </div>
    <?php endforeach ?>
  </div>

</div>

<script>
  botonesMostrar = document.querySelectorAll('.btn-ver-resultado')
  botonesMostrar.forEach(function(btn) {
    btn.addEventListener('click', function() {
      mdi = btn.querySelector('.mdi')
      mdi.classList.toggle("mdi-chevron-up")
      mdi.classList.toggle("mdi-chevron-down")
      resultado = this.closest('.resultado')
      resultado.classList.toggle('activo')
      li = btn.closest('li')
      if (li.querySelector('.detalles').style.display == 'block') {
        li.querySelector('.detalles').style.display = 'none'
        resultado.style.marginBottom = null
      } else {
        li.querySelector('.detalles').style.display = 'block'
        resultado.style.marginBottom = '1em'
      }
    })
  })
  btnsMostrarTodo = document.querySelectorAll('.btn-mostrar-todo')
  btnsMostrarTodo.forEach(function(btnMostrarTodo) {
    console.log(btnMostrarTodo)
    btnMostrarTodo.addEventListener('click', function() {
      this.classList.toggle('activo')
      var botones = btnMostrarTodo.closest('.cuestionario')
          .querySelectorAll('.btn-ver-resultado')
      if (this.classList.contains('activo')) {
        this.innerHTML = '<i class="mdi mdi-chevron-double-up"></i> Ocultar todo'
        botones.forEach(function(btn) {
          resultado = btn.closest('.resultado')
          if (!resultado.classList.contains('activo')) {
            btn.dispatchEvent(new Event('click'))
          }
        })
      } else {
        this.innerHTML = '<i class="mdi mdi-chevron-double-down"></i> Mostrar todo'
        botones.forEach(function(btn) {
          resultado = btn.closest('.resultado')
          if (resultado.classList.contains('activo')) {
            btn.dispatchEvent(new Event('click'))
          }
        })
      }
    })
  })
</script>



<style>

  #resultados {
  }

  .cuestionario {
  }
  .cuestionario .header {
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 0 1rem 0 0;
  }
  .cuestionario .header .informacion {
  }
  .cuestionario .header .informacion .nombre {
    font-size: 1.2em;
    padding: 0.5rem 1rem;
    font-weight: 600;
  }
  .resultado-general {
    display: flex;
    padding: 1em 0;
  }

  .resultado-general .barra {
    padding: 0 1em;
  }

  .parametros {
    padding-bottom: 5em;
  }
  .parametros ul li {
  }
  .parametros ul li .linea .nombre {
    display: flex;
    width: 55%;
    font-weight: 500;
  }
  .parametros ul li .linea .nombre .mdi {
    margin-right: 0.5em;
  }
  .parametros ul li .linea .nombre > div {
    margin-right: 1em;
  }
  .parametros ul li .detalles {
    padding: 1em 1.5em;
    background: #EEE;
    border-radius: 1em;
    margin: 0.5em 1em 0.5em 1em;
  }
  .parametros ul li .linea {
    display: flex;
    justify-content: space-between;
    padding: 0.2em 1em;
  }
  .parametros .resultado.activo:hover {
  }
  .parametros .resultado:hover {
  }
  .resultado.activo {
    padding-top: 0.3em;
    margin-top: -0.3em;
  }
  .resultado {
    border-radius: 1em;
    display: flex;
    flex-direction: column;
  }

  .porcentaje .barra {
  }
  .porcentaje {
    min-width: 20em;
    width: 45%;
    display: flex;
    justify-content: space-between;
    padding: 0 1em;
    height: 1.4em;
    color: #FFF;
    border-radius: 1em;
  }
  .btn-ver-resultado .mdi {
    line-height: 0.6em;
  }
  .btn-ver-resultado {
    white-space: nowrap;
    line-height: 1em;
    padding: 0.275rem 0.7rem;
    font-size: 0.85em;
    margin-left: 1em;
  }


  .porcentaje {
    position: relative;
  }

  .linea-33 {
    height: 1.4em;
    border-left: 1px solid #EEE;
    position: absolute;
    left: 33%;
  }

  .linea-66 {
    height: 1.4em;
    border-left: 1px solid #EEE;
    position: absolute;
    left: 66%;
  }

</style>
