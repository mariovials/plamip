<?php

use yii\helpers\Url;
use common\models\Respuestas;

$respuestas = Respuestas::find()->where('fecha_termino IS NOT NULL')
  ->orderBy('cuestionario_id, fecha_termino')
  ->all();

?>

<div class="wrapper">


  <header>
    <h1 class="titulo"> Mis resultados </h1>
  </header>

  <table>
    <thead>
      <tr>
        <th style="width: 15%"> Fecha</th>
        <th style="width: 35%"> Usuario </th>
        <th style="width: 30%"> Cuestionario </th>
        <th style="width: 20%"> </th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($respuestas as $respuesta): ?>
      <tr>
        <td><?= Yii::$app->formatter->asDateTime($respuesta->fecha_termino, 'corta') ?></td>
        <td class="usuario"><?= $respuesta->usuario->nombre ?></td>
        <td class="nombre"><?= $respuesta->cuestionario->nombre ?></td>
        <td class="botones">
          <a href="<?= Url::to(['/respuestas/ver', 'id' => $respuesta->id]) ?>" class="btn bajo">
            <span class="mdi mdi-chart-box"></span> Ver resultados
          </a>
          <a href="<?= Url::to(['/respuestas/preguntas', 'id' => $respuesta->id]) ?>" class="btn bajo">
            <span class="mdi mdi-text-short"></span> Ver respuestas
          </a>
        </td>
      </tr>
      <?php endforeach ?>
    </tbody>
  </table>

</div>

<style>
  .botones {
    white-space: nowrap;
    display: flex;
    column-gap: 0.4em;
  }
  #pagina {
    background-image: none;
  }
  th {
    text-align: left;
  }
  th, td {
    padding: 0.2em;
  }
  .btn-responder {
    background-color: #bc5957;
    color: #FFF;
  }
  .btn-responder:hover {
    background-color: #cc6967;
    color: #FFF;
  }
</style>
