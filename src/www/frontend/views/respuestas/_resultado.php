<?php

use common\helpers\Html;
use common\models\Rango;
use common\models\ParametroRango;

$parametros = $tipoParametro
  ->getParametros(isset($parametro) ? $parametro->id : null)
  ->all();

foreach ($parametros as $parametro):
  $max = $parametro->puntaje_maximo + 0;
  $rango = null;
  if ($max) {
    $puntaje = $parametro->getRespuestas($respuestas->id)->sum('puntaje') + 0;
    $p = $max ? floor($puntaje / $max * 100) : 100;
    $rango = Rango::find()
      ->where(['AND',
        ['<=', 'desde', $p],
        ['>=', 'hasta', $p],
        ['tipo_parametro_id' => $parametro->tipo_parametro_id],
      ])
      ->orderBy('desde DESC')
      ->one();
  } else {
    $p = 'Sin puntaje';
    $rango = Rango::find()
      ->where(['AND',
        ['tipo_parametro_id' => $parametro->tipo_parametro_id],
      ])
      ->one();
  }
  if ($rango) {
    $parametroRango = ParametroRango::find()
      ->where(['AND',
        ['rango_id' => $rango->id],
        ['parametro_id' => $parametro->id],
      ])
      ->one();
  }
?>
<li>

  <div class="resultado">
    <div class="linea">
      <div class="nombre">
        <span class="mdi mdi-<?= $parametro->tipoParametro->icono ?>"></span>
        <div> <?= $parametro->nombre ?> </div>
      </div>
      <div class="porcentaje"
      <?php if ($max): ?>
        style="background: linear-gradient(90deg, <?= $parametro->color ?> <?= $p ?>%, #CCC  <?= $p ?>%)"
      <?php else: ?>
        style="background: #CCC"
      <?php endif ?>
        >
        <div> <?= $max ? "{$p}%" : 'Parámetro sin puntaje' ?> </div>
        <div class="linea-33"></div>
        <div class="linea-66"></div>
      </div>
      <button class="btn-ver-resultado btn">
        <span class="mdi mdi-chevron-down"></span>
        Ver resultado
      </button>
    </div>
    <div class="detalles" style="display: none">
      <?php if ($rango): ?>
        <?php if ($max && $rango->nombre): ?>
          <b> <?= $rango->nombre ?> </b> <br>
        <?php endif ?>
        <?php if (isset($parametroRango)): ?>
          <?= nl2br($parametroRango->texto) ?>
        <?php endif ?>
      <?php else: ?>
        No está configurado su resultado
      <?php endif ?>
    </div>
  </div>

  <?php foreach ($parametro->tiposParametro as $subTipoParametro): ?>
  <ul style="margin-bottom: <?= $profundidad ?>em">
  <?= $this->render('_resultado', [
    'tipoParametro' => $subTipoParametro,
    'parametro' => $parametro,
    'respuestas' => $respuestas,
    'profundidad' => $profundidad + 0.5,
  ]) ?>
  </ul>
  <?php endforeach ?>
</li>
<?php endforeach; ?>
