<?php

use common\models\Resultado;
use yii\helpers\Url;

list($puntaje_total, $maximo_total) = $model->resultado();
$p = $maximo_total ? ceil($puntaje_total / $maximo_total * 100) : 100;
$resultado = Resultado::find()
  ->where(['AND',
    ['<=', 'desde', $p],
    ['>=', 'hasta', $p],
    ['cuestionario_id' => $model->cuestionario_id],
  ])
  ->orderBy('desde DESC')
  ->one();

?>

<div>&nbsp;</div>

<?php if (Yii::$app->request->get('terminar')): ?>
<div class="wrapper">
  <div class="gracias">
  <?php if ($p): ?>
    <b>¡Gracias por completar esta encuesta sobre el grado de innovación pública de su municipio!</b>
    <br>Apreciamos su dedicación y voluntad como funcionarios públicos en participar.
    <br>La innovación en los gobiernos locales es esencial para mejorar la eficiencia, transparencia y calidad de los servicios públicos.
    <br><br>Sus respuestas son clave para identificar áreas de mejora y desarrollar soluciones innovadoras que beneficiarán a su comunidad.
  <?php else: ?>
    <b>Lamentablemente, no ha podido continuar debido a que las respuestas indican que no se ha alcanzado el grado mínimo de innovación.</b>
    <br><br>
    La innovación es un camino continuo y desafiante, pero fundamental para mejorar la eficiencia, transparencia y calidad de los servicios públicos.
    <br>Animamos a su municipio a seguir trabajando en esta área.
    <br><br><b>Agradecemos sinceramente su tiempo y dedicación.
    <br>Su participación es muy valiosa para comprender los nuevos desafíos.</b>
  <?php endif ?>
  </div>
</div>
<?php endif ?>

<?php if ($p): ?>

<div class="wrapper">

  <header style="display: flex; justify-content: space-between;">
    <h1 class="titulo">
      Resultados para <?= $model->cuestionario->nombre ?>
      de <?= $model->usuario->nombre ?> <?= $model->usuario->institucion ?>
    </h1>
    <?php if (Yii::$app->user->esValidador()): ?>
    <a href="<?= Url::to(['/respuestas/preguntas', 'id' => $model->id]) ?>" class="btn bajo">
      <span class="mdi mdi-text-short"></span> Ver respuestas
    </a>
    <?php endif ?>
  </header>

</div>

<div class="wrapper">

  <div id="resultado-general">
    <div class="barra">
      <div class="pie animate" style="--p:<?= $p ?>;--c:#42A5F5"> <?= $p ?>% </div>
    </div>
    <div class="resultado">
      <?php if ($resultado): ?>
        <div style="font-size: 1.2em; font-weight: bold">
          <?= $resultado->nombre ?>
        </div>
        <div style="font-size: 1.02em">
          <?= nl2br($resultado->texto) ?>
        </div>
      <?php endif ?>
    </div>
  </div>

</div>

<div class="wrapper">

  <br>
  <header id="header-detalles">
    <h2 class="titulo"> Detalles </h2>
    <div class="botones">
      <button class="btn bajo" id="btn-print" onclick="print()">
        <i class="mdi mdi-printer"></i>
        Imprimir o PDF
      </button>
      <button class="btn bajo" id="btn-mostrar-todo">
        <i class="mdi mdi-chevron-double-down"></i>
        Mostrar todo
      </button>
    </div>
  </header>

  <div id="respuestas">

    <?php foreach ($model->cuestionario->tiposParametro as $tipoParametro): ?>
    <ul>
      <?= $this->render('_resultado', [
        'respuestas' => $model,
        'tipoParametro' => $tipoParametro,
        'parametro' => null,
        'profundidad' => 0,
      ]) ?>
    </ul>
    <?php endforeach; ?>

  </div>

</div>
<?php endif ?>

<script>
  botones = document.querySelectorAll('.btn-ver-resultado')
  btnMostrarTodo = document.getElementById('btn-mostrar-todo')
  btnMostrarTodo.addEventListener('click', function() {
    this.classList.toggle('activo')
    if (this.classList.contains('activo')) {
      this.innerHTML = '<i class="mdi mdi-chevron-double-up"></i> Ocultar todo'
      botones.forEach(function(btn) {
        resultado = btn.closest('.resultado')
        if (!resultado.classList.contains('activo')) {
          btn.dispatchEvent(new Event('click'))
        }
      })
    } else {
      this.innerHTML = '<i class="mdi mdi-chevron-double-down"></i> Mostrar todo'
      botones.forEach(function(btn) {
        resultado = btn.closest('.resultado')
        if (resultado.classList.contains('activo')) {
          btn.dispatchEvent(new Event('click'))
        }
      })
    }
  })
  botones.forEach(function(btn) {
    btn.addEventListener('click', function() {
      mdi = btn.querySelector('.mdi')
      mdi.classList.toggle("mdi-chevron-up")
      mdi.classList.toggle("mdi-chevron-down")
      resultado = this.closest('.resultado')
      resultado.classList.toggle('activo')
      li = btn.closest('li')
      if (li.querySelector('.detalles').style.display == 'block') {
        li.querySelector('.detalles').style.display = 'none'
      } else {
        li.querySelector('.detalles').style.display = 'block'
      }
    })
  })
</script>

<style>

  .porcentaje {
    position: relative;
  }

  .linea-33 {
    height: 1.4em;
    border-left: 1px solid #EEE;
    position: absolute;
    left: 33%;
  }

  .linea-66 {
    height: 1.4em;
    border-left: 1px solid #EEE;
    position: absolute;
    left: 66%;
  }

  #pagina {
    background-image: none;
  }

  @media print {
    #menu {
      display: none;
    }
    #por {
      display: none;
    }
    .botones {
      display: none !important;
    }
    #respuestas ul li .resultado {
      break-inside: avoid;
      margin-bottom: 1em;
      padding-top: 1em;
    }
    #respuestas ul li .detalles {
      display: block !important;
    }
    .btn-ver-resultado {
      display: none !important;
    }
    #footer {
      padding: 1em 3em;
    }
    * {
        -webkit-print-color-adjust: exact !important;   /* Chrome, Safari 6 – 15.3, Edge */
        color-adjust: exact !important;                 /* Firefox 48 – 96 */
        print-color-adjust: exact !important;           /* Firefox 97+, Safari 15.4+ */
    }
  }

  #resultado-general {
    display: flex;
  }
  #resultado-general .barra {
    min-width: 6em;
    max-width: 6em;
    margin: 0 1em;
    font-weight: bold;
    font-size: 1.8em;
    display: flex;
    align-items: flex-start;
    justify-content: center;
  }
  #respuestas {
    padding-bottom: 5em;
  }
  #respuestas ul li {
  }
  #respuestas ul li .linea .nombre {
    display: flex;
    width: 55%;
    font-weight: 500;
  }
  #respuestas ul li .linea .nombre .mdi {
    margin-right: 0.5em;
  }
  #respuestas ul li .linea .nombre > div {
    margin-right: 1em;
  }
  #respuestas ul li .detalles {
    padding: 1em 1.5em;
    background: #EEE;
    border-radius: 1em;
    margin: 0.5em 1em 0.5em 1em;
  }
  #respuestas ul li .linea {
    display: flex;
    justify-content: space-between;
    padding: 0.2em 1em;
  }
  .flex div {
    margin-left: 1em;
  }
  .flex {
    display: flex;
  }
  .porcentaje .barra {
  }
  .porcentaje {
    min-width: 20em;
    width: 45%;
    display: flex;
    justify-content: space-between;
    padding: 0 1em;
    height: 1.4em;
    color: #FFF;
    border-radius: 1em;
  }
  .btn-ver-resultado .mdi {
    line-height: 0.6em;
  }
  .btn-ver-resultado {
    white-space: nowrap;
    line-height: 1em;
    padding: 0.275rem 0.7rem;
    font-size: 0.85em;
    margin-left: 1em;
  }
  #respuestas .resultado.activo:hover {
    outline: 1px solid #CCC;
  }
  #respuestas .resultado:hover {
    background: #FAFAFA;
  }
  .resultado.activo {
    padding-top: 0.3em;
    margin-top: -0.3em;
  }
  .resultado {
    border-radius: 1em;
    display: flex;
    flex-direction: column;
  }
  #main #header-detalles {
    display: flex;
    width: 100%;
    justify-content: space-between;
    padding: 0 1em 1em 1em;
  }
  .gracias {
    margin: 1em 2em 2em 0;
    background: #e3f2fd;
    padding: 1.2em 2em 1.5em 2em;
    border-radius: 1em;
    box-shadow: 0 0 1em -0.3em #777;
    background-clip: border-box;
  }
  .botones {
    display: flex;
  }
  #btn-print {
    background: #BBDEFB;
  }
  .botones button {
    margin-left: 1em;
  }
</style>

<?php
/**
 *



    <?php foreach ($respuestas->cuestionario
      ->getSecciones()
      ->orderBy('orden')
      ->all() as $seccion): ?>
    <?php foreach ($seccion
      ->getPreguntas()
      ->joinWith('preguntaParametros')
      ->andWhere(['parametro_id' => $parametro->id])
      ->orderBy('orden')
      ->all() as $pregunta): ?>
      <?= $pregunta->nombre ?> <br>
    <?php endforeach ?>
    <?php endforeach ?>

 */
