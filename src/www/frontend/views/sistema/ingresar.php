<?php

use common\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = 'Ingresar';

?>

<div id="ingresar" class="wrapper">

  <div id="descripcion">
    <header>
      Plataforma de Medición de la Innovación Pública
    </header>
    <div class="contenido">
      <b> ¡Bienvenido a nuestra plataforma de medición de la innovación pública! </b>
      <br>
      <br>
      <p>
        Crea tu cuenta y explora esta herramienta para descubrir, evaluar y medir la innovación. Ingresa tus datos para revisar resultados generales de innovación en gobiernos locales o para medir y evaluar tu propia innovación.
      </p>
    </div>
    <div id="preheader">
      <span style="font-weight: 400"
        >Lab</span><span  style="font-weight: 800"
        >UBB</span>
    </div>
  </div>

  <div id="caja">
    <div id="logo">
      <a href="<?= Url::to(['/']) ?>">
        <img src="<?= Yii::getAlias('@web/img/logo.png') ?>" alt="">
      </a>
    </div>
    <?php $form = ActiveForm::begin(); ?>
    <div class="form">
      <div class="fieldset">
        <div class="filas">
          <div class="fila">
            <div class="campo grande">
              <i class="mdi mdi-account"></i>
              <?= $form->field($model, 'usuario'); ?>
            </div>
          </div>
          <div class="fila">
            <div class="campo grande">
              <i class="mdi mdi-lock"></i>
              <?= $form->field($model, 'contrasena')->passwordInput(); ?>
            </div>
          </div>
          <div class="fila">
            <div class="campo center grande" style="padding: 0;">
              <?= $form->field($model, 'recordarme')->checkbox(); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="boton">
      <button class="btn solo" id="btn-ingresar"> Ingresar </button>
    </div>
    <div id="nuevo">
      <a href="<?= Url::to(['/usuario/registro', 'to' => Yii::$app->user->returnUrl]) ?>"
        class="" id="btn-crear-usuario">
        Crear cuenta
      </a>
    </div>
    <?php ActiveForm::end(); ?>
  </div>

</div>

<style>
  .campo.center .form-group > label {
    display: flex;
    justify-content: center;
  }
  .hidden {
    display: none;
  }
  #pagina {
    overflow-y: scroll;
  }
  #preheader {
    font-size: 2em;
    font-weight: 900;
    padding: 6rem 0 0 0;
  }
  #descripcion {
    border-radius: 1em;
    box-shadow: 0 0.1em 1em -0.3em #000;
    padding: 1em 4em;
    color: #20364c;
    background: #FFFFFFAA;
    text-align: center;
  }
  #descripcion header {
    font-size: 2em;
    font-weight: bold;
    padding: 1em 2em;
    text-align: center;
    font-weight: 900;
  }
  #ingresar {
    display: flex;
    column-gap: 2em;
    width: 100%;
    margin-top: 2em;
  }
  #nuevo {
    display: flex;
    justify-content: center;
    padding: 2em 0;
  }
  #nuevo a:not(:last-child) {
    margin-right: 2em;
  }
  #main {
    background: none;
  }


  #caja {
    background: #FFF;
    box-shadow: 0 0.1em 1em -0.3em #0007;
    border-radius: 1em;
    margin: auto;
    min-width: 30rem;
    width: 30%;
    max-width: 35rem;
    padding: 2em;
  }
  #caja #logo {
    margin-bottom: 2em;
    padding: 0 4em;
  }
  #caja #logo img {
    display: block;
    height: 4rem;
    margin: 0 auto;
  }
  #boton {
    display: flex;
    justify-content: center;
    padding: 0.5em 0.5em 0em;
  }
  #caja #btn-ingresar {
    background: #bc5957;
    color: #FFF;
    padding-left: 3em;
    padding-right: 3em;
    font-size: 1rem;
  }
  #caja #btn-ingresar:hover {
    background: #cc6967;
  }

  .form .fieldset {
    margin-bottom: 0;
    border: 0;
  }
  .form .filas .fila:not(:last-child) {
    margin-bottom: 0;
  }
</style>
