<?php
?>

<div class="wrapper" id="portada">

  <header>
    <h1 class="titulo">
      Plataforma de Medición de la Innovación Pública
    </h1>
  </header>

  <div id="columnas" class="subwrapper">

    <div class="columna">
      <div class="imagen">
        <img src="<?= Yii::getAlias('@web/img/equipo_1.jpg') ?>" alt="">
      </div>
      <div class="texto">
        <h2>
          ¡Caminemos juntos hacia el fortalecimiento de una gestión Pública local y hacia la instalación de una cultura de Innovación!
        </h2>
        <p>
          La Plataforma Métrica Pública busca que los Gobiernos Locales puedan medir el nivel de Innovación actual en su gestión y de las iniciativas de innovación que han implementado durante estos últimos años. Se medirán indicadores claves que corresponden a las dimensiones de Gestión Pública local, Gobernanza Democrática y Desarrollo Territorial y cada una de sus variables, así como los Recursos e Insumos que disponen para iniciar sus innovaciones, los Procesos y Estrategias utilizados durante sus innovaciones y los Resultados que obtienen al innovar.
        </p>
      </div>
    </div>

    <div class="columna">
      <div class="texto">
        <h2>
          ¿Por qué es importante medir la Innovación Pública en los Gobiernos Locales?
        </h2>
        <p>
          Actualmente no existe una plataforma que cumpla este objetivo. Por lo mismo, Metrica Pública es una herramienta web que los Gobiernos locales de la Región del Bío Bio tendrán disponible para medir su nivel de innovación y comprender cuáles son las fortalezas y/o debilidades en su gestión, y puedan preguntarse cómo poder avanzar en mejoras.
        </p>
      </div>
      <div class="imagen">
        <img src="<?= Yii::getAlias('@web/img/equipo_2.jpg') ?>" alt="">
      </div>
    </div>

  </div>
  <br>
  <br>
</div>

<style>
  #portada header {
    display: flex;
    justify-content: center;
    padding: 1.5em 0;
  }
  #portada header h1 {
    background: #20364c;
    color: #FFF;
    padding: 1em 2em;
    border-radius: 3em;
    width: fit-content;
  }
  #columnas {
    display: flex;
    width: 100%;
    column-gap: 2em;
  }
  #columnas .columna {
    display: flex;
    flex-direction: column;
    width: 50%;
  }
  #columnas .columna .imagen {
    display: flex;
  }
  #columnas .columna .imagen img {
    width: 100%;
    border-radius: 1rem;
  }
  #columnas .columna .texto {
    padding: 1em;
  }
  #columnas .columna .texto h2 {
    font-size: 1.05em;
    margin-bottom: 0.5rem;
    font-weight: 600;
  }
  #columnas .columna .texto p {
    font-size: 0.95rem;
    line-height: 1.8em;
  }
</style>
