<?php

use common\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = 'Crear cuenta';
$this->params['icono'] = 'account';
$this->params['links'] = [
  ['label' => 'Usuarios', 'url' => ['/usuario']],
  'Agregar',
];

?>

<div id="registro" class="wrapper">

  <div id="caja">
    <div id="logo">
      <a href="<?= Url::to(['/']) ?>">
        <img src="<?= Yii::getAlias('@web/img/logo.png') ?>" alt="">
      </a>
    </div>
    <div class="form">
      <?php $form = ActiveForm::begin(); ?>
      <div class="filas">
        <div class="fila completa" style="margin-bottom: 0">
          <div class="campo ">
            <span class="mdi mdi-account"></span>
            <?= $form->field($model, 'nombre'); ?>
          </div>
        </div>
        <div class="fila completa">
        </div>
        <div class="fila completa">
          <div class="campo grande">
            <span class="mdi mdi-at"></span>
            <?= $form->field($model, 'correo_electronico'); ?>
          </div>
        </div>
        <div class="fila completa">
          <div class="campo grande">
            <span class="mdi mdi-bank"></span>
            <?= $form->field($model, 'institucion'); ?>
          </div>
        </div>
        <div class="fila completa">
          <div class="campo ">
            <span class="mdi mdi-lock"></span>
            <?= $form->field($model, 'contrasena')->passwordInput(); ?>
          </div>
          <div class="campo ">
            <?= $form->field($model, 'confirmar_contrasena')->passwordInput(); ?>
          </div>
        </div>
      </div>
      <div class="opciones">
        <div class="opcion">
          <button class="btn" id="btn-crear-usuario">
            Crear cuenta
          </button>
        </div>
        <div class="opcion">
          <a href="<?= Url::to(['/']) ?>" class="btn-flat solo">
            Cancelar
          </a>
        </div>
      </div>
      <?php ActiveForm::end(); ?>
    </div>
  </div>

  <div id="descripcion">
    <img src="<?= Yii::getAlias('@web/img/icono_b.png') ?>" alt="">
    <header>
      Plataforma de Medición de la Innovación Pública
    </header>
  </div>

</div>

<style>
  #pagina {
    overflow-y: scroll;
  }
  #descripcion img {
    width: 10em;
    margin: 7em auto 2em;
  }
  #descripcion {
    padding: 1em 2em;
    width: 50%;
    background: #20364c;
    box-shadow: 0 0.1em 1em -0.3em #000;
    border-radius: 1em;
    color: #FFF;
    display: flex;
    flex-direction: column;
    align-items: center;
    text-align: center;
  }
  #descripcion header {
    font-size: 1.8em;
    font-weight: bold;
    padding: 1em 2em;
    text-align: center;
    font-weight: 700;
  }
  #registro {
    display: flex;
    width: 100%;
    margin-top: 2em;
    column-gap: 2em;
  }
  #main {
    background: none;
    padding-bottom: 2em;
  }
  #caja #logo {
    margin-bottom: 2em;
  }
  #caja #logo img {
    display: block;
    height: 4rem;
    margin: 0 auto;
  }

  #caja {
    background: #FFF;
    box-shadow: 0 0 1em 0em #0007;
    border-radius: 1em;
    margin: auto;
    min-width: 35rem;
    width: 40rem;
    max-width: 45rem;
    padding: 2em;
  }
  #btn-crear-usuario {
    background: #bc5957;
    color: #FFF;
  }
  .form {
    margin: auto;
  }
  .form .opciones {
    background: #FFF;
    border: 0;
  }
</style>
