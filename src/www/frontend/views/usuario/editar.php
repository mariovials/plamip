<?php
use common\widgets\ActiveForm;

$this->title = "Editar $model->nombre";

$this->params['icono'] = 'account';
$this->params['links'] = [
  ['label' => 'Usuarios', 'url' => ['/usuario']],
  ['label' => $model->nombre, 'url' => $model->url],
  'Editar',
];
?>

<div class="form">
  <?php $form = ActiveForm::begin(); ?>
  <div class="filas">
    <div class="fila">
      <div class="campo">
        <span class="mdi mdi-account"></span>
        <?= $form->field($model, 'nombre'); ?>
      </div>
    </div>
    <div class="fila">
      <div class="campo">
        <span class="mdi mdi-bank"></span>
        <?= $form->field($model, 'institucion'); ?>
      </div>
    </div>
    <div class="fila">
      <div class="campo">
        <span class="mdi mdi-email"></span>
        <?= $form->field($model, 'correo_electronico'); ?>
      </div>
      <div class="campo">
        <?php $model->contrasena = '' ?>
        <?= $form->field($model, 'contrasena'); ?>
      </div>
    </div>
    <div class="fila">
      <div class="campo">
        <span class="mdi mdi-account-outline"></span>
        <?= $form->field($model, 'estado')
          ->dropDownList(['1' => 'Habilitado', '2' => 'Deshabilitado'],
            ['prompt' => 'Seleccione...']); ?>
      </div>
    </div>
  </div>
  <div class="opciones">
    <div class="opcion">
      <button class="btn">
        <span class="mdi mdi-<?= $model->isNewRecord ? 'plus-thick' : 'pencil' ?>"></span>
        <?= $model->isNewRecord ? 'Agregar' : 'Guardar' ?>
      </button>
    </div>
    <div class="opcion">
      <a href="<?= Yii::$app->request->get('from', '/usuario') ?>"
        class="btn-flat solo">
        Cancelar
      </a>
    </div>
  </div>
  <?php ActiveForm::end(); ?>
</div>
