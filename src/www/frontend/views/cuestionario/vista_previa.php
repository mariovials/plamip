<?php

use common\models\Pregunta;
use common\models\Respuesta;
use common\models\RespuestaOpcion;
use common\widgets\ActiveForm;

$this->title = $model->nombre;

$this->registerJs("
  const cuestionario = $model->id
", $this::POS_BEGIN);

$this->registerCssFile('@web/css/cuestionario/ver.css');
$this->registerJsFile('@web/js/cuestionario/vista-previa.js', [
  'depends' => [
    'common\assets\MaterialFormAsset',
    'yii\web\JqueryAsset',
  ],
]);

$secciones = $model->getSecciones()->orderBy('orden')->all();
$cantidad = count($secciones);

?>

<div id="cuestionario-header">
  <div class="wrapper">
    <img src="<?= Yii::getAlias('@web/img/logo.png') ?>" alt="">
    <div class="nombre">
      <?= $model->nombre; ?>
    </div>
    <a class="btn" id="btn-cerrar" onclick="window.close()">
      <span class="mdi mdi-close"></span>
      Cerrar
    </a>
  </div>
</div>

<div id="cuestionario" class="wrapper ">
  <?php
  foreach ($secciones as $i => $seccion):
    $ultimo = $i + 1 == $cantidad;
    $p = ($i + 1) / $cantidad * 100;
  ?>
  <div class="ficha lista seccion <?= $i == 0 ? 'activo' : '' ?>" id="seccion-<?= $seccion->id ?>" data-id="<?= $seccion->id ?>">
    <header>
      <div class="principal">
        <div class="titulo"> <div class="nombre"> <?= $seccion->nombre; ?> </div> </div>
      </div>
      <div class="opciones">
        Sección <?= $i + 1 ?> de <?= $cantidad ?>
      </div>
    </header>
    <main>
      <div class="lista">
        <div class="items">

          <?php $form = ActiveForm::begin(); ?>
          <?php foreach ($seccion->getPreguntas()->orderBy('orden')->all() as $pregunta): ?>
          <div class="item">

            <div class="pregunta" data-id="<?= $pregunta->id ?>">
              <div class="numero">
                <?= $pregunta->orden; ?>.
              </div>
              <div class="contenido">
                <div class="titulo">
                  <div class="nombre">  <?= $pregunta->nombre; ?> </div>
                  <div class="descripcion"><?= nl2br($pregunta->descripcion); ?></div>
                </div>
                <div class="respuesta">

                  <?php if ($pregunta->tipo == Pregunta::TIPO_TEXTO): ?>

                    <?php if ($pregunta->multiple): ?>
                      <?php foreach ($pregunta->getOpciones()->orderBy('orden')->all() as $opcion): ?>

                      <div class="opcion" data-id="<?= $opcion->id ?>" style="margin-top: 1em">
                        <label style="font-size: 0.9em"><?= $opcion->nombre ?></label>
                        <div class="form-group">
                          <input
                            data-opcion="<?= $opcion->id ?>"
                            class="input"
                            value=""
                            type="<?= $pregunta->numerico ? 'number' : 'text' ?>">
                        </div>
                      </div>
                      <?php endforeach ?>

                    <?php else: ?>
                      <div class="form-group">
                        <textarea placeholder="Respuesta..."></textarea>
                      </div>
                    <?php endif ?>

                  <?php else: ?>

                    <?php if ($pregunta->multiple): ?>
                      <?php foreach ($pregunta->getOpciones()->orderBy('orden')->all() as $opcion): ?>

                      <div class="form-group checkbox">
                        <label class="opcion" data-id="<?= $opcion->id ?>">
                          <input
                            type="checkbox"
                            class="checkbox <?= $opcion->editable ? 'editable' : '' ?>"
                            >
                          <div class="box"></div>
                          <div class="label"><?= $opcion->nombre ?></div>
                        </label>
                        <?php if ($opcion->editable): ?>
                        <input
                          style="display: none"
                          data-opcion="<?= $opcion->id ?>"
                          class="input"
                          value=""
                          type="<?= $pregunta->numerico ? 'number' : 'text' ?>">
                        <?php endif ?>
                      </div>
                      <?php endforeach ?>

                    <?php else: ?>
                      <div class="form-group radiolist">
                        <div id="opcion-nombre" role="radiogroup">
                          <?php foreach ($pregunta->getOpciones()->orderBy('orden')->all() as $opcion): ?>
                          <label class="opcion" data-id="<?= $opcion->id ?>">
                            <input
                              type="radio"
                              class="with-gap radio <?= $opcion->editable ? 'editable' : '' ?>"
                              name="pregunta-<?= $pregunta->id ?>">
                            <span><?= $opcion->nombre ?></span>
                          </label>
                          <?php if ($opcion->editable): ?>
                          <input
                            style="display: none"
                            data-opcion="<?= $opcion->id ?>"
                            class="input"
                            value=""
                            type="<?= $pregunta->numerico ? 'number' : 'text' ?>">
                          <?php endif ?>
                          <?php endforeach ?>
                        </div>
                      </div>
                    <?php endif ?>

                  <?php endif ?>
                </div>
              </div>
            </div>

            <div class="ayuda">
              <?= nl2br($pregunta->ayuda); ?>
            </div>

          </div>
          <?php endforeach ?>
          <?php ActiveForm::end(); ?>

        </div>
      </div>
    </main>
    <footer>
      <div class="opciones">
        <?php if ($i > 0): ?>
        <div class="opcion">
          <div class="btn-flat btn-atras">
            <span class="mdi mdi-arrow-left"></span>
            Atrás
          </div>
        </div>
        <?php endif ?>
        <?php if ($ultimo): ?>
        <div class="opcion">
          <button class="btn btn-terminar">
            Terminar
          </button>
        </div>
        <?php else: ?>
        <div class="opcion">
          <button class="btn icon-right btn-siguiente">
            Siguiente <span class="mdi mdi-arrow-right"></span>
          </button>
        </div>
        <?php endif ?>
        <div id="progreso"
          style="background: linear-gradient(90deg,
            rgba(21,  101, 192, 1) <?= $p ?>%,
            rgba(207, 216, 220, 1) <?= $p ?>%)">
        </div>
        <div>
          Sección <?= $i + 1 ?> de <?= $cantidad ?>
        </div>
      </div>
    </footer>
  </div>
  <?php endforeach ?>
</div>
