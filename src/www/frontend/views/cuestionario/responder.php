<?php

use common\models\Usuario;
use common\models\Pregunta;
use common\models\Respuesta;
use common\models\RespuestaOpcion;
use common\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = $model->nombre;
$respuestas->updateAttributes(['fecha_edicion' => date('Y-m-d H:i:s')]);

$BASE_URL = Url::to(['/']);
$this->registerJs("
  const BASE_URL = '$BASE_URL'
  const respuestas = $respuestas->id
  const cuestionario = $model->id
", $this::POS_BEGIN);

$this->registerCssFile('@web/css/cuestionario/ver.css?v=2');
$this->registerJsFile('@web/js/cuestionario/responder.js?v=4', [
  'depends' => [
    'common\assets\MaterialFormAsset',
    'yii\web\JqueryAsset',
  ],
]);

$secciones = $model->getSecciones()->orderBy('orden')->all();
$cantidad = count($secciones);

?>

<div id="mensaje">Respuesta guardada <i class="mdi mdi-check-circle"></i></div>
<div id="resultado"></div>

<div id="cuestionario-header">
  <div class="wrapper">
    <img src="<?= Yii::getAlias('@web/img/logo.png') ?>" alt="">
    <div class="nombre">
      <?= $model->nombre; ?>
    </div>
    <div style="font-weight: 600; white-space: nowrap">
      <?= Yii::$app->user->identity->nombre; ?>
    </div>
  </div>
</div>

<div id="cuestionario" class="wrapper ">
  <?php
  foreach ($secciones as $i => $seccion):
    $ultimo = $i + 1 == $cantidad;
    $p = ($i + 1) / $cantidad * 100;
  ?>
  <div class="ficha lista seccion <?= $i == 0 ? 'activo' : '' ?>" id="seccion-<?= $seccion->id ?>" data-id="<?= $seccion->id ?>">
    <header>
      <div class="principal">
        <div class="titulo"> <div class="nombre"> <?= $seccion->nombre; ?> </div> </div>
      </div>
      <div class="opciones">
        Sección <?= $i + 1 ?> de <?= $cantidad ?>
      </div>
    </header>
    <main>
      <div class="lista">
        <div class="items">

          <?php $form = ActiveForm::begin(); ?>
          <?php foreach ($seccion->getPreguntas()->orderBy('orden')->all() as $pregunta): ?>
          <?php
          $respuesta = Respuesta::findOne([
            'respuestas_id' => $respuestas->id,
            'pregunta_id' => $pregunta->id,
          ]);
          if (!$respuesta) {
            throw new Exception("No existe la respuesta para la pregunta $pregunta->id");
          }
          ?>
          <div class="item">

            <div class="pregunta"
              data-id="<?= $pregunta->id ?>"
              data-tipo="<?= $pregunta->tipoDiferenciado ?>"
              data-valor="<?= $respuesta->valor ?>"
              data-mostrada="<?= implode(',', $pregunta->getMostrar()->select('opcion_id')->column()) ?>">
              <div class="numero" style="display: none">
                <?= $pregunta->orden; ?>.
              </div>
              <div class="contenido">
                <div class="titulo">
                  <div class="nombre"> <?= $pregunta->nombre; ?> </div>
                  <div class="descripcion"><?= nl2br($pregunta->descripcion); ?></div>
                  <?php if (Yii::$app->user->identity->tipo == Usuario::ADMIN): ?>
                  <div class="descripcion">
                    <?php foreach ($pregunta->parametros as $parametro): ?>
                    <div class="campo" style="display: inline-block; margin-bottom: 0.2em">
                      <div class="chip parametro" style="
                        background: <?= $parametro->color ?>;
                        color: #FFFFFF">
                        <?= $parametro->icono() ?>
                        <?= $parametro->nombre ?>
                      </div>
                    </div>
                    <?php endforeach ?>
                  </div>
                  <?php endif ?>
                </div>
                <div class="respuesta">

                  <?php if ($pregunta->tipo == Pregunta::TIPO_TEXTO): ?>

                    <?php if ($pregunta->multiple): ?>
                      <?php foreach ($pregunta->getOpciones()->orderBy('orden')->all() as $opcion): ?>
                      <?php
                      $respuestaOpcion = RespuestaOpcion::findOne([
                        'respuesta_id' => $respuesta->id,
                        'opcion_id' => $opcion->id,
                      ]);
                      if (!$respuestaOpcion) {
                        throw new Exception("No existe la respuestaOpcion para la opcion $opcion->nombre ($opcion->id) de la pregunta $pregunta->nombre ($pregunta->id)");
                      }
                      ?>
                      <div class="opcion" data-id="<?= $opcion->id ?>" style="margin-top: 1em">
                        <label style="font-size: 0.9em"><?= $opcion->nombre ?></label>
                        <div class="form-group">
                          <input
                            data-opcion="<?= $opcion->id ?>"
                            class="input"
                            value="<?= $respuestaOpcion->valor ?>"
                            type="<?= $pregunta->numerico ? 'number' : 'text' ?>">
                        </div>
                      </div>
                      <?php endforeach ?>

                    <?php else: ?>
                      <div class="form-group">
                        <textarea placeholder="Respuesta..."><?= $respuesta->valor ?></textarea>
                      </div>
                    <?php endif ?>

                  <?php else: ?>

                    <?php if ($pregunta->multiple): ?>
                      <?php foreach ($pregunta->getOpciones()->orderBy('orden')->all() as $opcion): ?>
                      <?php
                      $respuestaOpcion = RespuestaOpcion::findOne([
                        'respuesta_id' => $respuesta->id,
                        'opcion_id' => $opcion->id,
                      ]);
                      if (!$respuestaOpcion) {
                        throw new Exception("No existe la opcion para la opcion $opcion->id de la pregunta $pregunta->id");
                      }
                      ?>
                      <div class="form-group checkbox">
                        <label class="opcion" data-id="<?= $opcion->id ?>">
                          <input
                            type="checkbox"
                            <?= $respuestaOpcion->valor ? 'checked' : '' ?>
                            data-mostrar="<?= implode(',', $opcion->getMostrar()->select('pregunta_id')->column()) ?>"
                            class="checkbox <?= $opcion->editable ? 'editable' : '' ?>"
                            >
                          <div class="box"></div>
                          <div class="label"><?= $opcion->nombre ?></div>
                        </label>
                        <?php if ($opcion->editable): ?>
                        <input
                          style="display: <?= $respuestaOpcion->valor ? 'block' : 'none' ?>"
                          data-opcion="<?= $opcion->id ?>"
                          class="input"
                          value="<?= $respuestaOpcion->valor ?>"
                          type="<?= $pregunta->numerico ? 'number' : 'text' ?>">
                        <?php endif ?>
                      </div>
                      <?php endforeach ?>

                    <?php else: ?>
                      <div class="form-group radiolist">
                        <div id="opcion-nombre" role="radiogroup">
                          <?php foreach ($pregunta->getOpciones()->orderBy('orden')->all() as $opcion): ?>
                          <?php
                          $respuestaOpcion = RespuestaOpcion::findOne([
                            'respuesta_id' => $respuesta->id,
                            'opcion_id' => $opcion->id,
                          ]);
                          if (!$respuestaOpcion) {
                            throw new Exception("No existe la opcion para la opcion $opcion->id de la pregunta $pregunta->id");
                          }
                          ?>
                          <label class="opcion" data-id="<?= $opcion->id ?>">
                            <input
                              type="radio"
                              <?= $respuestaOpcion->valor ? 'checked' : '' ?>
                              data-mostrar="<?= implode(',', $opcion->getMostrar()->select('pregunta_id')->column()) ?>"
                              class="with-gap radio <?= $opcion->editable ? 'editable' : '' ?>"
                              name="pregunta-<?= $pregunta->id ?>">
                            <span><?= $opcion->nombre ?></span>
                          </label>
                          <?php if ($opcion->editable): ?>
                          <input
                            style="display: <?= $respuestaOpcion->valor ? 'block' : 'none' ?>"
                            data-opcion="<?= $opcion->id ?>"
                            class="input"
                            value="<?= $respuestaOpcion->valor ?>"
                            type="<?= $pregunta->numerico ? 'number' : 'text' ?>">
                          <?php endif ?>
                          <?php endforeach ?>
                        </div>
                      </div>
                    <?php endif ?>

                  <?php endif ?>

                </div>
                <div class="mensaje-error"></div>
              </div>
            </div>

            <div class="ayuda">
              <?php if ($pregunta->ayuda): ?>
              <div class="titulo">
                Explicación de la pregunta
              </div>
              <div class="texto">
                <?= nl2br($pregunta->ayuda); ?>
              </div>
              <?php endif ?>
            </div>

          </div>
          <?php endforeach ?>
          <?php ActiveForm::end(); ?>

        </div>
      </div>
    </main>
    <footer>
      <div class="opciones">
        <?php if ($i > 0): ?>
        <div class="opcion">
          <div class="btn-flat btn-atras">
            <span class="mdi mdi-arrow-left"></span>
            Atrás
          </div>
        </div>
        <?php endif ?>
        <?php if ($ultimo): ?>
        <div class="opcion">
          <a class="btn btn-terminar"
            href="<?= Url::to(['/respuestas/terminar', 'id' => $respuestas->id, 'terminar' => true]) ?>">
            Terminar
          </a>
        </div>
        <?php else: ?>
        <div class="opcion">
          <button class="btn icon-right btn-siguiente">
            Siguiente <span class="mdi mdi-arrow-right"></span>
          </button>
        </div>
        <?php endif ?>
        <div id="progreso"
          style="background: linear-gradient(90deg,
            rgba(21,  101, 192, 1) <?= $p ?>%,
            rgba(207, 216, 220, 1) <?= $p ?>%)">
        </div>
        <div>
          Sección <?= $i + 1 ?> de <?= $cantidad ?>
        </div>
      </div>
    </footer>
  </div>
  <?php endforeach ?>
</div>
