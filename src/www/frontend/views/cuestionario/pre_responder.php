<?php

use yii\helpers\Url;

?>

<div class="wrapper">
  <header>
    <h1 class="titulo">
      <?= $model->nombre ?>
    </h1>
  </header>
</div>
<div class="wrapper">

  <div>
    <?php if ($respuestas): ?>

      <?php if ($model->cerrado_para_respuestas): ?>
        <p> El cuestionario no está recibiendo respuestas. </p>
      <?php elseif ($model->respuesta_unica): ?>
        <p> Ya ha respondido el cuestionario </p>
        <a href="<?= Url::to(['/respuestas/ver', 'id' => $respuestas->id]) ?>" class="btn">
          Ver respuestas
        </a>
      <?php else: ?>
        <p>No puede responder al cuestionario.</p>
      <?php endif ?>

    <?php endif ?>
  <div>

</div>
