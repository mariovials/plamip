<?php

use common\models\Cuestionario;
use yii\helpers\Url;
use common\models\Respuestas;

$cuestionarios = Cuestionario::find()->orderBy('id')->limit(10)->all();

?>


<div class="wrapper">

  <div id="cuestionarios">

    <?php
    foreach ($cuestionarios as $cuestionario):
      $respuestas = null;
      if (!Yii::$app->user->isGuest) {
        $respuestas = Respuestas::find()->where([
          'cuestionario_id' => $cuestionario->id,
          'usuario_id' => Yii::$app->user->id,
        ])
        ->orderBy('fecha_edicion DESC')
        ->one();
      }
    ?>
    <div class="item">
      <div class="superior">
        <div class="img hidden">
          <img src="https://www.stockvault.net/data/2016/03/21/189309/preview16.jpg" alt="">
        </div>
        <div class="informacion">
          <div class="nombre">
            <?= $cuestionario->nombre ?>
          </div>
          <div class="descripcion">
            <?= nl2br($cuestionario->descripcion_larga) ?>
          </div>
        </div>

      </div>
      <div class="inferior">

        <?php if ($cuestionario->cerrado_para_respuestas): ?>
        Este cuestionario no está recibiendo respuestas.
        <?php else: ?>
          <?php if ($respuestas): ?>
            <?php if ($cuestionario->respuesta_unica): ?>
              <?php if ($respuestas->fecha_termino): ?>
                <a href="<?= Url::to(['/respuestas/ver', 'id' => $respuestas->id]) ?>" class="btn">
                  Ver respuestas
                </a>
                <div>
                  Ya ha respondido el cuestionario, el <?= Yii::$app->formatter->asDateTime($respuestas->fecha_termino, 'corta') ?>
                </div>
              <?php else: ?>
                <a href="<?= Url::to(['/cuestionario/responder', 'id' => $cuestionario->id]) ?>"
                  class="btn btn-responder"
                  style="display: inline-block;">
                  Continuar
                </a>
                <div>
                  Ultima edición el <?= Yii::$app->formatter->asDateTime($respuestas->fecha_edicion, 'corta') ?>
                </div>
              <?php endif ?>
            <?php else: ?>
              <?php if ($respuestas->fecha_termino): ?>
                <a href="<?= Url::to(['/cuestionario/responder', 'id' => $cuestionario->id]) ?>"
                  class="btn btn-responder">
                  Responder
                </a>
              <?php else: ?>
                <a href="<?= Url::to(['/cuestionario/responder', 'id' => $cuestionario->id]) ?>"
                  class="btn btn-responder"
                  style="display: inline-block;">
                  Continuar
                </a>
                <div>
                  Ultima edición el <?= Yii::$app->formatter->asDateTime($respuestas->fecha_edicion, 'corta') ?>
                </div>
              <?php endif ?>
            <?php endif ?>
          <?php else: ?>
            <a href="<?= Url::to(['/cuestionario/responder', 'id' => $cuestionario->id]) ?>"
              class="btn btn-responder">
              Responder
            </a>
          <?php endif ?>
        <?php endif ?>
      </div>
    </div>
    <?php endforeach ?>

  </div>

    <a href="<?= Url::to(['/pdf/Lab UBB Memoria de experiencias 2022.pdf']) ?>" class="btn">
      <span class="mdi mdi-file-pdf-box"></span>
      Ver Memoria de experiencias
    </a>
    <br>
    <br>

</div>


<style>
  #pagina {
    background-image: none;
  }
  .btn-responder {
    background-color: #bc5957;
    color: #FFF;
  }
  .btn-responder:hover {
    background-color: #cc6967;
    color: #FFF;
  }
  #cuestionarios {
    display: flex;
    flex-wrap: wrap;
    gap: 1.5em 2em;
    padding: 2em 0 1em 0;
  }
  .item .inferior {
    display: flex;
    justify-content: space-between;
    padding: 0 1.5em 1em;
    align-items: center;
    flex-direction: row-reverse;
  }
  .item .inferior .btn {
    white-space: nowrap;
    margin-left: 1em;
  }
  .item {
    border-radius: 1em;
    overflow: hidden;
    max-width: calc((100% - 4em) / 2);
    min-width: 20em;
    width: calc(100% / 2);
    border: 1px solid hsl(210deg 13% 90%);
    background: hsl(210deg 13% 98%);
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    box-shadow: 0 0 1em -0.5em #AAA;
  }
  .item:hover {
  }
  .item .img img {
  }
  .item .img img {
    width: 100%;
    max-height: 9em;
    object-fit: cover;
  }
  .item .informacion {
    padding: 1.2em 1.5em 1.2em 1.5em;
    display: flex;
    flex-direction: column;
    flex-wrap: nowrap;
    justify-content: space-between;
  }
  .item .informacion .contenido {
  }
  .item .informacion .nombre {
    font-size: 1.1em;
  }
  .item .informacion .descripcion {
    font-size: 0.95em;
  }
  .item .informacion .nombre a {
    font-weight: inherit;
  }
  .item .informacion .nombre {
    font-weight: 600;
    margin-bottom: 0.7em;
  }
  .item .opciones {
    margin: 1em;
    display: flex;
    justify-content: flex-end;
  }
  .item .opciones .opcion {
  }
  .item .opciones .opcion .btn {
    background-color: #bc5957;
    color: #FFF;
  }
</style>
