<?php

use yii\helpers\Url;
use common\models\Respuestas;

$this->title = $model->nombre;

$secciones = $model->getSecciones()->orderBy('orden')->all();
$cantidad = count($secciones);

$respuestas = null;
if (!Yii::$app->user->isGuest) {
  $respuestas = Respuestas::find()->where([
    'cuestionario_id' => $model->id,
    'usuario_id' => Yii::$app->user->id,
  ])
  ->orderBy('fecha_edicion DESC')
  ->one();
}

?>

<div class="wrapper">

  <header>
    <h1 class="titulo">
      <?= $model->nombre ?>
    </h1>
  </header>

  <div>
    <br>
    <?= nl2br($model->descripcion_larga) ?>
    <br>

    <?php if ($model->cerrado_para_respuestas): ?>
      <p> Este cuestionario no está recibiendo respuestas. </p>
    <?php else: ?>
      <?php if ($respuestas): ?>
        <?php if ($model->respuesta_unica): ?>
          <?php if ($respuestas->fecha_termino): ?>
            Ya ha respondido el cuestionario, el <?= Yii::$app->formatter->asDateTime($respuestas->fecha_termino, 'larga') ?>
            <br>
            <br>
            <a href="<?= Url::to(['/respuestas/ver', 'id' => $respuestas->id]) ?>" class="btn">
              Ver respuestas
            </a>
          <?php else: ?>
            <a href="<?= Url::to(['/cuestionario/responder', 'id' => $model->id]) ?>"
              class="btn"
              style="display: inline-block;"
              id="btn-responder">
              Continuar
            </a>
            Ultima edición el <?= Yii::$app->formatter->asDateTime($respuestas->fecha_edicion, 'larga') ?>
          <?php endif ?>
        <?php else: ?>
          <?php if ($respuestas->fecha_termino): ?>
            <a href="<?= Url::to(['/cuestionario/responder', 'id' => $model->id]) ?>"
              class="btn"
              id="btn-responder">
              Responder
            </a>
          <?php else: ?>
            <a href="<?= Url::to(['/cuestionario/responder', 'id' => $model->id]) ?>"
              class="btn"
              style="display: inline-block;"
              id="btn-responder">
              Continuar
            </a>
            Ultima edición el <?= Yii::$app->formatter->asDateTime($respuestas->fecha_edicion, 'larga') ?>
          <?php endif ?>
        <?php endif ?>
      <?php else: ?>
        <a href="<?= Url::to(['/cuestionario/responder', 'id' => $model->id]) ?>"
          class="btn"
          id="btn-responder">
          Responder
        </a>
      <?php endif ?>
    <?php endif ?>
  </div>




</div>



<style>
  #btn-responder {
    background-color: #bc5957;
    color: #FFF;
  }
</style>
