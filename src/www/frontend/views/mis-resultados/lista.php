<?php

use yii\helpers\Url;

$respuestas = Yii::$app->user->identity->getRespuestas()->orderBy('cuestionario_id, fecha_termino')->all();

?>

<div class="wrapper">


  <header>
    <h1 class="titulo"> Mis resultados </h1>
  </header>

  <table>
    <thead>
      <tr>
        <th style="width: 20%"> Fecha</th>
        <th style="width: 60%">Cuestionario</th>
        <th style="width: 20%"> </th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($respuestas as $respuesta): ?>
      <tr>
        <td><?= Yii::$app->formatter->asDateTime($respuesta->fecha_termino, 'corta') ?></td>
        <td class="nombre"><?= $respuesta->cuestionario->nombre ?></td>
        <td>
          <?php if ($respuesta->fecha_termino): ?>
          <a href="<?= Url::to(['/respuestas/ver', 'id' => $respuesta->id]) ?>" class="btn bajo">
            Ver resultados
          </a>
          <?php else: ?>
          <a href="<?= Url::to(['/cuestionario/responder', 'id' => $respuesta->cuestionario_id]) ?>"
            class="btn bajo btn-responder">
            Continuar
          </a>
          <?php endif ?>
        </td>
      </tr>
      <?php endforeach ?>
    </tbody>
  </table>

</div>

<style>
  #pagina {
    background-image: none;
  }
  th {
    text-align: left;
  }
  th, td {
    padding: 0.1em 0.2em;
  }
  .btn-responder {
    background-color: #bc5957;
    color: #FFF;
  }
  .btn-responder:hover {
    background-color: #cc6967;
    color: #FFF;
  }
</style>
