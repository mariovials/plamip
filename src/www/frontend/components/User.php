<?php

namespace frontend\components;
use yii\web\IdentityInterface;

use Yii;

class User extends \yii\web\User
{
  public function esAdmin()
  {
    return $this->identity->esAdmin();
  }

  public function esValidador()
  {
    if (!$this->isGuest) {
      return $this->identity->esValidador();
    }
    return false;
  }

  public function getIsGuest()
  {
    if (Yii::$app->request->cookies->has('_identity')
      || Yii::$app->response->cookies->has('_identity'))
      return parent::getIsGuest();
    else
      return true;
  }
}
