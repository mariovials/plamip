<?php
$params = array_merge(
  require __DIR__ . '/../../common/config/params.php',
  require __DIR__ . '/../../common/config/params-local.php',
  require __DIR__ . '/params.php',
  require __DIR__ . '/params-local.php'
);

return [
  'id' => 'backend',
  'basePath' => dirname(__DIR__),
  'bootstrap' => ['log'],
  'controllerNamespace' => 'backend\controllers',
  'defaultRoute' => 'sistema/lista',

  'modules' => [
    'config' => backend\modules\config\Module::class,
  ],

  'components' => [

    'view' => [
      'theme' => [
        'pathMap' => [
          '@app/modules/config/views' => [
            '@app/modules/config/views',
            '@app/views',
          ],
        ],
      ],
    ],

    'request' => [
      'csrfParam' => '_csrf',
      'cookieValidationKey' => 'gFpvCwmTDAiSQ-Vio8lrfLIvMskzz4Ae',
    ],

    'errorHandler' => [
      'errorAction' => 'sistema/error',
    ],
  ],
  'params' => $params,
];
