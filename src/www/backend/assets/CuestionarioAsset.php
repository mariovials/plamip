<?php

namespace backend\assets;

use yii\web\AssetBundle;

class CuestionarioAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'css/cuestionario.css?v=2',
  ];
  public $depends = [
    'backend\assets\AppAsset',
    'yii\jui\JuiAsset',
  ];
}
