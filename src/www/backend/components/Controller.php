<?php
namespace backend\components;

use Yii;
use yii\filters\AccessControl;
use common\models\Usuario;

/**
 * Controlador Base de Backend
 */
class Controller extends \yii\web\Controller
{
  public $defaultAction = 'lista';
  public $menu = false;
  public $layout = 'cuestionario';

  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::class,
        'rules' => [
          [
            'allow' => true,
            'roles' => ['@'],
            'matchCallback' => function($rule, $action) {
              return Yii::$app->user->esAdmin();
            }
          ],
        ],
      ],
    ];
  }
}
