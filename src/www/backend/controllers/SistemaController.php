<?php

namespace backend\controllers;

use Yii;
use backend\components\Controller;
use common\models\IngresarForm;
use yii\filters\AccessControl;

class SistemaController extends Controller
{
  public $menu = false;
  public $layout = 'main';

  public function actions()
  {
    return [
      'error' => \yii\web\ErrorAction::class,
    ];
  }

  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::class,
        'rules' => [
          [
            'allow' => true,
            'actions' => ['ingresar', 'error'],
            'roles' => ['?'],
          ],
          [
            'allow' => true,
            'actions' => ['lista', 'salir'],
            'roles' => ['@'],
          ],
        ],
      ],
    ];
  }

  public function actionIngresar()
  {
    $this->layout = 'blanco';
    if (!Yii::$app->user->isGuest) {
      return $this->goHome();
    }
    $model = new IngresarForm();
    if ($model->load(Yii::$app->request->post()) && $model->ingresar()) {
      return $this->goBack();
    }
    $model->contrasena = '';
    return $this->render('ingresar', [
      'model' => $model,
    ]);
  }

  public function actionLista()
  {
    return $this->render('lista');
  }

  public function actionSalir()
  {
    if (Yii::$app->user->logout()) {
      return $this->redirect(Yii::$app->urlManagerFrontend->createAbsoluteUrl(['/']));
    } else {
      throw new \Exception("No se puede cerrar sesión", 1);
    }
  }
}
