<?php

namespace backend\controllers;

use Yii;
use common\models\Parametro;
use common\models\CompraSearch;
use backend\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ParametroController implements the CRUD actions for Parametro model.
 */
class ParametroController extends Controller
{
  public function behaviors()
  {
    return array_merge(parent::behaviors(),
      [
        'verbs' => [
          'class' => VerbFilter::class,
          'actions' => [
            'delete' => ['POST'],
          ],
        ],
      ]
    );
  }

  public function actionLista()
  {
    $searchModel = new CompraSearch();
    $dataProvider = $searchModel->search($this->request->queryParams);
    return $this->render('lista', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  public function actionVer($id)
  {
    return $this->render('ver', [
      'model' => $this->findModel($id),
    ]);
  }

  public function actionAgregar($tipo_parametro_id, $parametro_id = null)
  {
    $model = new Parametro();
    $model->tipo_parametro_id = $tipo_parametro_id;
    $model->parametro_id = $parametro_id;

    if ($this->request->isPost) {
      if ($model->load($this->request->post()) && $model->save()) {
        return $this->redirect(Yii::$app->request->get('to', ['ver', 'id' => $model->id]));
      }
    } else {
      $model->loadDefaultValues();
    }
    return $this->render('agregar', [
      'model' => $model,
    ]);
  }

  /**
   * Updates an existing Parametro model.
   * If update is successful, the browser will be redirected to the 'ver' page.
   * @param int $id
   * @return string|\yii\web\Response
   * @throws NotFoundHttpException if the model cannot be found
   */
  public function actionEditar($id)
  {
    $model = $this->findModel($id);
    if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
      return $this->redirect(Yii::$app->request->get('to', ['ver', 'id' => $model->id]));
    }
    return $this->render('editar', [
      'model' => $model,
    ]);
  }

  /**
   * Deletes an existing Parametro model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param int $id
   * @return \yii\web\Response
   * @throws NotFoundHttpException if the model cannot be found
   */
  public function actionEliminar($id)
  {
    $this->findModel($id)->delete();
    return $this->redirect(Yii::$app->request->get('to', ['lista']));
  }

  /**
   * Finds the Parametro model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param int $id
   * @return Parametro the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model = Parametro::findOne(['id' => $id])) !== null) {
      return $model;
    }
    throw new NotFoundHttpException('The requested page does not exist.');
  }
}
