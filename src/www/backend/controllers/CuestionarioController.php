<?php

namespace backend\controllers;

use Yii;
use common\models\Cuestionario;
use yii\data\ActiveDataProvider;
use backend\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class CuestionarioController extends Controller
{
  public $layout = 'main';

  public function behaviors()
  {
    return array_merge(parent::behaviors(),
      [
        'verbs' => [
          'class' => VerbFilter::class,
          'actions' => [
            'delete' => ['POST'],
          ],
        ],
      ]
    );
  }

  public function actionLista()
  {
    $dataProvider = new ActiveDataProvider([
      'query' => Cuestionario::find(),
    ]);
    return $this->render('lista', [
      'dataProvider' => $dataProvider,
    ]);
  }

  public function actionVer($id, $tab = 'parametros')
  {
    $this->layout = 'cuestionario';
    return $this->render('ver', [
      'model' => $this->findModel($id),
      'tab' => $tab,
    ]);
  }

  public function actionPreguntas($id)
  {
    $this->layout = 'cuestionario';
    return $this->render('preguntas', [
      'model' => $this->findModel($id),
    ]);
  }

  public function actionRespuestas($id)
  {
    $this->layout = 'cuestionario';
    return $this->render('respuestas', [
      'model' => $this->findModel($id),
    ]);
  }

  public function actionAgregar()
  {
    $model = new Cuestionario();

    if ($this->request->isPost) {
      if ($model->load($this->request->post()) && $model->save()) {
        return $this->redirect(['preguntas', 'id' => $model->id]);
      }
    } else {
      $model->loadDefaultValues();
    }
    return $this->render('agregar', [
      'model' => $model,
    ]);
  }

  public function actionEditar($id)
  {
    $this->layout = 'cuestionario';
    $model = $this->findModel($id);
    if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
      return $this->redirect(Yii::$app->request->get('from', ['ver', 'id' => $model->id]));
    }
    return $this->render('editar', [
      'model' => $model,
    ]);
  }

  public function actionEliminar($id)
  {
    $this->findModel($id)->delete();
    return $this->redirect(['lista']);
  }

  protected function findModel($id)
  {
    if (($model = Cuestionario::findOne(['id' => $id])) !== null) {
      return $model;
    }
    throw new NotFoundHttpException('The requested page does not exist.');
  }
}
