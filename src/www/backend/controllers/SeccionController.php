<?php

namespace backend\controllers;

use Yii;
use common\models\Seccion;
use yii\data\ActiveDataProvider;
use backend\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class SeccionController extends Controller
{
  public function behaviors()
  {
    return array_merge(parent::behaviors(),
      [
        'verbs' => [
          'class' => VerbFilter::class,
          'actions' => [
            'delete' => ['POST'],
          ],
        ],
      ]
    );
  }

  public function actionLista()
  {
    $dataProvider = new ActiveDataProvider([
      'query' => Seccion::find(),
    ]);
    return $this->render('lista', [
      'dataProvider' => $dataProvider,
    ]);
  }

  public function actionVer($id)
  {
    return $this->render('ver', [
      'model' => $this->findModel($id),
    ]);
  }

  public function actionOrdenar($id, $orden = 1)
  {
    $model = Seccion::findOne($id);
    $model->orden = $orden;
    $model->save();
    return $this->redirect(['/cuestionario/preguntas', 'id' => $model->cuestionario_id]);
  }

  public function actionAgregar($cuestionario_id)
  {
    $model = new Seccion();
    $model->cuestionario_id = $cuestionario_id;

    if ($this->request->isPost) {
      if ($model->load($this->request->post()) && $model->save()) {
        return $this->redirect(Yii::$app->request->get('to', ['ver', 'id' => $model->id]));
      }
    } else {
      $model->loadDefaultValues();
    }
    return $this->render('agregar', [
      'model' => $model,
    ]);
  }

  public function actionEditar($id)
  {
    $model = $this->findModel($id);
    if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
      return $this->redirect(Yii::$app->request->get('to', ['ver', 'id' => $model->id]));
    }
    return $this->render('editar', [
      'model' => $model,
    ]);
  }

  public function actionEliminar($id)
  {
    $this->findModel($id)->delete();
    return $this->redirect(Yii::$app->request->get('to', ['lista']));
  }

  protected function findModel($id)
  {
    if (($model = Seccion::findOne(['id' => $id])) !== null) {
      return $model;
    }
    throw new NotFoundHttpException('The requested page does not exist.');
  }
}
