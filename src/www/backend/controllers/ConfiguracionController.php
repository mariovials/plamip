<?php

namespace backend\controllers;

use Yii;
use common\models\Configuracion;
use yii\data\ActiveDataProvider;
use backend\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class ConfiguracionController extends Controller
{
  public $menu = true;

  public function behaviors()
  {
    return array_merge(parent::behaviors(),
      [
        'verbs' => [
          'class' => VerbFilter::class,
          'actions' => [
            'delete' => ['POST'],
          ],
        ],
      ]
    );
  }

  public function actionLista()
  {
    $this->layout = 'main';
    $dataProvider = new ActiveDataProvider([
      'query' => Configuracion::find(),
    ]);
    return $this->render('lista', [
      'dataProvider' => $dataProvider,
    ]);
  }

  public function actionVer($id)
  {
    return $this->render('ver', [
      'model' => $this->findModel($id),
    ]);
  }

  public function actionAgregar()
  {
    $model = new Configuracion();

    if ($this->request->isPost) {
      if ($model->load($this->request->post()) && $model->save()) {
        return $this->redirect(['ver', 'id' => $model->id]);
      }
    } else {
      $model->loadDefaultValues();
    }
    return $this->render('agregar', [
      'model' => $model,
    ]);
  }

  public function actionEditar($id)
  {
    $model = $this->findModel($id);
    if ($this->request->isPost) {
      if ($model->load($this->request->post()) && $model->save()) {
        return $this->redirect(Yii::$app->request->get('from', ['ver', 'id' => $model->id]));
      } else {
        echo "<pre>";
        var_dump($model->attributes);
        var_dump($model->errors);
      }
    }
    return $this->render('editar', [
      'model' => $model,
    ]);
  }

  public function actionEliminar($id)
  {
    $this->findModel($id)->delete();
    return $this->redirect(['lista']);
  }

  protected function findModel($id)
  {
    if (($model = Configuracion::findOne(['id' => $id])) !== null) {
      return $model;
    }
    throw new NotFoundHttpException('The requested page does not exist.');
  }
}
