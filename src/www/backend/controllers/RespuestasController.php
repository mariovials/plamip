<?php

namespace backend\controllers;

use Yii;
use backend\components\Controller;
use common\models\Respuestas;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class RespuestasController extends Controller
{
  public $menu = false;
  public $layout = 'cuestionario';

  public function behaviors()
  {
    return array_merge(parent::behaviors(),
      [
        'verbs' => [
          'class' => VerbFilter::class,
          'actions' => [
            'delete' => ['POST'],
          ],
        ],
      ]
    );
  }

  public function actionVer($id)
  {
    $this->layout = 'cuestionario';
    return $this->render('ver', [
      'model' => $this->findModel($id),
    ]);
  }

  public function actionEliminar($id)
  {
    $model = $this->findModel($id);
    $model->delete();
    return $this->redirect(['/cuestionario/respuestas',
      'id' => $model->cuestionario_id]);
  }

  protected function findModel($id)
  {
    if (($model = Respuestas::findOne(['id' => $id])) !== null) {
      return $model;
    }
    throw new NotFoundHttpException('The requested page does not exist.');
  }

}
