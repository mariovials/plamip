<?php

namespace backend\modules\config\controllers;

use backend\components\Controller;

/**
 *
 */
class DefaultController extends Controller
{
  public $layout = 'main';
  public $menu = true;

  public function actionLista()
  {
    return $this->render('lista');
  }
}
