<?php
use common\widgets\ActiveForm;

$this->title = "Editar $model->nombre";

$this->params['icono'] = 'account';
$this->params['links'] = [
  ['label' => 'Usuarios', 'url' => ['/config/usuario']],
  ['label' => $model->nombre, 'url' => $model->url],
  'Editar',
];
?>

<div>
  <?= $this->render('_form', ['model' => $model]); ?>
</div>
