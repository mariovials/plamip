<?php

use common\helpers\Html;
use yii\helpers\Url;

$this->title = $model->nombre;

$this->params['icono'] = 'account';
$this->params['links'] = [
  ['label' => 'Usuarios', 'url' => ['/config/usuario']],
  $this->title,
];
$this->params['opciones'][] = Html::a(
  '<span class="mdi mdi-delete"></span> Eliminar',
  ['/config/usuario/eliminar', 'id' => $model->id, 'from' => Url::current()],
  ['class' => 'btn-flat', 'data' => ['method' => 'POST', 'confirm' => '¿Está seguro?']]);
$this->params['opciones'][] = Html::a(
  '<span class="mdi mdi-pencil"></span> Editar',
  ['/config/usuario/editar', 'id' => $model->id, 'from' => Url::current(), 'to' => Url::current()],
  ['class' => 'btn']);
?>

<div class="ficha">
  <header>
    <div class="principal">
      <div class="icono">
        <span class="mdi mdi-account"></span>
      </div>
      <div class="titulo">
        <div class="nombre">
         <?= $model->nombre; ?>
        </div>
      </div>
    </div>
  </header>
  <main>
    <div class="fila">
      <div class="campo">
        <div class="label">
          <?= $model->getAttributeLabel('institucion'); ?>
        </div>
        <div class="value">
          <?= $model->institucion; ?>
        </div>
      </div>
    </div>
    <div class="fila">
      <div class="campo">
        <div class="label">
          <?= $model->getAttributeLabel('usuario'); ?>
        </div>
        <div class="value">
          <?= $model->usuario; ?>
        </div>
      </div>
      <div class="campo">
        <div class="label">
          <?= $model->getAttributeLabel('correo_electronico'); ?>
        </div>
        <div class="value">
          <?= $model->correo_electronico; ?>
        </div>
      </div>
    </div>
    <div class="fila">
      <div class="campo">
        <div class="label">
          <?= $model->getAttributeLabel('estado'); ?>
        </div>
        <div class="value">
          <?= $model->estado(); ?>
        </div>
      </div>
      <div class="campo">
        <div class="label">
          <?= $model->getAttributeLabel('tipo'); ?>
        </div>
        <div class="value">
          <?= $model->tipo(); ?>
        </div>
      </div>
    </div>
  </main>
</div>
