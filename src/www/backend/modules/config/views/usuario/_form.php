<?php

use common\widgets\ActiveForm;
use yii\helpers\Url;

?>

<div class="form">
  <?php $form = ActiveForm::begin(); ?>
  <div class="filas">

    <div class="fila completa">
      <div class="campo">
        <span class="mdi mdi-account"></span>
        <?= $form->field($model, 'nombre'); ?>
      </div>
    </div>

    <div class="fila completa">
      <div class="campo">
        <span class="mdi mdi-bank"></span>
        <?= $form->field($model, 'institucion'); ?>
      </div>
    </div>

    <div class="fila completa">
      <div class="campo">
        <span class="mdi mdi-email"></span>
        <?= $form->field($model, 'correo_electronico'); ?>
      </div>
    </div>

    <?php if ($model->isNewRecord): ?>
    <div class="fila completa">
      <div class="campo">
        <span class="mdi mdi-lock"></span>
        <?php $model->contrasena = '' ?>
        <?= $form->field($model, 'contrasena'); ?>
      </div>
      <div class="campo ">
        <?= $form->field($model, 'confirmar_contrasena'); ?>
      </div>
    </div>
    <?php endif ?>

    <div class="fila completa">
      <div class="campo">
        <span class="mdi mdi-account-edit"></span>
        <?= $form->field($model, 'tipo')
          ->dropDownList([
            '1' => 'Administrador',
            '2' => 'Usuario',
            '3' => 'Validador',
          ],
            ['prompt' => [
              'text' => '',
              'options' => ['value' => '', 'style' => ['display' => 'none']]],
            ]); ?>
      </div>
      <div class="campo">
        <span class="mdi mdi-toggle-switch-outline"></span>
        <?= $form->field($model, 'estado')
          ->dropDownList(['1' => 'Habilitado', '2' => 'Deshabilitado'],
            ['prompt' => [
              'text' => '',
              'options' => ['value' => '', 'style' => ['display' => 'none']]],
            ]); ?>
      </div>
    </div>
  </div>
  <div class="opciones">
    <div class="opcion">
      <button class="btn">
        <span class="mdi mdi-<?= $model->isNewRecord ? 'plus-thick' : 'pencil' ?>"></span>
        <?= $model->isNewRecord ? 'Agregar' : 'Guardar' ?>
      </button>
    </div>
    <div class="opcion">
      <a href="<?= Url::to(Yii::$app->request->get('from', ['/config/usuario'])) ?>"
        class="btn-flat solo">
        Cancelar
      </a>
    </div>
  </div>
  <?php ActiveForm::end(); ?>
</div>
