<?php

use yii\helpers\Url;

?>

<div class="item">
  <div class="informacion">
    <div class="texto">
      <div class="primario">
        <div class="campo">
          <a href="<?= Url::to(['/config/usuario/ver', 'id' => $model->id, 'to' => Url::current()]) ?>">
            <?= $model->nombre ?>
          </a>
        </div>
        <div class="campo">
          <?= $model->correo_electronico ?>
        </div>
      </div>
    </div>
  </div>
  <div class="opciones">
    <div class="opcion">
      <div class="chip">
        <?= $model->tipo() ?>
      </div>
    </div>
    <div class="opcion grande">
      <a class="btn " href="<?= Url::to(['/config/usuario/editar',
        'id' => $model->id,
        'to' => Url::current()]) ?>">
        <span class="mdi mdi-pencil"></span> Editar
      </a>
    </div>
  </div>
</div>
