<?php

use common\models\Usuario;
use backend\widgets\ListView;
use common\helpers\Html;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

$this->title = 'Usuarios';
$this->params['icono'] = 'account';
$this->params['links'] = [ 'Usuarios'];
$this->params['opciones'][] = Html::a(
  '<span class="mdi mdi-plus"></span> Agregar',
  ['/config/usuario/agregar', 'from' => Url::current(), 'to' => Url::current()],
  [ 'class' => 'btn'],
);
?>

<div class="ficha lista">
  <main>
    <?= ListView::widget([
      'dataProvider' => new ActiveDataProvider([
        'query' => Usuario::find(),
        'sort' => ['attributes' => ['nombre']],
        'pagination' => ['pageSize' => 10],
      ]),
      'itemView' => '_lista',
    ]); ?>
  </main>
</div>
