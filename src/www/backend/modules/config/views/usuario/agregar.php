<?php

$this->title = 'Agregar Usuario';
$this->params['icono'] = 'account';
$this->params['links'] = [
  ['label' => 'Usuarios', 'url' => ['/config/usuario']],
  'Agregar',
];

?>

<div>
  <?= $this->render('_form', ['model' => $model]); ?>
</div>
