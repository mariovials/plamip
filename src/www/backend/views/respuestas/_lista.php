<?php

use yii\helpers\Url;

?>

<div class="item" data-id="<?= $model->id ?>">
  <div class="informacion">
    <div class="texto">
      <div class="primario">
        <a href="<?= Url::to(['/respuestas/ver', 'id' => $model->id]) ?>">
          <?= $model->usuario->nombre ?>
        </a>
      </div>
    </div>
  </div>
  <div class="opciones">
    <div class="opcion">
      <div class="chip">
      <?php if ($model->fecha_edicion): ?>
        Terminado
      <?php else: ?>
        Pendiente
      <?php endif ?>
      </div>
    </div>
    <div class="opcion">
      <?= Yii::$app->formatter->asDatetime($model->fecha_termino ?: $model->fecha_edicion, 'corta') ?>
    </div>
  </div>
</div>
