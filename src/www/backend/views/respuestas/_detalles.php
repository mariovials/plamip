<?php



?>

<div class="ficha detalles">
  <div class="fila">
    <div class="campo grande">
      <a target="_blank" href="<?= Yii::$app->urlManagerFrontend
        ->createAbsoluteUrl(['/respuestas/ver', 'id' => $model->id]); ?>">
          Ver resultados <span class="mdi mdi-open-in-new"></span>
        </a>
    </div>
  </div>
</div>
