<?php

use common\helpers\Html;
use yii\helpers\Url;
use common\models\Pregunta;
use common\models\Respuesta;
use common\models\RespuestaOpcion;

$model->iniciar();

$this->title = "Respuestas de {$model->usuario->nombre}";

$cuestionario = $model->cuestionario;
$this->params['cuestionario'] = $cuestionario;
$this->params['menu'] = 'respuestas';
$this->params['links'][] = ['label' => 'Respuestas', 'url' => ['/cuestionario/respuestas', 'id' => $cuestionario->id]];
$this->params['links'][] = $model->usuario->nombre;
$this->params['opciones'][] = Html::a(
  '<span class="mdi mdi-delete"></span> Eliminar',
  ['/respuestas/eliminar', 'id' => $model->id, 'from' => Url::current()],
  ['class' => 'btn-flat peligroso', 'data' => ['confirm' => '¿Está seguro?'] ]);


$secciones = $cuestionario->getSecciones()->orderBy('orden')->all();
$cantidad = count($secciones);

$this->params['lateral'] = $this->render('_detalles', ['model' => $model]);
?>

<div id="" class="wrapper ">
  <?php
  foreach ($secciones as $i => $seccion):
    $ultimo = $i + 1 == $cantidad;
    $p = ($i + 1) / $cantidad * 100;
  ?>
  <div class="ficha lista seccion" id="seccion-<?= $seccion->id ?>" data-id="<?= $seccion->id ?>">
    <header>
      <div class="principal">
        <div class="titulo"> <div class="nombre"> <?= $seccion->nombre; ?> </div> </div>
      </div>
    </header>
    <main>
      <div class="lista">
        <div class="items">

          <?php foreach ($seccion->getPreguntas()->orderBy('orden')->all() as $pregunta): ?>
          <?php
          $respuesta = Respuesta::findOne([
            'respuestas_id' => $model->id,
            'pregunta_id' => $pregunta->id,
          ]);
          ?>
          <div class="pregunta">
            <div class="contenido">
              <div class="titulo">
                <div class="nombre">  <?= $pregunta->nombre; ?> </div>
                <div class="descripcion"><?= nl2br($pregunta->descripcion); ?></div>
                <div class="descripcion">
                  <?php foreach ($pregunta->parametros as $parametro): ?>
                  <div class="campo" style="display: inline-block; margin-bottom: 0.2em">
                    <div class="chip parametro" style="
                      background: <?= $parametro->color ?>;
                      color: #FFFFFF">
                      <?= $parametro->icono() ?>
                      <?= $parametro->nombre ?>
                    </div>
                  </div>
                  <?php endforeach ?>
                </div>
              </div>
              <div class="respuesta">

                <?php if ($pregunta->tipo == Pregunta::TIPO_TEXTO): ?>

                  <?php if ($pregunta->multiple): ?>
                    <?php foreach ($pregunta->getOpciones()->orderBy('orden')->all() as $opcion): ?>
                    <?php
                    $respuestaOpcion = RespuestaOpcion::findOne([
                      'respuesta_id' => $respuesta->id,
                      'opcion_id' => $opcion->id,
                    ]);
                    ?>
                    <div class="opcion" data-id="<?= $opcion->id ?>" style="margin-top: 1em">
                      <label style="font-size: 0.9em"><?= $opcion->nombre ?></label>
                      <div>
                        <?= $respuestaOpcion->valor ?>
                      </div>
                    </div>
                    <?php endforeach ?>

                  <?php else: ?>
                    <?= nl2br($respuesta->valor) ?>
                  <?php endif ?>

                <?php else: ?>

                  <?php if ($pregunta->multiple): ?>
                    <?php foreach ($pregunta->getOpciones()->orderBy('orden')->all() as $opcion): ?>
                    <?php
                    $respuestaOpcion = RespuestaOpcion::findOne([
                      'respuesta_id' => $respuesta->id,
                      'opcion_id' => $opcion->id,
                    ]);
                    ?>

                    <div>
                      <div class="label">
                      <?php if ($respuestaOpcion->valor): ?>
                        <span class="mdi mdi-checkbox-marked"></span>
                      <?php else: ?>
                        <span class="mdi mdi-checkbox-blank-outline"></span>
                      <?php endif ?>
                        <?= $opcion->nombre ?>
                      </div>
                      <?php if ($opcion->editable && $respuestaOpcion->valor): ?>
                      <div class="value"> <?= $respuestaOpcion->valor ?> </div>
                      <?php endif ?>
                    </div>
                    <?php endforeach ?>

                  <?php else: ?>
                    <div class="form-group radiolist">
                      <div id="opcion-nombre" role="radiogroup">
                        <?php foreach ($pregunta->getOpciones()->orderBy('orden')->all() as $opcion): ?>
                        <?php
                        $respuestaOpcion = RespuestaOpcion::findOne([
                          'respuesta_id' => $respuesta->id,
                          'opcion_id' => $opcion->id,
                        ]);
                        if (!$respuestaOpcion->valor) continue;
                        ?>
                        <div>
                          <div class="label"> <?= $opcion->nombre ?> </div>
                          <?php if ($opcion->editable): ?>
                          <div class="value"> <?= $respuestaOpcion->valor ?> </div>
                          <?php endif ?>
                        </div>
                        <?php endforeach ?>
                      </div>
                    </div>
                  <?php endif ?>

                <?php endif ?>
              </div>
            </div>
          </div>
          <?php endforeach ?>

        </div>
      </div>
    </main>
  </div>
  <?php endforeach ?>
</div>

<style>
  .pregunta {
    display: flex;
    border: 1px solid #EEE;
    margin-bottom: 1em;
    padding: 1em 1.5em;
    box-shadow: 0 0.05em 0.4em -0.2em #777;
    border-radius: 1em;
  }
  .pregunta .numero {
    padding: 0 1em 0 0;
    font-weight: 500;
  }
  .pregunta .contenido {
    width: 100%;
  }
  .pregunta .titulo .nombre {
    color: #000;
    font-weight: 500;
  }
  .pregunta .respuesta {
    color: #555;
  }
  .pregunta .titulo .descripcion {
    font-size: 0.9em;
    margin: 0.3em 0;
  }
</style>
