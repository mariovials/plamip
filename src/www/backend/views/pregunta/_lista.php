<?php

use common\helpers\Html;
use common\models\Pregunta;
use yii\helpers\Url;
?>

<div class="item" data-id="<?= $model->id ?>" id="pregunta-<?= $model->id?>">
  <div class="informacion">
    <div class="icono">
      <?= $model->icono; ?>
      <span class="mdi mdi-drag drag"></span>
    </div>
    <div class="texto">
      <div class="primario">
        <div class="campo">
          <a href="<?= Url::to(['/pregunta/ver',
            'id' => $model->id,
            'from' => Url::to(['/cuestionario/preguntas', 'id' => $model->cuestionario_id, '#' => "pregunta-$model->id"]),
            ]) ?>">
            <?= $model->nombre ?>
          </a>
        </div>
      </div>

      <div class="secundario">
        <?php if ($this->context->id == 'pregunta'): ?>
        <div class="campo chip">
          <span class="mdi mdi-list-box"></span>
          <?= $model->cuestionario->nombre ?>
        </div>
        <?php endif ?>

        <?php foreach ($model->parametros as $parametro): ?>
        <div class="campo">
          <div class="chip parametro" style="
            background: <?= $parametro->color ?>;
            color: #FFFFFF">
            <?= $parametro->icono() ?>
            <?= $parametro->nombre ?>
          </div>
        </div>
        <?php endforeach ?>
      </div>

      <?php if ($model->tipo == Pregunta::TIPO_SELECCION || $model->multiple): ?>
      <div class="terciario" style="font-size: 0.95em; padding: 0.3em 0.5em 0.2em 1em">
        <?php foreach ($model->opciones as $opcion): ?>
          <?= $opcion->pregunta->icono ?>
          <?= $opcion->nombre ?>
          <br>
        <?php endforeach ?>
      </div>
      <?php endif ?>


    </div>
  </div>
  <div class="opciones ocultas">
    <div class="opcion">
      <?= Html::a(
        '<span class="mdi mdi-pencil"></span>',
        ['/pregunta/editar', 'id' => $model->id,
          'to' => ['/cuestionario/preguntas', 'id' => $model->cuestionario_id, '#' => "pregunta-$model->id"],
          'from' => Url::to(['/cuestionario/preguntas', 'id' => $model->cuestionario_id, '#' => "pregunta-$model->id"]),
        ],
        ['class' => 'btn solo']); ?>
    </div>
    <div class="opcion">
      <?= Html::a(
        '<span class="mdi mdi-delete"></span>',
        ['/pregunta/eliminar', 'id' => $model->id, 'to' => $model->seccion->url],
        ['class' => 'btn solo', 'data' => ['confirm' => '¿Está seguro?']]); ?>
    </div>
  </div>
  <div class="opciones">
    <div class="opcion">
      <?= $model->puntaje + 0 ?>
    </div>
  </div>
</div>
