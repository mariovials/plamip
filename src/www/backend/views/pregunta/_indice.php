<?php
use yii\helpers\Url;

$opciones = $opciones ?? [];
?>

<div class="ficha">

  <header>
    <div class="principal">
      <div class="icono"> <span class="mdi mdi-help-box"></span> </div>
      <div class="titulo">
        <div class="nombre">
          <?php if (!isset($link) || $link): ?>
            <a href="<?= Url::to(['/pregunta']) ?>">
              Preguntas
            </a>
          <?php else: ?>
            Preguntas
          <?php endif ?>
        </div>
        <div class="descripcion">
          Lista de Preguntas del sistema
        </div>
      </div>
    </div>

    <?php if ($opciones): ?>
    <div class="opciones">

      <?php if (in_array('agregar', $opciones)): ?>
      <div class="opcion">
        <a href="<?= Url::to(['/pregunta/agregar']) ?>" class="btn">
          <span class="mdi mdi-plus-thick"></span> Agregar
        </a>
      </div>
      <?php endif ?>

    </div>
    <?php endif ?>

  </header>

</div>
