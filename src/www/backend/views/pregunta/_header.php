<?php

use common\helpers\Html;
use yii\helpers\Url;

$opciones = $opciones ?? [];

?>

<header>
  <div class="principal">
    <div class="icono"> <?= $model->icono ?> </div>
    <div class="titulo">
      <div class="nombre">
        <?php if (isset($link) && !$link): ?>
          <?= $model->nameAttribute; ?>
        <?php else: ?>
          <?= $model->htmlLink(); ?>
        <?php endif; ?>
      </div>
      <div class="descripcion">

        <?php foreach ($model->parametros as $parametro): ?>
        <div class="campo">
          <div class="chip parametro" style="
            background: <?= $parametro->color ?>;
            color: #FFFFFF">
            <?= $parametro->icono() ?>
            <?= $parametro->nombre ?>
          </div>
        </div>
        <?php endforeach ?>

      </div>
    </div>
  </div>
  <div class="opciones">
    <?php if (in_array('eliminar', $opciones)): ?>
    <div class="opcion">
    </div>
    <?php endif ?>
    <?php if (in_array('editar', $opciones)): ?>
    <div class="opcion">
      <?= Html::a(
        '<span class="mdi mdi-pencil"></span> Editar',
        ['/pregunta/editar', 'id' => $model->id, 'from' => Url::current()],
        ['class' => 'btn'],
      ); ?>
    </div>
    <?php endif ?>
  </div>
</header>
