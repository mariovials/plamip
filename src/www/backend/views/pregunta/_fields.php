<?php

use common\models\Pregunta;

$attributes = $attributes ?? array_keys($model->attributes);
$model->puntaje += 0;
?>

<div class="filas">

  <?php if (in_array('nombre', $attributes)): ?>
  <div class="fila">
    <div class="campo nombre ancho">
      <i class="mdi mdi-text-box"></i>
      <?= $form->field($model, 'nombre')->textarea(); ?>
    </div>
  </div>
  <?php endif; ?>

  <?php if (in_array('descripcion', $attributes)): ?>
  <div class="fila">
    <div class="campo descripcion grande ">
      <i class="mdi mdi-text-short"></i>
      <?= $form->field($model, 'descripcion'); ?>
    </div>
  </div>
  <?php endif; ?>

  <div class="fila">
    <?php if (in_array('puntaje', $attributes)): ?>
    <div class="campo puntaje">
      <i class="mdi mdi-counter"></i>
      <?= $form->field($model, 'puntaje')->number(['step' => 0.01]); ?>
    </div>
    <?php endif; ?>
    <?php if (in_array('tipo', $attributes)): ?>
    <?php $model->tipo = $model->tipo ?: Pregunta::TIPO_TEXTO ?>
    <div class="campo tipo" style="width: max-content;">
      <?= $model->icono ?>
      <?= $form->field($model, 'tipo')->dropDownList(Pregunta::TIPOS); ?>
    </div>
    <div class="campo multiple">
      <?= $form->field($model, 'multiple')->checkbox(); ?>
    </div>
    <div class="campo numerico">
      <?= $form->field($model, 'numerico')->checkbox(); ?>
    </div>
    <?php endif; ?>
  </div>

  <?php if (in_array('numero', $attributes)): ?>
  <div class="fila">
    <div class="campo numero grande">
      <i class="mdi mdi-text-box"></i>
      <?= $form->field($model, 'numero'); ?>
    </div>
  </div>
  <?php endif; ?>

  <?php if (in_array('estado', $attributes)): ?>
  <div class="fila">
    <div class="campo estado grande">
      <i class="mdi mdi-text-box"></i>
      <?= $form->field($model, 'estado'); ?>
    </div>
  </div>
  <?php endif; ?>

  <?php if (in_array('dependencia', $attributes)): ?>
  <div class="fila">
    <div class="campo dependencia grande">
      <i class="mdi mdi-text-box"></i>
      <?= $form->field($model, 'dependencia'); ?>
    </div>
  </div>
  <?php endif; ?>

  <div class="titulo"> Parámetros de análisis </div>
  <div class="fila">
    <?php
    foreach ($model->cuestionario->tiposParametro as $tipoParametro) {
      echo $this->render('_field_parametro', [
        'model' => $model,
        'form' => $form,
        'tipoParametro' => $tipoParametro,
      ]);
    }
    ?>
  </div>

  <?php if (in_array('ayuda', $attributes)): ?>
  <div class="fila">
    <div class="campo ayuda grande">
      <i class="mdi mdi-text-long"></i>
      <?= $form->field($model, 'ayuda')->quillEditor(); ?>
    </div>
  </div>
  <?php endif; ?>

</div>
