<?php

use backend\widgets\ListView;
use common\helpers\Html;
use common\models\Pregunta;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

$opciones = $opciones ?? [];
?>

<div class="ficha pregunta" data-id="<?= $model->id ?>">

  <?= $this->render('_header', ['model' => $model, 'link' => false, 'opciones' => $opciones]) ?>

  <main>

    <div class="fila">
      <div class="campo">
        <div class="label"> <?= $model->getAttributeLabel('tipo') ?> </div>
        <div class="value"> <?= $model->icono; ?> <?= $model->tipo() ?> </div>
      </div>
      <div class="campo <?= !$model->puntaje ? 'error' : '' ?>">
        <div class="label"> <?= $model->getAttributeLabel('puntaje') ?> </div>
        <div class="value bold"> <?= $model->puntaje + 0 ?> </div>
      </div>
    </div>

    <?php if ($model->descripcion): ?>
    <div class="fila">
      <div class="campo grande">
        <div class="value"> <?= $model->descripcion ?> </div>
      </div>
    </div>
    <?php endif ?>

    <?php if ($model->tipo == Pregunta::TIPO_SELECCION || $model->multiple): ?>

    <div class="fila">
      <div class="campo grande">
        <div class="label"> Opciones </div>
          <div class="lista">
            <main>
            <?= ListView::widget([
                'layout' => '{items}',
                'emptyText' => '',
                'dataProvider' => new ActiveDataProvider([
                  'query' => $model->getOpciones()->orderBy('orden')
                ]),
                'itemView' => '/opcion/_lista',
              ]); ?>
            </main>
            <footer>
              <div class="opciones">
                <div class="opcion">
                  <?= Html::a(
                      '<span class="mdi mdi-plus-circle-outline"></span> Agregar opción',
                      ['/opcion/agregar',
                        'pregunta_id' => $model->id,
                        'to' => Url::current(),
                        'from' => Url::current(),
                    ], [
                      'id' => 'btn-agregar-opcion',
                      'class' => ['btn'],
                    ]) ?>
                </div>
              </div>
            </footer>
          </div>
      </div>
    </div>



    <?php endif ?>

    <?php if ($model->ayuda): ?>
    <div class="fila">
      <div class="campo grande">
        <div class="value"> <?= $model->ayuda ?> </div>
      </div>
    </div>
    <?php endif ?>

  </main>

</div>

<style>
  .opcion.puntaje {
    margin-right: 1em;
    min-width: 8em;
    text-align: right;
  }
  .ficha.lista main {
    padding: 0.5em 0;
  }
  .ficha.lista footer {
    justify-content: flex-end;
    padding: 0;
  }
  .editable .chip {
    background-color: #bbe1ff;
  }
</style>
