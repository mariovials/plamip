<?php

use common\helpers\ArrayHelper;

$preguntaParametro = $model->getPreguntaParametros()
  ->andWhere(['parametro_id' => $tipoParametro->getParametros()
  ->select('id')])
  ->one();
$parametro_id = $preguntaParametro ? $preguntaParametro->parametro_id : null;

$parametros = [];
$opciones = [];
$subTiposParametro = [];
foreach ($tipoParametro->parametros as $parametro) {
  $parametros[$parametro->id] = $parametro->nombre;
  $opciones[$parametro->id] = [
    'selected' => $parametro_id == $parametro->id,
    'data' => [
      'tipo-parametro' => $parametro->parametro_id,
      'color' => "$parametro->color",
    ],
  ];
}
?>

<div class="campo" style="max-width: calc(25% - 2em);">
  <?= $tipoParametro->icono() ?>
  <?= $form->field($model, 'parametro[]', [
      'labelOptions' => [
        'for' => "pregunta-parametro-$tipoParametro->id",
        'class' => $parametro_id ? 'activo' : '',
      ],
      'inputOptions' => [
        // 'disabled' => !$model->isNewRecord && !$parametro_id,
        'id' => "pregunta-parametro-$tipoParametro->id",
        'class' => "select-parametro",
        'data' => ['tipos-parametro' => implode(',', $tipoParametro
          ->getTiposParametro()
          ->select('id')
          ->column())]]])
    ->label($tipoParametro->nombre)
    ->dropDownList($parametros,
    ['prompt' => ['text' => '', 'options' => ['style' => ['display' => 'none']]],
    'options' => $opciones,
  ]); ?>
</div>
<?php
foreach ($tipoParametro->tiposParametro as $tipoParametro) {
  echo $this->render('_field_parametro', [
    'model' => $model,
    'form' => $form,
    'tipoParametro' => $tipoParametro]);
}
?>
