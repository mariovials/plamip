<?php

$this->title = 'Agregar Pregunta';

$this->params['cuestionario'] = $model->cuestionario;
$this->params['menu'] = 'preguntas';

$this->params['icono'] = 'help-box';
$this->params['links'] = [
  ['label' => $model->seccion->nombre, 'url' => ['/cuestionario/preguntas', 'id' => $model->cuestionario_id, '#' => "seccion-$model->seccion_id"]],
  'Agregar pregunta',
];

?>

<div class="pregunta agregar">

  <?= $this->render('_form', [
    'model' => $model,
    'attributes' => [
      'ayuda',
      'tipo',
      'nombre',
      'descripcion',
      'puntaje',
      'seccion_id',
    ],
  ]); ?>

</div>
