<?php
use backend\widgets\ListView;
use common\models\PreguntaSearch;

$this->title = 'Preguntas';
$this->params['icono'] = 'help-box';
$this->params['links'] = [ 'Preguntas'];

$searchModel = new PreguntaSearch();
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
$dataProvider->pagination = false;

?>

<div class="pregunta indice">

  <div class="ficha lista">
    <main>
      <?= ListView::widget([
        'layout' => '{items}',
        'dataProvider' => $dataProvider,
        'itemView' => '_lista',
      ]); ?>
    </main>
  </div>

</div>
