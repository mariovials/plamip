<?php

use common\helpers\Html;
use yii\helpers\Url;

$this->title = $model->nombre;

$this->params['cuestionario'] = $model->cuestionario;
$this->params['menu'] = 'preguntas';

$this->params['icono'] = 'help-box';
$this->params['links'] = [
  "Pregunta {$model->numero}",
];
$this->params['opciones'][] = Html::a(
  '<span class="mdi mdi-delete"></span> Eliminar',
  ['/pregunta/eliminar', 'id' => $model->id, 'to' => Url::to($model->cuestionario->url)],
  ['class' => 'btn-flat', 'data' => ['confirm' => '¿Está seguro?']]);
$this->params['opciones'][] = Html::a(
  '<span class="mdi mdi-pencil"></span> Editar',
  ['/pregunta/editar', 'id' => $model->id, 'from' => Url::current(), 'to' => Url::current()],
  ['class' => 'btn']);

$this->params['lateral'] = $this->render('_detalles', ['model'=> $model]);
$this->params['atras'] = Yii::$app->request->get('from', ['/cuestionario/preguntas', 'id' => $model->cuestionario_id])

?>

<div class="pregunta ver">

  <?= $this->render('_ficha', [ 'model' => $model ]); ?>

</div>
