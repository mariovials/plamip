<?php

$this->title = 'Editar ' . $model->nombre;

$this->params['cuestionario'] = $model->cuestionario;
$this->params['menu'] = 'preguntas';

$this->params['icono'] = 'help-box';
$this->params['links'] = [
  ['label' => "Pregunta {$model->numero}", 'url' => $model->url],
  'Editar',
];

?>

<div class="pregunta editar">

  <?= $this->render('_form', [
    'model' => $model,
    'attributes' => [
      'ayuda',
      'tipo',
      'nombre',
      'descripcion',
      'puntaje',
      'seccion_id',
    ],
  ]); ?>

</div>
