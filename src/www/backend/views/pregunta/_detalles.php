<?php
use common\helpers\Html;
?>

<div class="ficha pregunta" data-id="<?= $model->id ?>">

  <main>
    <div class="fila">
      <div class="campo grande">
        <div class="label"> <?= $model->getAttributeLabel('fecha_creacion') ?> </div>
        <div class="value"> <?= Yii::$app->formatter->asDateTime($model->fecha_creacion) ?> </div>
      </div>
      <?php if ($model->fecha_edicion): ?>
      <div class="campo grande">
        <div class="label"> <?= $model->getAttributeLabel('fecha_edicion') ?> </div>
        <div class="value"> <?= Yii::$app->formatter->asDateTime($model->fecha_edicion) ?> </div>
      </div>
      <?php endif ?>
    </div>
  </main>

</div>

<div class="divider"></div>

<?php foreach ($model->cuestionario->secciones as $seccion): ?>
  <?php if ($seccion->id == $model->seccion_id): ?>
    <div class="titulo">
      <span class="mdi mdi-view-agenda"></span>
      <div class="ellipsis-two-lines">
        <?= $seccion->nombre ?>
      </div>
    </div>
    <ul class="navegador">
      <?php foreach ($model->seccion->getPreguntas()->orderBy('orden')->all() as $pregunta): ?>
      <li> <?= Html::a($pregunta->nombre, $pregunta->url,
        ['class' => [
          Yii::$app->request->get('id') == $pregunta->id ? 'activo' : null,
        ]]) ?> </li>
      <?php endforeach; ?>
    </ul>
  <?php else: ?>
  <div class="titulo">
    <span class="mdi mdi-view-agenda"></span>
    <div class="ellipsis-two-lines">
      <?= $seccion->htmlLink() ?>
    </div>
  </div>
  <?php endif ?>
<?php endforeach; ?>

<style>
  .ficha > main .fila .campo.grande {
    min-height: 1.4em;
  }
  #lateral .secundario > .titulo,
  #lateral .secundario > .titulo a {
    line-height: 1em;
  }
  #lateral .secundario > .titulo {
    font-size: 0.95rem;
  }
</style>
