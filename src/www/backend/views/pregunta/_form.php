<?php
use common\widgets\ActiveForm;
use yii\helpers\Url;

$attributes = $attributes ?? array_keys($model->attributes);

$this->registerJsFile(Url::to(['/js/pregunta/form.js?v=1']), [
  'depends' => 'backend\assets\AppAsset',
]);

?>

<div class="form pregunta">

  <?php $form = ActiveForm::begin(); ?>

  <?= $this->render('_fields', [
    'form' => $form,
    'model' => $model,
    'attributes' => $attributes,
  ]) ?>

  <div class="opciones">
    <div class="opcion">
      <button class="btn">
        <span class="mdi mdi-<?= $model->isNewRecord ? 'plus-thick' : 'pencil' ?>"></span>
        <?= $model->isNewRecord ? 'Agregar' : 'Guardar' ?>
      </button>
    </div>
    <div class="opcion">
      <a href="<?= Url::to(Yii::$app->request->get('to', ['/pregunta'])) ?>" class="btn-flat solo">
        Cancelar
      </a>
    </div>
  </div>

  <?php ActiveForm::end(); ?>

</div>
