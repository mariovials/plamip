<?php

use yii\helpers\Url;

?>
<div id="head" class="<?= $this->context->menu ? '' : 'sin-menu' ?>">
  <div class="izquierda">
    <a id="logo" href="<?= Url::to(['/cuestionario']) ?>">
      <img src="<?= Yii::getAlias('@web/img/logo.png') ?>" alt="">
    </a>
  </div>
  <div class="central">
    <?= $this->render('_central') ?>
  </div>
  <div class="derecha">
    <?= $this->render('_usuario') ?>
  </div>
</div>
