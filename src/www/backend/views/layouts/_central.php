<?php

use yii\helpers\Url;

?>

<?php if ($this->context->layout == 'main'): ?>

<?php else: ?>

<div id="menu-cuestionario">
  <div id="nombre-cuestionario" class="ellipsis-two-lines">
    <?= $this->params['cuestionario']->nombre ?>
  </div>

  <ul id="menu-header">
    <li class="<?= $this->params['menu'] == 'preguntas' ? 'activo' : '' ?>">
      <a href="<?= Url::to(['/cuestionario/preguntas',
        'id' => $this->params['cuestionario']->id ]) ?>"
        title="Preguntas">
        <span class="mdi mdi-list-box"></span>
        <div class="nombre"> Preguntas </div>
      </a>
    </li>
    <li class="<?= $this->params['menu'] == 'respuestas' ? 'activo' : '' ?>">
      <a href="<?= Url::to(['/cuestionario/respuestas',
        'id' => $this->params['cuestionario']->id ]) ?>"
        title="Respuestas">
        <span class="mdi mdi-file-table"></span>
        <div class="nombre"> Respuestas </div>
      </a>
    </li>
    <li class="<?= $this->params['menu'] == 'configuracion' ? 'activo' : '' ?>">
      <a href="<?= Url::to(['/cuestionario/ver',
        'id' => $this->params['cuestionario']->id ]) ?>"
        title="Configuración">
        <span class="mdi mdi-cog-box"></span>
        <div class="nombre"> Configuración </div>
      </a>
    </li>
    <li class="">
      <a class="link-vista-previa" href="<?= Yii::$app->urlManagerFrontend
        ->createAbsoluteUrl(['/cuestionario/vista-previa',
          'id' => $this->params['cuestionario']->id,
          'from' => Url::current(),
        ]) ?>" target="_blank" title="Vista previa">
        <span class="mdi mdi-eye"></span>
      </a>
    </li>
  </ul>
</div>

<?php endif ?>
