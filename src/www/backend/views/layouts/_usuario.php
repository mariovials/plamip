<?php

use yii\helpers\Url;

?>

<ul id="botones-usuario">
  <li>
    <a href="<?= Yii::$app->urlManagerFrontend->createAbsoluteUrl(['/']) ?>" class="btn solo">
      <i class="mdi mdi-web"></i>
    </a>
  </li>
  <li>
    <a href="<?= Url::to(['/config']) ?>" class="btn solo">
      <i class="mdi mdi-cog"></i>
    </a>
  </li>
  <li id="menu-usuario" class="grande">
    <a class="btn" id="boton-usuario">
      <span class="nombre">
        <?= Yii::$app->user->identity->nombre ?>
      </span>
      <span class="mdi mdi-account-circle"></span>
    </a>
    <ul id="desplegable-usuario" class="desplegable">
      <li>
        <a href="<?= Url::to(['/sistema/salir']) ?>">
          <span class="mdi mdi-logout"></span> Salir
        </a>
      </li>
    </ul>
  </li>
</ul>
