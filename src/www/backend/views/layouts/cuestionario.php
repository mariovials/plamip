<?php

use backend\assets\CuestionarioAsset;
use backend\widgets\Breadcrumb;
use yii\helpers\Html;
use yii\helpers\Url;

CuestionarioAsset::register($this);

$atrasUrl = isset($this->params['atras'])
  ? $this->params['atras']
  : Yii::$app->request->get('from');

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
  <meta charset="<?= Yii::$app->charset ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="icon" type="image/x-icon" href="<?= Yii::getAlias('@web/favicon.ico') ?>">
  <?php $this->registerCsrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>
  <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

  <div id="pagina">

    <?= $this->render('_head'); ?>

    <div id="body" class="<?= $this->context->menu ? '' : 'sin-menu' ?>">

      <div id="contenido">
        <?php if (Yii::$app->controller->action->id != 'preguntas'): ?>
          <div id="lateral" style="min-width: 17rem; border-right: 1px solid #e7e7e7">
            <?php if (isset($this->blocks['menu'])): ?>
              <div class="secundario">
              <?= $this->blocks['menu'] ?>
              </div>
            <?php endif; ?>
          </div>
        <?php endif ?>

        <div id="principal">

        <?php if (Yii::$app->controller->action->id != 'preguntas'): ?>
        <div id="header">
          <div class="izq <?= (!isset($this->params['opciones']) || !$this->params['opciones']) ? 'sola' : '' ?>">

            <?php if ($atrasUrl): ?>
            <a class="btn-atras" href="<?= Url::to($atrasUrl) ?>">
              <span class="mdi mdi-arrow-left"></span>
            </a>
            <?php endif ?>
            <?php if (isset($this->params['icono'])): ?>
            <span class="mdi mdi-<?= $this->params['icono'] ?>"></span>
            <?php endif; ?>
            <?php
            if (!isset($this->params['links'])) {
              $this->params['links'][] = $this->title;
            }
            ?>
            <?= Breadcrumb::widget(['links' => $this->params['links']]) ?>
          </div>
          <?php if (isset($this->params['opciones'])): ?>
          <div class="opciones">
            <?php foreach($this->params['opciones'] as $opcion): ?>
              <?= $opcion ?>
            <?php endforeach; ?>
          </div>
          <?php endif; ?>
        </div>
        <?php endif ?>

          <div id="main">
            <?= $content ?>
          </div>

        </div>

        <div id="lateral">
          <?php if (isset($this->params['menu'])) { ?>
          <div class="principal">
            <ul class="menu">

            </ul>
          </div>
          <?php } ?>
          <div class="secundario">
            <?= $this->params['lateral'] ?? ''; ?>
          </div>
        </div>

      </div>

    </div>

  </div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage();
