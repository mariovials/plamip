<?php

use backend\assets\AppAsset;
use backend\widgets\Breadcrumb;
use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;

AppAsset::register($this);

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
  <meta charset="<?= Yii::$app->charset ?>">
  <link rel="icon" type="image/x-icon" href="<?= Yii::getAlias('@web/favicon.ico') ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <?php $this->registerCsrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>
  <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

  <div id="pagina">

    <?= Alert::widget() ?>

    <?= $this->render('_head'); ?>

    <div id="body" class="<?= $this->context->menu ? '' : 'sin-menu' ?>">

      <?= $this->context->menu ? $this->render('menu') : '' ?>

      <div id="contenido">

        <div id="principal">

          <div id="header">
            <div class="izq <?= (!isset($this->params['opciones']) || !$this->params['opciones']) ? 'sola' : '' ?>">
              <?php if (isset($this->params['icono'])): ?>
              <span class="mdi mdi-<?= $this->params['icono'] ?>"></span>
              <?php endif; ?>
              <?php
              if (!isset($this->params['links'])) {
                $this->params['links'][] = $this->title;
              }
              ?>
              <?= Breadcrumb::widget(['links' => $this->params['links']]) ?>
            </div>
            <?php if (isset($this->params['opciones'])): ?>
            <div class="opciones">
              <?php foreach($this->params['opciones'] as $opcion): ?>
                <?= $opcion ?>
              <?php endforeach; ?>
            </div>
            <?php endif; ?>
          </div>

          <div id="main">
            <?= $content ?>
          </div>

        </div>

        <div id="lateral">
          <?php if (isset($this->params['menu'])) { ?>
          <div class="principal">
            <ul class="menu">
              <?php
              foreach ($this->params['menu'] as $i => $item) {
                if ($item == 'divider') {
                  echo '<li class="divider"></li>';
                } else {
                  echo "<li>$item</li>";
                }
              }
              ?>
            </ul>
          </div>
          <?php } ?>
          <div class="secundario">
            <?= $this->params['lateral'] ?? ''; ?>
          </div>
        </div>

      </div>

    </div>

  </div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage();
