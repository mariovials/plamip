<?php

use yii\helpers\Url;

use common\helpers\Inflector;
use common\helpers\Html;

$suma = $model
  ->getParametros(isset($parametro) ? $parametro->id : null)
  ->sum('puntaje_maximo') + 0;
$puntaje = $model->puntaje_maximo + 0;
$listo = $suma == $puntaje && $suma > 0;
?>

<div class="tipo-parametro">
  <div class="header">
    <div class="informacion">
      <div class="icono">
        <span class="mdi mdi-<?= $model->icono ?>"></span>
      </div>
      <div class="nombre">
        <?= Html::a(Inflector::pluralize($model->nombre), $model->url) ?>
      </div>
    </div>
    <div class="opciones ocultas">
      <div class="opcion">
        <a class="btn bajo"
          href="<?= Url::to(['/tipo-parametro/agregar',
            'tipo_parametro_id' => $model->id,
            'cuestionario_id' => $model->cuestionario_id,
            'to' => Url::current(),
            'from' => Url::current(),
          ]) ?>">
          <span class="mdi mdi-plus"></span> Agregar subparámetro
        </a>
      </div>
      <div class="opcion">
        <a class="btn " href="<?= Url::to(['/tipo-parametro/editar',
          'id' => $model->id,
          'from' => Url::current(),
          'to' => Url::current()]) ?>">
          <span class="mdi mdi-pencil"></span> Editar
        </a>
      </div>
      <div class="opcion">
        <a class="btn modal-trigger" href="#modal-tipo-parametro-<?= $model->id ?>">
          <span class="mdi mdi-delete"></span> Eliminar
        </a>
      </div>
    </div>
    <div class="opciones">
      <div class="opcion puntaje_maximo <?= $listo ? '' : 'falta' ?>">
        Puntaje: <?= (!$listo) ? "$suma /" : '' ?> <?= $puntaje ?>
      </div>
    </div>
  </div>

  <div class="parametros <?= $listo ? 'listo' : '' ?>">

    <?php
    foreach ($model->getParametros()
      ->andFilterWhere(['parametro_id' => isset($parametro) ? $parametro->id : null])
      ->all() as $subparametro) {
      echo $this->render('/parametro/_lista', ['model' => $subparametro]);
    }
    ?>

    <?php if (!$listo): ?>
    <div class="parametro agregar">
      <div class="header">
        <div class="informacion">
          <div class="nombre">
            <a class="btn bajo" href="<?= Url::to(['/parametro/agregar',
              'tipo_parametro_id' => $model->id,
              'parametro_id' => isset($parametro) ? $parametro->id : null,
              'to' => Url::current(),
              'from' => Url::current()]) ?>">
              <span class="mdi mdi-<?= $model->icono ?>"></span>
              Agregar <?= $model->nombre ?>
            </a>
          </div>
        </div>
      </div>
    </div>
    <?php endif ?>

  </div>
</div>

<div class="modal dialog" id="modal-tipo-parametro-<?= $model->id ?>">
  <div class="ficha">
    <header>
      <div class="principal">
        <div class="icono"> <span class="mdi mdi-delete-outline"></span> </div>
        <div class="titulo"> <div class="nombre"> Eliminar <?= $model->nombre ?></div> </div>
      </div>
    </header>
    <main>
      Se eliminarán: <br>
      - <?= implode('<br>- ', $model->getParametros()->select('nombre')->column()) ?>

      <?php if ($model->tipoParametro): ?>
      <br> <br> Presente en: <br>
      <?= implode(', ', $model->tipoParametro->getParametros()->select('nombre')->column()) ?>
      <?php endif ?>
    </main>
    <footer>
      <div class="opciones">
        <div class="opcion">
          <a class="btn " href="<?= Url::to(['/tipo-parametro/eliminar',
            'id' => $model->tipo_parametro_id ?: $model->id,
            'from' => Url::current(),
            'to' => Url::current()]) ?>">
            <span class="mdi mdi-delete"></span> Aceptar
          </a>
        </div>
        <div class="opcion">
          <a href="#" class="btn-flat modal-close"> Cancelar </a>
        </div>
      </div>
    </footer>
  </div>
</div>
