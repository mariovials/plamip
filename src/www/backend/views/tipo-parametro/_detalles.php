<?php

use common\helpers\Inflector;
use yii\helpers\Url;

?>

<?= $this->render('/cuestionario/_detalles_tipos_parametro', [
  'model' => $model->cuestionario,
  'parametro' => null,
]) ?>

<div class="titulo">
  <span class="mdi mdi-<?= $model->icono ?>"></span>
  <div class="ellipsis-two-lines"> <?= Inflector::pluralize($model->nombre) ?> </div>
</div>
<ul class="navegador parametro">
  <?php foreach ($model->getParametros()->orderBy('parametro_id, nombre')->all() as $parametro): ?>
  <li>
    <a href="<?= Url::to($parametro->url) ?>">
      <div class="color" style="background-color: <?= $parametro->color ?>"></div>
      <div class="nombre"> <?= $parametro->nombre ?> </div>
    </a>
  </li>
  <?php endforeach ?>
</ul>

<div class="divider"></div>

<div class="ficha parametro" data-id="<?= $model->id ?>">
  <main>
    <div class="fila">
      <div class="campo">
        <div class="label"> <?= $model->getAttributeLabel('fecha_creacion') ?> </div>
        <div class="value"> <?= Yii::$app->formatter->asDatetime($model->fecha_creacion) ?> </div>
      </div>
    </div>
    <div class="fila">
      <div class="campo">
        <div class="label"> <?= $model->getAttributeLabel('fecha_edicion') ?> </div>
        <div class="value"> <?= Yii::$app->formatter->asDatetime($model->fecha_edicion) ?> </div>
      </div>
    </div>
  </main>

</div>
