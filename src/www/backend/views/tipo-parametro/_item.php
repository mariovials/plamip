<?php

use yii\helpers\Url;

use common\helpers\Inflector;

$nivel = $nivel ?? 0;
?>

<div class="item tipo-parametro">
  <div class="informacion">
    <div class="icono" style="color: <?= $model->color ?>">
      <span class="mdi mdi-<?= $model->icono ?>"></span>
    </div>
    <div class="texto">
      <div class="primario">
        <div class="campo">
          <?php if ($model->tipoParametro && $model->parametro): ?>
          <?= $model->tipoParametro->htmlLink() ?>
          <?php else: ?>
          <?= $model->htmlLink() ?>
          <?php endif ?>
        </div>
      </div>
    </div>
  </div>
  <div class="opciones">
    <?php if ($model->tipoParametro && $model->parametro): ?>
    <div class="opcion">
      <div class="chip">
        <span class="mdi mdi-sync"></span>
        Para <?= Inflector::pluralize($model->parametro->tipoParametro->nombre) ?>
      </div>
    </div>
    <?php elseif ($model->parametro): ?>
    <div class="opcion">
      <div class="chip">
        <span class="mdi mdi-sync-off"></span>
        Solo para <?= $model->parametro->nombre ?>
      </div>
    </div>
    <?php endif ?>
    <div class="opcion puntaje_maximo" style="margin-right: 1em">
      <?= $model->getAttributeLabel('puntaje_maximo') ?>: <?= $model->puntaje_maximo + 0 ?>
    </div>
    <div class="opcion">
      <a class="btn solo" href="<?= Url::to(['/tipo-parametro/eliminar',
        'id' => $model->id,
        'to' => Url::current()]) ?>">
        <span class="mdi mdi-delete"></span>
      </a>
    </div>
  </div>
</div>
