<?php

use yii\helpers\Url;

$this->title = 'Agregar Tipo de Parámetro';

$this->params['cuestionario'] = $model->cuestionario;
$this->params['menu'] = 'configuracion';

if ($model->tipoParametro) {
  $tiposParametro = [];
  $tipoParametro = $model->tipoParametro;
  while ($tipoParametro) {
    $tiposParametro[] = [
      'label' => $tipoParametro->nombre,
      'url' => ['/tipo-parametro/ver', 'id' => $tipoParametro->id],
    ];
    $tipoParametro = $tipoParametro->tipoParametro;
  }
  $this->params['links'] = array_reverse($tiposParametro);
}

if ($model->parametro) {
  $parametros = [];
  $parametro = $model->parametro;
  while ($parametro) {
    $parametros[] = [
      'label' => $parametro->nombre,
      'url' => ['/tipo-parametro/ver', 'id' => $parametro->id],
    ];
    $parametro = $parametro->parametro;
  }
  $this->params['links'] = array_reverse($parametros);
}

$tipo = $model->tipo_parametro_id ? 'Subtipo de parámetro' : 'Tipo de parámetro';
$this->params['links'][] = "Agregar $tipo";

?>

<div class="parametro agregar">

  <?php if ($model->parametro): ?>
  <div style="margin: 1em 2em">
    <div class="fila nota">
      <div class="texto">
        <div class="titulo">
          <span class="mdi mdi-information"></span>
          Se agregará este tipo de parámetro solamente a <?= $model->parametro->nombre ?>
        </div>
        <div class="descripcion">
          <a href="<?= Url::to(['/tipo-parametro/agregar',
            'tipo_parametro_id' => $model->parametro->tipoParametro->id,
            'cuestionario_id' => $model->cuestionario_id,
            'to' => Yii::$app->request->get('to', Url::current()),
            'from' => Url::current()]) ?>">
            Agregar para todos los elementos de <?= $model->parametro->tipoParametro->nombre ?>
          </a>
        </div>
      </div>
    </div>
  </div>
  <?php endif ?>

  <?php if ($model->tipoParametro): ?>
  <div style="margin: 1em 2em">
    <div class="fila nota">
      <div class="texto">
        <div class="titulo">
          <span class="mdi mdi-information"></span>
          Se agregará este tipo de parámetro para todos los elementos
          de <?= $model->tipoParametro->nombre ?>
        </div>
        <div class="descripcion">
          <?= implode(', ', $model->tipoParametro->getParametros()
            ->select('nombre')
            ->column()) ?> y todos los que se creen posteriormente
        </div>
      </div>
    </div>
  </div>
  <?php endif ?>

  <?= $this->render('_form', [
    'model' => $model,
    'attributes' => [
      'nombre',
      'descripcion',
      'puntaje_maximo',
    ],
  ]); ?>

</div>
