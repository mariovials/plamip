<?php

$this->title = 'Editar ' . $model->nombre;

$this->params['cuestionario'] = $model->cuestionario;
$this->params['menu'] = 'configuracion';
$this->params['icono'] = $model->icono;

$tiposParametros = [];
$tipoParametro = $model->tipoParametro;
while ($tipoParametro) {
  $tiposParametros[] = [
    'label' => $tipoParametro->nombre,
    'url' => ['/parametro/ver', 'id' => $tipoParametro->id],
  ];
  $tipoParametro = $tipoParametro->tipoParametro;
}
$this->params['links'] = array_reverse($tiposParametros);
$this->params['links'][] = ['label' => $model->nombre, 'url' => $model->url];
$this->params['links'][] = 'Editar';


?>

<div class="parametro editar">

  <?= $this->render('_form', [
    'model' => $model,
    'attributes' => [
      'nombre',
      'descripcion',
      'puntaje_maximo',
    ],
  ]); ?>

</div>
