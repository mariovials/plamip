<?php

use common\helpers\Html;
use yii\helpers\Url;
use common\helpers\Inflector;

$attributes = $attributes ?? array_keys($model->attributes);
$mdi = Html::a(
  'Para ver las lista completa de iconos disponibles visite MDI Icons',
  'https://pictogrammers.com/library/mdi/',
  ['target' => '_blank']);

$model->puntaje_maximo = $model->puntaje_maximo ? $model->puntaje_maximo + 0 : null;



$BASE_URL = Url::to(['/']);
$this->registerJs("
  const BASE_URL = '$BASE_URL'
", $this::POS_BEGIN);

?>

<div class="filas">

  <div class="fila">
    <div class="campo nombre grande">
      <i class="mdi mdi-text-box"></i>
      <?= $form->field($model, 'nombre'); ?>
    </div>
  </div>

  <div class="fila">
    <div class="campo puntaje_maximo" style="width: 16em">
      <i class="mdi mdi-counter"></i>
      <?= $form->field($model, 'puntaje_maximo'); ?>
    </div>

    <div class="campo icono" id="campo-icono">
      <i class="mdi mdi-chart-donut"></i>
      <?= $form->field($model, 'icono')->hint($model->getAttributeHint('icono') . $mdi); ?>
      <div class="mdi-picker"> <div class="icons"></div> </div>
      <div class="mdi-picker-preview">
        <span class="mdi mdi-<?= $model->icono ?>"></span>
      </div>
    </div>
  </div>

  <div class="fila">
    <div class="campo descripcion grande">
      <i class="mdi mdi-text"></i>
      <?= $form->field($model, 'descripcion')->textarea(); ?>
    </div>
  </div>

</div>

<style>
  #campo-color {
    display: flex;
    align-items: flex-start;
    width: 19em;
  }
  .color-picker-preview {
    min-width: 2.8rem;
    min-height: 2.8rem;
    border-radius: 0.5rem;
    margin-left: 0.5rem;
    cursor: pointer;
  }
  .color-picker {
    display: none;
    position: absolute;
    z-index: 1000;
    background-color: #FFF;
    grid-template-columns: repeat(5, 1.9rem);
    grid-template-rows: repeat(4, 1.9rem);
    gap: 0.5rem;
    border-radius: 0.5rem;
    border: 1px solid #EEE;
    padding: 0.7em;
    border: 2px solid #DDD;
    top: 2.9em;
    right: 0;
    width: 100%;
  }
  .color-picker .color {
    border-radius: 0.2rem;
    cursor: pointer;
  }
  .color-picker.activo {
    display: grid;
  }


  #campo-icono {
    display: flex;
    align-items: flex-start;
    width: 19em;
  }
  .mdi-picker {
    display: none;
    position: absolute;
    z-index: 1000;
    overflow-y: auto;
    width: 27.7rem;
    top: 2.9em;
    right: 0;
    background-color: #FFF;
    border: 2px solid #DDD;
    overflow: hidden;
    border-radius: 0.5rem;
  }
  .mdi-picker .icons {
    overflow: hidden;
    overflow-y: scroll;
    display: flex;
    flex-wrap: wrap;
    align-items: flex-start;
    max-height: 18em;
  }
  .mdi-picker.activo {
    display: block;
  }
  .mdi-picker .icons .mdi {
    font-size: 1.6rem;
    padding: 0.2em 0.5em;
    border: 1px solid #EEE;
    cursor: pointer;
    color: inherit;
  }
  .mdi-picker-preview .mdi {
    font-size: 2rem;
  }
  .mdi-picker-preview {
    color: #F5F5F5;
    background-color: #546E7A;
    min-width: 2.8rem;
    min-height: 2.8rem;
    border-radius: 0.5rem;
    margin-left: 0.5rem;
    cursor: pointer;
    display: flex;
    justify-content: center;
    align-items: center;
  }

</style>

<script>

  /**
   * MDI Picker
   */
  mdiPicker = document.querySelector('.mdi-picker')
  mdiPreview = document.querySelector('.mdi-picker-preview')
  inputMdi = document.getElementById('tipoparametro-icono')
  document.querySelector('.field-tipoparametro-icono').appendChild(mdiPicker)
  inputMdi.addEventListener('focus', function() {
    mdiPicker.classList.add('activo')
    clickEnMdiPicker = false
  })
  let clickEnMdiPicker = false
  inputMdi.addEventListener('blur', function() {
    if (!clickEnMdiPicker) mdiPicker.classList.remove('activo')
  })
  mdiPicker.addEventListener('mousedown', function() {
    clickEnMdiPicker = true
  })
  inputMdi.addEventListener('input', function() {
    mdiIcon = `<span class="mdi mdi-${this.value}"></span>`
    mdiPreview.innerHTML = mdiIcon
  })
  mdiPreview.addEventListener('click', function(e) {
    inputMdi.focus()
    e.stopPropagation()
  })
  inputMdi.onkeyup = function (e) {
    iconos = mdiPicker.querySelectorAll('.mdi')
    if (this.value == '') {
      iconos.forEach(icono => {
          icono.style.display = 'block'
      })
    }
    iconos.forEach(icono => {
      if (icono.dataset.alias.indexOf(this.value) === -1)
        icono.style.display = 'none'
      else
        icono.style.display = 'block'
    })
  }

  fetch(BASE_URL + 'js/parametro/mdi-7.4.47.json')
    .then(response => { return response.json() })
    .then(data => {
      for (var i = 0; i < data.i.length; i++) {
        icon = data.i[i]
        alias = icon.n + ' ' + icon.al.join
        if (alias.indexOf('format') == -1 && alias.indexOf('graph') !== -1 || alias.indexOf('chart') != -1) {
          fontIcon = `<span class="mdi mdi-${icon.n}" data-codigo="${icon.n}" data-alias="${alias}"></span>`
          mdiPicker.querySelector('.icons').insertAdjacentHTML('beforeend', fontIcon)
        }
      }
    })
    .then(function() {
      initPicker()
    })

  initPicker = function() {
    iconos = mdiPicker.querySelectorAll('.mdi')
    iconos.forEach(icono => {
      icono.addEventListener('click', function() {
        inputMdi.value = this.dataset.codigo
        inputMdi.dispatchEvent(new Event('input'))
        inputMdi.dispatchEvent(new Event('change'))
        mdiPicker.classList.remove('activo')
      })
    })
  }
</script>
