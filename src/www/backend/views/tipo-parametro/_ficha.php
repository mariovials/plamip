<?php

use yii\helpers\Url;
use common\helpers\Html;
use common\helpers\Inflector;

$suma = $model->getParametros()->sum('puntaje_maximo') + 0;
if ($model->parametro) {
  $error = $model->parametro->puntaje_maximo != $suma;
} else if ($model->tipoParametro) {
  $suma = $suma / $model->tipoParametro->getParametros()->count();
  $error = $model->puntaje_maximo + 0 != $suma;
} else {
  $error = $model->puntaje_maximo != $suma;
}

?>

<div class="ficha tipo-parametro" data-id="<?= $model->id ?>">
  <br>
  <main>

    <div class="fila">

      <?php if ($model->tipoParametro): ?>
      <div class="campo">
        <div class="label"> Para cada </div>
        <div class="value"> <?= $model->tipoParametro->htmlLink() ?> </div>
      </div>
      <?php endif ?>

      <?php if ($model->parametro): ?>
      <div class="campo">
        <div class="label"> Solo para </div>
        <div class="value"> <?= $model->parametro->htmlLink() ?> </div>
      </div>
      <?php endif ?>

      <div class="campo <?= $error ? 'error' : '' ?>">
        <div class="label"> <?= $model->getAttributeLabel('puntaje_maximo') ?> </div>
        <div class="value">
          <?php if ($error): ?>
            <?= $suma ?> /
          <?php endif; ?>
          <b><?= $model->puntaje_maximo + 0 ?></b>
        </div>
      </div>

      <div class="campo">
        <div class="label"> <?= $model->getAttributeLabel('icono') ?> </div>
        <div class="value"> <span class="mdi mdi-<?= $model->icono ?>"></span> </div>
      </div>

    </div>

    <?php if ($model->descripcion): ?>
    <div class="fila">
      <div class="campo">
        <div class="label"> <?= $model->getAttributeLabel('descripcion') ?> </div>
        <div class="value"> <?= nl2br($model->descripcion) ?> </div>
      </div>
    </div>
    <?php endif ?>

    <div class="fila" style="display: flex; flex-wrap: nowrap;">
      <div class="campo grande">
        <div class="label"> Subparámetros </div>
        <div class="value">
          <div class="lista">
            <main>
              <div class="items">
                <?php foreach ($model->tiposParametro as $tipoParametro): ?>
                  <?= $this->render('/tipo-parametro/_item', ['model' => $tipoParametro]) ?>
                <?php endforeach ?>
                <a class="btn bajo"
                  href="<?= Url::to(['/tipo-parametro/agregar',
                    'tipo_parametro_id' => $model->id,
                    'cuestionario_id' => $model->cuestionario_id,
                    'to' => Url::current(),
                    'from' => Url::current(),
                  ]) ?>">
                  <span class="mdi mdi-plus"></span> Agregar subparámetro
                </a>
                <div class="hint">
                  Para todos elementos de <?= $model->nombre ?>
                </div>
              </div>
            </main>
          </div>
        </div>
      </div>
    </div>

    <div class="fila">
      <div class="campo grande">
        <div class="label"> Rangos de resultados </div>
        <div class="value">

          <ul id="rangos">
            <?php foreach ($model->rangos as $rango): ?>
            <li>
              <div class="informacion">
                <?= $rango->nombre ?>
                <?= $rango->desde + 0 ?>% a <?= $rango->hasta + 0 ?>%
              </div>
              <div class="opciones">
                <div class="opcion">
                  <?= Html::a('<span class="mdi mdi-pencil"></span>',
                    ['/rango/editar', 'id' => $rango->id,
                      'from' => Url::current(),
                      'to' => Url::current()],
                    ['class' => 'btn bajo solo']) ?>
                </div>
                <div class="opcion">
                  <?= Html::a('<span class="mdi mdi-delete"></span>',
                    ['/rango/eliminar', 'id' => $rango->id, 'to' => Url::current()],
                    ['class' => 'btn bajo solo']) ?>
                </div>
              </div>
            </li>
            <?php endforeach ?>
          </ul>
          <?php if ($rango && $rango->hasta != 100): ?>
          <a class="btn bajo"
            href="<?= Url::to(['/rango/agregar',
              'tipo_parametro_id' => $model->id,
              'to' => Url::current(),
              'from' => Url::current(),
            ]) ?>">
            <span class="mdi mdi-plus"></span> Agregar rango
          </a>
          <?php endif ?>

          <?php if ($model->rangos): ?>
          <?php
          $mensajes = [];
          $desde = 0;
          $hasta = 0;
          foreach ($model->rangos as $rango) {
            if ($rango->desde != $hasta) {
              $mensajes[] = "Rango $rango->nombreRango debe comenzar en $hasta";
            }
            $desde = $rango->desde + 0;
            $hasta = $rango->hasta + 0;
          }
          if ($hasta != 100) {
            $mensajes[] = "Rango no termina en 100.";
          }
          ?>
          <?php if ($mensajes): ?>
          <div id="errores-rango">
            <b>Problemas en rangos</b>: <br>
            <ul>
              <?php foreach ($mensajes as $mensaje): ?>
              <li> <?= $mensaje ?> </li>
              <?php endforeach ?>
            </ul>
          </div>
          <?php endif ?>
          <?php endif ?>

        </div>
      </div>
    </div>

  </main>
</div>

<style>
#errores-rango {
  background: #FFEBEE;
  padding: 0.6em 1em;
  color: #D32F2F;
  border: 1px solid #EF9A9A;
  border-radius: 0.3em;
  margin-top: 1em;
  font-size: 0.95em;
  width: fit-content;
}
.ficha.tipo-parametro .hint {
  font-size: 0.9em;
  margin-top: 0.3em
}
#rangos {
}
div.lista .items .item {
  padding: 0.1em 1em;
}
#rangos {
  width: fit-content;
}
#rangos li {
  display: flex;
  align-items: center;
  margin-bottom: 0.2em;
  padding-left: 1em;
}
#rangos li .opciones .opcion {
  margin-left: 0.2em;
}
#rangos li .opciones {
  display: flex;
}
#rangos li .informacion {
  min-width: 17em;
}
#rangos li:hover {
  background: #FAFAFA;
}
.campo.error {
  color: red;
}
</style>
