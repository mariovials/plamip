<?php

use yii\helpers\Url;
use common\helpers\Inflector;

?>

<div class="ficha parametros" id="ficha-parametros">
  <header>
    <div class="principal">
      <div class="titulo">
        <div class="nombre">
          <?= Inflector::pluralize($model->nombre) ?>
        </div>
      </div>
    </div>
  </header>
  <main>
    <?php if ($model->tipoParametro): ?>
    <?php foreach ($model->tipoParametro->parametros as $parametroPadre): ?>
    <div class="fila">
      <div class="campo grande">
        <div class="label">
          <span class="mdi mdi-<?= $parametroPadre->icono ?>"
            style="color: <?= $parametroPadre->color ?>"></span>
          <?= $parametroPadre->nombre ?>
        </div>
        <div class="value">
          <ul class="lista-parametros">
            <?php foreach ($model
              ->getParametros()
              ->andWhere(['parametro_id' => $parametroPadre->id])
              ->all() as $parametro):
            $activo = $this->context->id == 'parametro'
              && $parametro->id == Yii::$app->request->get('id')
            ?>
            <li>
              <a href="<?= Url::to(['/parametro/ver',
                'id' => $parametro->id,
                'from' => Url::current(),
                ]) ?>"
                class="<?= $activo ? 'activo' : '' ?>">
                <div class="color" style="background-color: <?= $parametro->color ?>"></div>
                <?= $parametro->nombre ?>
              </a>
            </li>
            <?php endforeach ?>
          </ul>
        </div>
      </div>
    </div>
    <?php endforeach ?>
    <?php else: ?>
    <div class="fila">
      <div class="campo grande">
        <div class="value">
          <ul class="lista-parametros">
            <?php
            foreach ($model->parametros as $parametro):
            $activo = $this->context->id == 'parametro'
              && $parametro->id == Yii::$app->request->get('id')
            ?>
            <li>
              <a href="<?= Url::to(['/parametro/ver',
                'id' => $parametro->id,
                'from' => Url::current(),
                ]) ?>"
                class="<?= $activo ? 'activo' : '' ?>">
                <div class="color" style="background-color: <?= $parametro->color ?>"></div>
                <?= $parametro->nombre ?>
              </a>
            </li>
            <?php endforeach ?>
          </ul>
        </div>
      </div>
    </div>
    <?php endif ?>
  </main>
</div>





<style>

  #ficha-parametros {
    background: #f6f8f9;
    margin: 1em;
  }

  .lista-parametros li {
    margin-bottom: 0.1em;
  }
  .lista-parametros li a {
    display: flex;
    align-items: center;
    padding: 0.1em 0.6em;
    border-radius: 2em;
  }
  .lista-parametros li a.activo {
    background: #0001;
  }
  .lista-parametros li a .color {
    width: 1em;
    height: 1em;
    border-radius: 0.3em;
    margin-right: 0.5em;
  }
  .ficha.tipo-parametro .puntaje {
    margin-bottom: 0.5em;
    font-weight: 600;
  }

  .parametro.ver .ficha.parametro {
    width: 100%;
  }
  .parametro.ver .ficha.parametros {
    min-width: 22em;
  }
  .parametro.ver {
    display: flex;
  }
  .parametro.ver .ficha.parametros {
    background: #f6f8f9;
    margin: 1em;
  }
</style>
