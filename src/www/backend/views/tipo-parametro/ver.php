<?php

use common\helpers\Html;
use yii\helpers\Url;

$this->title = $model->nombre;

$this->params['cuestionario'] = $model->cuestionario;
$this->params['menu'] = 'configuracion';
$this->params['atras'] = Yii::$app->request->get('from', $model->cuestionario->url);

$this->params['icono'] = $model->icono;

$tiposParametros = [];
$tipoParametro = $model->tipoParametro;
while ($tipoParametro) {
  $tiposParametros[] = [
    'label' => $tipoParametro->nombre,
    'url' => ['/tipo-parametro/ver', 'id' => $tipoParametro->id],
  ];
  $tipoParametro = $tipoParametro->tipoParametro;
}
$this->params['links'] = array_reverse($tiposParametros);
$this->params['links'][] = $model->nombre;

$this->params['opciones'][] = Html::a(
    '<span class="mdi mdi-delete"></span> Eliminar',
    ['/tipo-parametro/eliminar', 'id' => $model->id,
      'from' => Url::current(),
      'to' => $model->tipoParametro->url ?? $model->cuestionario->url,
    ],
    ['class' => 'btn-flat', 'data' => ['confirm' => '¿Está seguro?']]);
$this->params['opciones'][] = Html::a(
    '<span class="mdi mdi-pencil"></span> Editar',
    ['/tipo-parametro/editar', 'id' => $model->id, 'from' => Url::current(), 'to' => Url::current()],
    ['class' => 'btn-flat']);

$this->params['lateral'] = $this->render('_detalles', ['model'=> $model]);

?>

<div class="tipo-parametro ver">

  <?= $this->render('_ficha', ['model' => $model]); ?>

</div>

<style>
  .tipo-parametro.ver .ficha.tipo-parametro {
    width: 100%;
  }
  .tipo-parametro.ver .ficha.parametros {
    min-width: 22em;
  }
  .tipo-parametro.ver {
    display: flex;
  }
</style>
