<?php

$this->title = 'Editar ' . $model->nombre;

$this->params['cuestionario'] = $model->tipoParametro->cuestionario;
$this->params['menu'] = 'configuracion';
$this->params['atras'] = $model->tipoParametro->cuestionario->url;

$tipoParametro = $model->tipoParametro;
while ($tipoParametro) {
  $tiposParametro[] = [
    'label' => $tipoParametro->nombre,
    'url' => ['/tipo-parametro/ver', 'id' => $tipoParametro->id],
  ];
  $tipoParametro = $tipoParametro->tipoParametro;
}
$this->params['links'] = array_reverse($tiposParametro);

$this->params['icono'] = $model->icono;
$this->params['links'][] = ['label' => $model->nombre, 'url' => $model->url];
$this->params['links'][] = "Editar";

?>

<div class="parametro editar">

  <?= $this->render('_form', [
    'model' => $model,
    'attributes' => [
      'nombre',
      'descripcion',
      'puntaje_maximo',
    ],
  ]); ?>

</div>
