<?php

use common\helpers\Html;

$attributes = $attributes ?? array_keys($model->attributes);
$mdi = Html::a(
  'Visite MDI Icons',
  'https://pictogrammers.com/library/mdi/',
  ['target' => '_blank']);

$model->puntaje_maximo = $model->puntaje_maximo ? $model->puntaje_maximo + 0 : null;
?>

<div class="filas">

  <?php if (in_array('nombre', $attributes)): ?>
  <div class="fila">
    <div class="campo nombre grande">
      <i class="mdi mdi-text-box"></i>
      <?= $form->field($model, 'nombre'); ?>
    </div>
  </div>
  <?php endif; ?>

  <div class="fila">
    <div class="campo puntaje_maximo">
      <i class="mdi mdi-counter"></i>
      <?= $form->field($model, 'puntaje_maximo')->number(['step' => '0.1']); ?>
    </div>
    <div class="campo color" id="campo-color">
      <i class="mdi mdi-palette"></i>
      <?= $form->field($model, 'color'); ?>
      <div class="color-picker-preview"></div>
      <div class="color-picker">
        <div class="color" style="background-color: #e53935;" data-color="#e53935">abc</div>
        <div class="color" style="background-color: #e91e63;" data-color="#e91e63">abc</div>
        <div class="color" style="background-color: #8e24aa;" data-color="#8e24aa">abc</div>
        <div class="color" style="background-color: #673ab7;" data-color="#673ab7">abc</div>
        <div class="color" style="background-color: #3949ab;" data-color="#3949ab">abc</div>

        <div class="color" style="background-color: #1e88e5;" data-color="#1e88e5">abc</div>
        <div class="color" style="background-color: #03a9f4;" data-color="#03a9f4">abc</div>
        <div class="color" style="background-color: #00bcd4;" data-color="#00bcd4">abc</div>
        <div class="color" style="background-color: #009688;" data-color="#009688">abc</div>
        <div class="color" style="background-color: #43a047;" data-color="#43a047">abc</div>

        <div class="color" style="background-color: #8bc34a;" data-color="#8bc34a">abc</div>
        <div class="color" style="background-color: #b0c102;" data-color="#b0c102">abc</div>
        <div class="color" style="background-color: #d3be00;" data-color="#d3be00">abc</div>
        <div class="color" style="background-color: #e1a900;" data-color="#e1a900">abc</div>
        <div class="color" style="background-color: #ff9800;" data-color="#ff9800">abc</div>

        <div class="color" style="background-color: #ff5722;" data-color="#ef6c00">abc</div>
        <div class="color" style="background-color: #795548;" data-color="#795548">abc</div>
        <div class="color" style="background-color: #9e9e9e;" data-color="#9e9e9e">abc</div>
        <div class="color" style="background-color: #607d8b;" data-color="#607d8b">abc</div>
        <div class="color" style="background-color: #37474f;" data-color="#37474f">abc</div>
      </div>
    </div>
  </div>

  <?php if (in_array('descripcion', $attributes)): ?>
  <div class="fila">
    <div class="campo descripcion grande">
      <i class="mdi mdi-text"></i>
      <?= $form->field($model, 'descripcion')->textarea(); ?>
    </div>
  </div>
  <?php endif; ?>

</div>

<style>
  #campo-color {
    display: flex;
    align-items: flex-start
  }
  .color-picker-preview {
    min-width: 2.8rem;
    min-height: 2.8rem;
    border-radius: 0.5rem;
    margin-left: 0.5rem;
    cursor: pointer;
  }
  .color-picker {
    display: none;
    position: absolute;
    z-index: 1000;
    background-color: #FFF;
    grid-template-columns: repeat(5, 1.9rem);
    grid-template-rows: repeat(4, 1.9rem);
    gap: 0.5rem;
    border-radius: 0.5rem;
    border: 1px solid #EEE;
    padding: 0.7em;
    border: 2px solid #DDD;
    top: 2.9em;
    right: 0;
    width: 100%;
  }
  .color-picker .color {
    border-radius: 0.2rem;
    cursor: pointer;
    color: #FFF;
  }
  .color-picker.activo {
    display: grid;
  }

</style>

<script>

  /**
   * Color picker
   */
  colorPicker = document.querySelector('.color-picker')
  document.querySelector('.field-parametro-color').appendChild(colorPicker)

  inputColor = document.getElementById('parametro-color')
  inputColor.setAttribute('maxlength', 7)
  inputColor.onkeydown = function (e) {
    if (this.value == '#' && e.key == '#') return false
    if (e.code == 'Backspace' && this.value == '#') return false
    return /[#0-9a-fA-F]/i.test(e.key)
  }
  inputColor.onkeyup = function (e) {
    if (this.value.length <= 1 && this.value != '#') {
      this.value = '#'
    }
  }
  inputColor.addEventListener('focus', function() {
    colorPicker.classList.add('activo')
    clickEnColorPicker = false
  })
  let clickEnColorPicker = false
  inputColor.addEventListener('blur', function() {
    if (!clickEnColorPicker) colorPicker.classList.remove('activo')
  })
  inputColor.addEventListener('input', function() {
    previewColor.style.backgroundColor = this.value
  })
  colorPicker.addEventListener('mousedown', function() {
    clickEnColorPicker = true
  })

  previewColor = document.querySelector('.color-picker-preview')
  previewColor.style.backgroundColor = inputColor.value
  previewColor.addEventListener('click', function(e) {
    inputColor.focus()
    e.stopPropagation()
  })

  colores = colorPicker.querySelectorAll('.color')
  colores.forEach(color => {
    color.addEventListener('click', function() {
      inputColor.value = this.dataset.color
      inputColor.dispatchEvent(new Event('input'))
      colorPicker.classList.remove('activo')
    })
  })
</script>
