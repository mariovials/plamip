<?php

$this->title = "Agregar {$model->tipoParametro->nombre}";

$this->params['cuestionario'] = $model->tipoParametro->cuestionario;
$this->params['menu'] = 'configuracion';

$a = '';
if ($model->tipoParametro->tipoParametro) {
  $a = "a {$model->tipoParametro->tipoParametro->nombre}";
}
$this->params['icono'] = $model->tipoParametro->icono;

$parametros = [];
$parametro = $model->parametro;
while ($parametro) {
  $parametros[] = [
    'label' => $parametro->nombre,
    'url' => ['/parametro/ver', 'id' => $parametro->id],
  ];
  $parametro = $parametro->parametro;
}
$this->params['links'] = array_reverse($parametros);
$this->params['links'][] = $this->title;

?>

<div class="parametro agregar">

  <?= $this->render('_form', [
    'model' => $model,
    'attributes' => [
      'nombre',
      'descripcion',
      'puntaje_maximo',
    ],
  ]); ?>

</div>
