<?php

use common\helpers\Inflector;
use yii\helpers\Url;

$suma = $model->getParametros()->sum('puntaje_maximo') + 0;
$puntaje = $model->puntaje_maximo + 0;
$listo = !$model->tipoParametro->getTiposParametro()->exists();

?>

<?= $this->render('/cuestionario/_detalles_tipos_parametro', [
  'model' => $model->tipoParametro->cuestionario,
  'parametro' => $model,
]) ?>


<div class="titulo">
  <span class="mdi mdi-<?= $model->tipoParametro->icono ?>"></span>
  <div class="ellipsis-two-lines"> <?= Inflector::pluralize($model->tipoParametro->nombre) ?> </div>
</div>
<ul class="navegador parametro">
  <?php foreach ($model->tipoParametro
    ->getParametros()
    ->orderBy('parametro_id, nombre')
    ->all() as $parametro): ?>
  <li>
    <a href="<?= Url::to($parametro->url) ?>"
      class="<?= Yii::$app->request->get('id') == $parametro->id ? 'activo' : '' ?>">
      <div class="color" style="background-color: <?= $parametro->color ?>"></div>
      <div class="nombre"> <?= $parametro->nombre ?> </div>
    </a>
  </li>
  <?php endforeach ?>
</ul>
<div class="divider"></div>

<div class="ficha parametro" data-id="<?= $model->id ?>">

  <main>
    <div class="fila">
      <div class="campo">
        <div class="label"> Puntaje </div>
        <div class="value">
        <?= (!$listo) ? "$suma /" : '' ?> <?= $puntaje ?></div>
      </div>
    </div>
    <div class="fila">
      <div class="campo">
        <div class="label"> <?= $model->getAttributeLabel('fecha_creacion') ?> </div>
        <div class="value"> <?= Yii::$app->formatter->asDatetime($model->fecha_creacion) ?> </div>
      </div>
    </div>
    <div class="fila">
      <div class="campo">
        <div class="label"> <?= $model->getAttributeLabel('fecha_edicion') ?> </div>
        <div class="value"> <?= Yii::$app->formatter->asDatetime($model->fecha_edicion) ?> </div>
      </div>
    </div>
  </main>

</div>
