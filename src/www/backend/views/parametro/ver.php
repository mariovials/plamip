<?php

use common\helpers\Html;
use yii\helpers\Url;

$this->title = $model->nombre;

$this->params['cuestionario'] = $model->tipoParametro->cuestionario;
$this->params['menu'] = 'configuracion';

$this->params['icono'] = $model->icono;
$this->params['atras'] = Yii::$app->request->get('from', $model->tipoParametro->cuestionario->url);

$parametros = [];
$parametro = $model->parametro;
while ($parametro) {
  $parametros[] = [
    'label' => $parametro->nombre,
    'url' => ['/parametro/ver', 'id' => $parametro->id],
  ];
  $parametro = $parametro->parametro;
}
$this->params['links'] = array_reverse($parametros);
$this->params['links'][] = $model->nombre;

$this->params['opciones'][] = Html::a(
    '<span class="mdi mdi-delete"></span> Eliminar',
    ['/parametro/eliminar', 'id' => $model->id, 'from' => Url::current()],
    ['class' => 'btn-flat', 'data' => ['confirm' => '¿Está seguro?']]);
$this->params['opciones'][] = Html::a(
    '<span class="mdi mdi-pencil"></span> Editar',
    ['/parametro/editar', 'id' => $model->id, 'from' => Url::current(), 'to' => Url::current()],
    ['class' => 'btn-flat']);

$this->params['lateral'] = $this->render('_detalles', ['model'=> $model]);

?>

<div class="parametro ver">

  <?= $this->render('_ficha', ['model' => $model]); ?>

</div>
