<?php

use common\helpers\Inflector;
use yii\helpers\Url;
use common\helpers\Html;

?>

<div class="ficha parametro" data-id="<?= $model->id ?>">
  <br>
  <main>

    <div class="fila">
      <div class="campo">
        <div class="label"> <?= $model->getAttributeLabel('tipo_parametro_id') ?> </div>
        <div class="value"> <?= $model->tipoParametro->htmlLink() ?> </div>
      </div>
      <div class="campo">
        <div class="label"> Puntaje </div>
        <div class="value">
          <b><?= $model->puntaje_maximo + 0 ?></b>
          /<?= $model->tipoParametro->puntaje_maximo + 0 ?>
        </div>
      </div>
      <div class="campo color">
        <div class="label"> <?= $model->getAttributeLabel('color') ?> </div>
        <div class="value" style="background: <?= $model->color ?>">
        </div>
      </div>
    </div>

    <?php if ($model->descripcion): ?>
    <div class="fila">
      <div class="campo">
        <div class="label"> <?= $model->getAttributeLabel('descripcion') ?> </div>
        <div class="value"> <?= $model->descripcion ?> </div>
      </div>
    </div>
    <?php endif ?>

    <div class="fila">
      <div class="campo grande">
        <div class="label"> Subparámetros </div>
        <div class="value">
          <div class="lista">
            <main>
              <div class="items">
                <?php foreach ($model->tiposParametro as $tipoParametro): ?>
                <?= $this->render('/tipo-parametro/_item', ['model' => $tipoParametro]) ?>
                <?php endforeach ?>
                <a class="btn bajo" href="<?= Url::to(['/tipo-parametro/agregar',
                  'parametro_id' => $model->id,
                  'from' => Url::current(),
                  ]) ?>">
                  <span class="mdi mdi-plus"></span> Agregar subparámetro
                </a>
              </div>
            </main>
          </div>
        </div>
      </div>
    </div>

    <div class="fila">
      <div class="campo grande">
        <div class="label"> Resultados </div>
        <div class="value">

          <ul id="rangos-resultado">
            <?php
            $rangos = $model->tipoParametro->getRangos()->orderBy('desde ASC');
            if ($model->puntaje_maximo == 0) {
              $rangos = $rangos->limit(1)->all();
            } else {
              $rangos = $rangos->all();
            }
            foreach ($rangos as $rango):
              $resultado = $model->getParametroRango($rango->id);
            ?>
            <li>
              <div class="informacion">
                <div class="rango">
                  <?= $model->puntaje_maximo > 0
                    ? $rango->nombreRango : 'Resultados para todos (Parámetro sin puntaje)' ?>
                </div>
                <div class="texto">
                  <?php if ($resultado->texto): ?>
                    <?= nl2br($resultado->texto) ?>
                  <?php else: ?>
                  <span style="color: red">
                    No definido
                  <?php endif ?>
                  </span>
                </div>
              </div>
              <div class="opciones">
                <div class="opcion">
                  <?= Html::a('<span class="mdi mdi-pencil"></span>',
                    ['/parametro-rango/editar',
                      'id' => $resultado->id,
                      'from' => Url::current(),
                      'to' => Url::current()],
                    ['class' => 'btn bajo solo']) ?>
                </div>
              </div>
            </li>
            <?php endforeach ?>
          </ul>
        </div>
      </div>
    </div>

  </main>

</div>

<style>
.ficha.parametro .campo.color .value {
  width: 8em;
  height: 1.4em;
  border-radius: 0.22em;
}

.ficha.parametro .hint {
  font-size: 0.9em;
  margin-top: 0.3em
}

#rangos-resultado li {
  margin-bottom: 0.5em;
  display: flex;
  justify-content: space-between;
}

#rangos-resultado li .opciones {
  display: flex;
}
#rangos-resultado li .opciones .opcion {
  margin-left: 0.2em;
}

#rangos-resultado li .informacion .rango {
  font-size: 0.9em;
  font-weight: bold;
}
#rangos-resultado li .informacion .texto {
  font-size: 0.95em;
}

</style>
