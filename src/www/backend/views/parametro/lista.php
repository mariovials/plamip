<?php
use common\models\Parametro;
use backend\widgets\ListView;
use common\helpers\Html;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

$this->title = 'Parámetros de análisis';

$this->params['cuestionario'] = $model->cuestionario;
$this->params['menu'] = 'configuracion';

$this->params['icono'] = 'chart-box';
$this->params['links'] = [ 'Parámetros de análisis'];
$this->params['opciones'][] = Html::a(
  '<span class="mdi mdi-plus"></span> Agregar',
  ['/parametro/agregar', 'from' => Url::current(), 'to' => Url::current()],
  [ 'class' => 'btn'],
);
?>

<div class="parametro indice">

  <div class="ficha lista">
    <main>
      <?= ListView::widget([
        'dataProvider' => new ActiveDataProvider([
          'query' => Parametro::find(),
          'sort' => ['attributes' => ['nombre']],
          'pagination' => ['pageSize' => 10],
        ]),
        'itemView' => '_lista',
      ]); ?>
    </main>
  </div>

</div>
