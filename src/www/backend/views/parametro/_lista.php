<?php

use yii\helpers\Url;

$puntaje = $model->puntaje_maximo + 0;
if ($model->getParametros()->exists()) {
  $suma = $model->getParametros()->sum('puntaje_maximo') + 0;
  $listo = $suma == $puntaje && $suma > 0;
} else {
  $listo = true;
}
?>

<div class="parametro" id="parametro-<?= $model->id ?>">
  <div class="header">
    <div class="informacion">
      <div class="icono" style="background-color: <?= $model->color ?>">
      </div>
      <div class="nombre" style="">
        <?= $model->htmlLink() ?>
      </div>
    </div>
    <div class="opciones ocultas">
      <div class="opcion">
        <a class="btn bajo" href="<?= Url::to(['/tipo-parametro/agregar',
          'parametro_id' => $model->id,
          'to' => Url::current(),
          'from' => Url::current(),
          ]) ?>">
          <span class="mdi mdi-plus"></span> Agregar subparámetro
        </a>
      </div>
      <div class="opcion">
        <a class="btn" href="<?= Url::to(['/parametro/editar',
          'id' => $model->id,
          'from' => Url::current(),
          'to' => Url::current()]) ?>">
          <span class="mdi mdi-pencil"></span> Editar
        </a>
      </div>
      <div class="opcion">
        <a class="btn modal-trigger" href="#modal-parametro-<?= $model->id ?>">
          <span class="mdi mdi-delete"></span> Eliminar
        </a>
      </div>
    </div>
    <div class="opciones">
      <div class="opcion puntaje_maximo <?= $listo ? '' : 'falta' ?>">
        Puntaje: <?= (!$listo) ? "$suma /" : '' ?> <?= $puntaje ?>
      </div>
    </div>
  </div>
  <?php
  foreach ($model->tiposParametro as $tipoParametro) {
    echo $this->render('/tipo-parametro/_lista', [
      'model' => $tipoParametro,
      'parametro' => $model,
    ]);
  }
  ?>
</div>


<div class="modal dialog" id="modal-parametro-<?= $model->id ?>">
  <div class="ficha">
    <header>
      <div class="principal">
        <div class="icono"> <span class="mdi mdi-delete-outline"></span> </div>
        <div class="titulo"> <div class="nombre"> Eliminar <?= $model->nombre ?></div> </div>
      </div>
    </header>
    <footer>
      <div class="opciones">
        <div class="opcion">
          <a class="btn" href="<?= Url::to(['/parametro/eliminar',
            'id' => $model->id,
            'from' => Url::current(),
            'to' => Url::current()]) ?>">
            <span class="mdi mdi-delete"></span> Eliminar
          </a>
        </div>
        <div class="opcion">
          <a href="#" class="btn-flat modal-close"> Cancelar </a>
        </div>
      </div>
    </footer>
  </div>
</div>
