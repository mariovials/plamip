<?php

use yii\helpers\Url;

use common\helpers\Inflector;

$nivel = $nivel ?? 0;
?>

<div class="item parametro" style="margin-left: <?= $nivel ?>em ">
  <div class="informacion">
    <div class="icono" style="color: <?= $model->color ?>">
      <span class="mdi mdi-<?= $model->icono ?>"></span>
    </div>
    <div class="texto">
      <div class="primario">
        <div class="campo">
          <?= $model->htmlLink() ?>
        </div>
      </div>
    </div>
  </div>
  <div class="opciones">
    <div class="opcion puntaje_maximo">
      <?= $model->puntaje_maximo + 0 ?>
    </div>
    <div class="opcion grande">
      <a class="btn" href="<?= Url::to(['/parametro/editar',
        'id' => $model->id,
        'from' => Url::current(),
        'to' => Url::current()]) ?>">
        <span class="mdi mdi-pencil"></span> Editar
      </a>
    </div>
    <div class="opcion">
      <a class="btn solo" href="<?= Url::to(['/parametro/eliminar',
        'id' => $model->id,
        'to' => Url::current()]) ?>">
        <span class="mdi mdi-delete"></span>
      </a>
    </div>
  </div>
</div>
