<?php

use common\helpers\Html;
use yii\helpers\Url;

$this->title = $model->nombre;
$this->params['icono'] = 'eye';
$this->params['links'] = [
  ['label' => 'Mostrar', 'url' => ['/mostrar']],
  $this->title,
];
$this->params['cuestionario'] = $model->opcion->pregunta->cuestionario;
$this->params['menu'] = 'configuracion';

$this->params['opciones'][] = Html::a(
    '<span class="mdi mdi-delete"></span> Eliminar',
    ['/mostrar/eliminar', 'id' => $model->id, 'from' => Url::current()],
    ['class' => 'btn-flat', 'data' => ['confirm' => '¿Está seguro?']]);
$this->params['opciones'][] = Html::a(
    '<span class="mdi mdi-pencil"></span> Editar',
    ['/mostrar/editar', 'id' => $model->id, 'from' => Url::current(), 'to' => Url::current()],
    ['class' => 'btn']);

$this->params['lateral'] = $this->render('_detalles', ['model'=> $model]);

?>

<div class="mostrar ver">

  <?= $this->render('_ficha', ['model' => $model]); ?>

</div>
