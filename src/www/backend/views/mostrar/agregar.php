<?php

$this->title = 'Mostrar pregunta';
$this->params['icono'] = 'eye';
$this->params['links'] = [
  ['label' => "Pregunta {$model->opcion->pregunta->numero}", 'url' => $model->opcion->pregunta->url],
  ['label' => $model->opcion->nombre, 'url' => $model->opcion->url],
  $this->title,
];
$this->params['cuestionario'] = $model->opcion->pregunta->cuestionario;
$this->params['menu'] = 'configuracion';
$this->params['atras'] = $model->opcion->url;

?>

<div class="mostrar agregar">

  <?= $this->render('_form', [
    'model' => $model,
    'attributes' => [
      'id',
      'opcion_id',
      'pregunta_id',
    ],
  ]); ?>

</div>
