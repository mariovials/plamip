<?php

$attributes = $attributes ?? array_keys($model->attributes);

$preguntas = [];
foreach ($model->opcion->pregunta->cuestionario
  ->getPreguntas()
  ->andWhere("id <> {$model->opcion->pregunta_id}")
  ->orderBy('orden')
  ->all() as $pregunta) {
  $preguntas[$pregunta->id] = $pregunta->nombre;
}
$opciones = [];
foreach ($model->opcion->mostrar as $mostrar) {
  $opciones[$mostrar->pregunta_id] = [
    'disabled' => true,
  ];
}
?>

<div class="filas">

  <?php if (in_array('pregunta_id', $attributes)): ?>
  <div class="fila">
    <div class="campo pregunta_id grande">
      <i class="mdi mdi-text-box"></i>
      <?= $form->field($model, 'pregunta_id')->dropDownList($preguntas, [
        'options' => $opciones,
        'prompt' => ['text' => '', 'options' => ['style' => ['display' => 'none']]],
      ]); ?>
    </div>
  </div>
  <?php endif; ?>

</div>
