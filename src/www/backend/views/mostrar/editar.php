<?php

$this->title = 'Editar ' . $model->id;
$this->params['icono'] = 'eye';
$this->params['links'] = [
  ['label' => 'Mostrar', 'url' => ['/mostrar']],
  ['label' => $model->id, 'url' => $model->url],
  'Editar',
];
$this->params['cuestionario'] = $model->opcion->pregunta->cuestionario;
$this->params['menu'] = 'configuracion';

?>

<div class="mostrar editar">

  <?= $this->render('_form', [
    'model' => $model,
    'attributes' => [
      'id',
      'opcion_id',
      'pregunta_id',
    ],
  ]); ?>

</div>
