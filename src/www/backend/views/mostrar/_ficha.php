<?php
$opciones = $opciones ?? [];
?>

<div class="ficha mostrar" data-id="<?= $model->id ?>">

  <header>
    <div class="principal">
      <div class="icono"> <span class="mdi mdi-eye"></span> </div>
      <div class="titulo">
        <div class="nombre">
        <?= $model->id; ?>
        <div class="descripcion">
        </div>
      </div>
    </div>
  </header>

  <main>
    <div class="fila">
      <div class="campo">
        <div class="label"> <?= $model->getAttributeLabel('opcion_id') ?> </div>
        <div class="value"> <?= $model->opcion_id ?> </div>
      </div>
    </div>
    <div class="fila">
      <div class="campo">
        <div class="label"> <?= $model->getAttributeLabel('pregunta_id') ?> </div>
        <div class="value"> <?= $model->pregunta_id ?> </div>
      </div>
    </div>
  </main>

</div>
