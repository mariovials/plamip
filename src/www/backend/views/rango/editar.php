<?php

$this->title = 'Editar ' . $model->id;
$this->params['icono'] = 'arrow-expand-horizontal';
$this->params['links'] = [
  ['label' => 'Rango', 'url' => ['/rango']],
  ['label' => $model->id, 'url' => $model->url],
  'Editar',
];
$this->params['cuestionario'] = $model->tipoParametro->cuestionario;
$this->params['menu'] = 'configuracion';

$model->desde = $model->desde + 0;
$model->hasta = $model->hasta + 0;

?>

<div class="rango editar">

  <?= $this->render('_form', [
    'model' => $model,
    'attributes' => [
      'id',
      'tipo_parametro_id',
      'desde',
      'hasta',
    ],
  ]); ?>

</div>
