<?php

$this->title = 'Agregar Rango';
$this->params['icono'] = 'arrow-expand-horizontal';
$this->params['links'] = [
  ['label' => $model->tipoParametro->nombre, 'url' => $model->tipoParametro->url],
  'Agregar rango de resultados',
];

$this->params['cuestionario'] = $model->tipoParametro->cuestionario;
$this->params['menu'] = 'configuracion';

$model->desde = $model->desde ?: $model->tipoParametro->getRangos()->max('hasta') + 0;
$model->hasta = $model->hasta ?: $model->desde;

?>

<div class="rango agregar">

  <?= $this->render('_form', [
    'model' => $model,
    'attributes' => [
      'id',
      'tipo_parametro_id',
      'desde',
      'hasta',
    ],
  ]); ?>

</div>
