<?php

use common\helpers\Html;
use yii\helpers\Url;

$this->title = $model->nombre;
$this->params['icono'] = 'arrow-expand-horizontal';
$this->params['links'] = [
  ['label' => 'Rango', 'url' => ['/rango']],
  $this->title,
];

$this->params['opciones'][] = Html::a(
    '<span class="mdi mdi-delete"></span> Eliminar',
    ['/rango/eliminar', 'id' => $model->id, 'from' => Url::current()],
    ['class' => 'btn-flat', 'data' => ['confirm' => '¿Está seguro?']]);
$this->params['opciones'][] = Html::a(
    '<span class="mdi mdi-pencil"></span> Editar',
    ['/rango/editar', 'id' => $model->id, 'from' => Url::current(), 'to' => Url::current()],
    ['class' => 'btn']);

$this->params['lateral'] = $this->render('_detalles', ['model'=> $model]);

?>

<div class="rango ver">

  <?= $this->render('_ficha', ['model' => $model]); ?>

</div>
