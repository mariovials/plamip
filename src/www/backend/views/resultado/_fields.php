<?php

$attributes = $attributes ?? array_keys($model->attributes);

?>

<div class="filas">

  <div class="fila">
    <div class="campo nombre grande">
      <i class="mdi mdi-text-box"></i>
      <?= $form->field($model, 'nombre'); ?>
    </div>
  </div>

  <div class="fila completa">
    <div class="campo desde">
      <i class="mdi mdi-arrow-expand-horizontal"></i>
      <?= $form->field($model, 'desde')->number(['step' => 0.1,
        'readonly' => !$model->cuestionario->getResultados()->exists(),
      ]); ?>
    </div>
    <div class="campo hasta">
      <?= $form->field($model, 'hasta')->number(['step' => 0.1]); ?>
    </div>
  </div>

  <div class="fila">
    <div class="campo texto grande">
      <i class="mdi mdi-text-box"></i>
      <?= $form->field($model, 'texto')->quillEditor(); ?>
    </div>
  </div>

  <div class="fila">
    <div class="campo texto grande">
      <i class="mdi mdi-text-box"></i>
      <?= $form->field($model, 'texto_promedio')->quillEditor(); ?>
    </div>
  </div>

</div>
