<?php

$opciones = $opciones ?? [];

?>

<div class="ficha rango" data-id="<?= $model->id ?>">

  <header>
    <div class="principal">
      <div class="icono"> <span class="mdi mdi-arrow-expand-horizontal"></span> </div>
      <div class="titulo">
        <div class="nombre">
        <?= $model->id; ?>
        <div class="descripcion">
        </div>
      </div>
    </div>
  </header>

  <main>
    <div class="fila">
      <div class="campo">
        <div class="label"> <?= $model->getAttributeLabel('cuestionario_id') ?> </div>
        <div class="value"> <?= $model->cuestionario_id ?> </div>
      </div>
    </div>
    <div class="fila">
      <div class="campo">
        <div class="label"> <?= $model->getAttributeLabel('desde') ?> </div>
        <div class="value"> <?= $model->desde ?> </div>
      </div>
    </div>
    <div class="fila">
      <div class="campo">
        <div class="label"> <?= $model->getAttributeLabel('hasta') ?> </div>
        <div class="value"> <?= $model->hasta ?> </div>
      </div>
    </div>
  </main>

</div>
