<?php

$this->title = 'Agregar Rango de resultado general';
$this->params['icono'] = 'arrow-expand-horizontal';

$this->params['cuestionario'] = $model->cuestionario;
$this->params['menu'] = 'configuracion';

$model->desde = $model->desde ?: $model->cuestionario->getResultados()->max('hasta') + 0;
$model->hasta = $model->hasta ?: $model->desde;

?>

<div class="rango agregar">

  <?= $this->render('_form', [
    'model' => $model,
    'attributes' => [
      'id',
      'tipo_parametro_id',
      'desde',
      'hasta',
    ],
  ]); ?>

</div>
