<?php
$attributes = $attributes ?? array_keys($model->attributes);
?>

<div class="filas">

  <?php if (in_array('nombre', $attributes)): ?>
  <div class="fila">
    <div class="campo nombre grande">
      <i class="mdi mdi-text-box"></i>
      <?= $form->field($model, 'nombre'); ?>
    </div>
  </div>
  <?php endif; ?>

  <?php if (in_array('descripcion', $attributes)): ?>
  <div class="fila">
    <div class="campo descripcion grande">
      <i class="mdi mdi-text-box"></i>
      <?= $form->field($model, 'descripcion')->quillEditor(); ?>
    </div>
  </div>
  <?php endif; ?>

</div>
