<?php

$this->title = 'Editar ' . $model->nombre;

$this->params['cuestionario'] = $model->cuestionario;
$this->params['menu'] = 'preguntas';

$this->params['icono'] = 'view-agenda';
$this->params['links'] = [
  ['label' => $model->nombre, 'url' => $model->url],
  'Editar',
];

?>

<div class="seccion editar">

  <?= $this->render('_form', [
    'model' => $model,
    'attributes' => [
      'id',
      'cuestionario_id',
      'orden',
      'nombre',
      'descripcion',
      'fecha_creacion',
      'fecha_edicion',
    ],
  ]); ?>

</div>
