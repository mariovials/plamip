<?php

$this->title = 'Agregar Seccion';

$this->params['cuestionario'] = $model->cuestionario;
$this->params['menu'] = 'preguntas';

$this->params['icono'] = 'view-agenda';
$this->params['links'] = [
  ['label' => $model->cuestionario->nombre, 'url' => $model->cuestionario->url],
  'Agregar sección',
];

?>

<div class="seccion agregar">

  <?= $this->render('_form', [
    'model' => $model,
    'attributes' => [
      'id',
      'cuestionario_id',
      'orden',
      'nombre',
      'descripcion',
      'fecha_creacion',
      'fecha_edicion',
    ],
  ]); ?>

</div>
