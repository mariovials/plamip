
<div class="ficha seccion" data-id="<?= $model->id ?>">

  <main>
    <div class="fila">
      <div class="campo">
        <div class="label">
          <?= $model->getAttributeLabel('fecha_creacion') ?>
        </div>
        <div class="value">
          <?= Yii::$app->formatter->asDatetime($model->fecha_creacion) ?>
        </div>
      </div>
    </div>
    <div class="fila">
      <div class="campo">
        <div class="label">
          <?= $model->getAttributeLabel('fecha_edicion') ?>
        </div>
        <div class="value">
          <?= Yii::$app->formatter->asDatetime($model->fecha_edicion) ?>
        </div>
      </div>
    </div>
  </main>

</div>
