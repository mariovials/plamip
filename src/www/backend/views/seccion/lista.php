<?php
use common\models\Seccion;
use backend\widgets\ListView;
use common\helpers\Html;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

$this->title = 'Seccions';
$this->params['icono'] = 'view-agenda';
$this->params['links'] = [ 'Seccions'];
$this->params['opciones'][] = Html::a(
  '<span class="mdi mdi-plus"></span> Agregar',
  ['/seccion/agregar', 'from' => Url::current(), 'to' => Url::current()],
  [ 'class' => 'btn'],
);
?>

<div class="seccion indice">

  <div class="ficha lista">
    <main>
      <?= ListView::widget([
        'dataProvider' => new ActiveDataProvider([
          'query' => Seccion::find(),
          'sort' => ['attributes' => ['nombre']],
          'pagination' => ['pageSize' => 10],
        ]),
        'itemView' => '_lista',
      ]); ?>
    </main>
  </div>

</div>
