<?php
$opciones = $opciones ?? [];
?>

<div class="ficha seccion" data-id="<?= $model->id ?>">

  <header>
    <div class="principal">
      <div class="icono"> <span class="mdi mdi-view-agenda"></span> </div>
      <div class="titulo">
        <div class="nombre">
        <?= $model->nombre; ?>
        <div class="descripcion">
        </div>
      </div>
    </div>
  </header>

  <main>
    <div class="fila">
      <div class="campo">
        <div class="label"> <?= $model->getAttributeLabel('cuestionario_id') ?> </div>
        <div class="value"> <?= $model->cuestionario_id ?> </div>
      </div>
    </div>
    <div class="fila">
      <div class="campo">
        <div class="label"> <?= $model->getAttributeLabel('orden') ?> </div>
        <div class="value"> <?= $model->orden ?> </div>
      </div>
    </div>
    <div class="fila">
      <div class="campo">
        <div class="label"> <?= $model->getAttributeLabel('nombre') ?> </div>
        <div class="value"> <?= $model->nombre ?> </div>
      </div>
    </div>
    <div class="fila">
      <div class="campo">
        <div class="label"> <?= $model->getAttributeLabel('descripcion') ?> </div>
        <div class="value"> <?= $model->descripcion ?> </div>
      </div>
    </div>
  </main>

</div>
