<?php

use common\models\Cuestionario;
use common\models\Usuario;
use yii\helpers\Url;

$this->title = 'Admin';
$this->params['icono'] = 'view-dashboard';
$this->params['links'] = [ 'Panel de administración'];
?>

<div class="site index">

  <div class="contadores">

    <a class="contador" href="<?= Url::to(['/cuestionario']) ?>">
      <div class="icono"> <span class="mdi mdi-list-box"></span> </div>
      <div class="texto">
        <div class="numero"> <?= Cuestionario::find()->count() ?> </div>
        <div class="texto"> Cuestionarios </div>
      </div>
    </a>

  </div>

</div>

<style>
  body {
    overflow-y: scroll;
  }
  .contadores {
    display: flex;
    flex-wrap: wrap;
    padding: 1rem 1.5rem;
  }
  .contador {
    color: #111;
    display: flex;
    width: 16em;
    height: 8em;
    border-radius: 0.6em;
    justify-content: center;
    align-items: center;
    margin: 0 1em 1em 0;
    background: #f3f5f7;
  }
  .contador .icono {
    font-size: 3.5em;
    margin-right: 1.5rem;
  }
  .contador .texto .numero {
    font-size: 1.6em;
    font-weight: 800;
  }
  .contador .texto .texto {
    font-weight: 700;
    letter-spacing: -0.03em;
  }
</style>
