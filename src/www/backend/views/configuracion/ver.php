<?php
$this->title = $model->nombre;

$this->params['links'] = [
  ['label' => 'Configuración', 'url' => ['/configuracion']],
  ['label' => $model->nombre, 'url' => $model->url],
];
?>

<div class="configuracion ver">

  <?= $this->render('_ficha', [
    'model' => $model,
    'attributes' => [
      'valor',
    ],
    'opciones' => [
      'editar',
      // 'eliminar',
    ],
  ]); ?>

</div>
