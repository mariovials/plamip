<?php
$this->title = 'Agregar Configuración';
$this->params['links'] = [
  ['label' => 'Configuración', 'url' => ['/configuracion']],
  'Agregar',
];
?>

<div class="configuracion agregar">

  <?= $this->render('_form', ['model' => $model]); ?>

</div>
