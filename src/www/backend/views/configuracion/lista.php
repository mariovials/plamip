<?php
use common\models\Configuracion;
use backend\widgets\ListView;
use yii\data\ActiveDataProvider;
use common\helpers\Html;
use yii\helpers\Url;

$this->title = 'Configuración';
$this->params['icono'] = 'cog';
$this->params['links'] = [$this->title];
if (YII_ENV == 'dev') {
  $this->params['opciones'][] = Html::a(
    '<span class="mdi mdi-plus"></span> Agregar',
    ['/configuracion/agregar', 'from' => Url::current(), 'to' => Url::current()],
    ['class' => 'btn'],
  );
}
$configuraciones = [
  [
    'nombre' => 'Parámetros',
    'icono' => 'cogs',
    'attributes' => ['nombre', 'valor'],
    'codigos' => [
    ],
  ],
];

$this->title = 'Configuraciones';
?>

<div class="configuracion indice">

  <?php foreach ($configuraciones as $configuracion): ?>
  <div class="ficha lista">
    <header>
      <div class="principal">
        <div class="icono"> <i class="mdi mdi-<?= $configuracion['icono'] ?>"></i> </div>
        <div class="titulo">
          <div class="nombre"> <?= $configuracion['nombre'] ?> </div>
        </div>
      </div>
    </header>
    <main>
      <?= ListView::widget([
        'layout' => '{items}',
        'dataProvider' => new ActiveDataProvider([
          'query' => Configuracion::find()
            ->where(['codigo' => $configuracion['codigos']]),
          'sort' => [
            'attributes' => [
              'nombre',
            ],
            'defaultOrder' => [
              'nombre' => SORT_ASC,
            ],
          ],
          'pagination' => false,
        ]),
        'itemView' => '_lista',
        'viewParams' => [
          'attributes' => $configuracion['attributes'],
          'opciones' => [
            'valor',
            'editar',
          ],
        ],
      ]); ?>
    </main>
  </div>
  <?php endforeach ?>

</div>
