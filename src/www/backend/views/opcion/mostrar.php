<?php

use common\helpers\Html;
use yii\helpers\Url;
use common\widgets\ActiveForm;

$preguntas = [];
foreach ($model->pregunta->cuestionario
  ->getPreguntas()
  ->andWhere("id <> {$model->pregunta_id}")
  ->andWhere(['>', 'orden', $model->pregunta->orden])
  ->orderBy('orden')
  ->all() as $pregunta) {
  if (preg_match("/\d+\.\d+/", $pregunta->nombre)) {
    continue;
  }
  $preguntas[$pregunta->id] = $pregunta->nombre;
}

$model->preguntas = $model
  ->getMostrar()
  ->select('pregunta_id')
  ->column();

$this->title = $model->nombre;

$this->params['cuestionario'] = $model->pregunta->cuestionario;
$this->params['menu'] = 'preguntas';
$this->params['icono'] = $model->pregunta->getIcono(false);
$this->params['links'] = [
  ['label' => "Pregunta {$model->pregunta->numero}", 'url' => $model->pregunta->url],
  ['label' => $model->nombre, 'url' => $model->url],
  'Cambiar preguntas para mostrar'
];

?>

<div class="form opcion mostrar">

  <?php $form = ActiveForm::begin(); ?>

  <div class="filas">

    <div class="fila">
      <div class="campo pregunta_id grande">
        <i class="mdi mdi-text-box"></i>
        <?= $form->field($model, 'preguntas')->checkboxList($preguntas, [
          'item' => function($index, $label, $name, $checked, $value) {
            $checked = $checked ? 'checked' : '';
            return '
            <label>
              <input ' . $checked . '
                type="checkbox"
                id="opcion-preguntas"
                class="with-gap"
                name="' . $name . '"
                value="' . $value . '"
                aria-required="true">
                  <div class="box"></div>
                  <div class="label">' . $label . '</div>
              </label>
            ';
          },
        ]); ?>
      </div>
    </div>

  </div>

  <div class="opciones">
    <div class="opcion">
      <button class="btn">
        <span class="mdi mdi-<?= $model->isNewRecord ? 'plus-thick' : 'pencil' ?>"></span>
        <?= $model->isNewRecord ? 'Agregar' : 'Guardar' ?>
      </button>
    </div>
    <div class="opcion">
      <a href="<?= Yii::$app->request->get('from', Url::to(['/opcion/ver', 'id' => $model->id])) ?>" class="btn-flat solo">
        Cancelar
      </a>
    </div>
  </div>

  <?php ActiveForm::end(); ?>


</div>


<style type="text/css">
  #selector {
    display: flex;
  }
  #selector .btn {
    margin: 0 1em 0.5em 0;
  }
</style>
<?php

$this->registerJs(<<<JS

  selector = $("<div id='selector'>")

  btnTodas = $("<button id='btn-todas' class='btn bajo'>")
  btnTodas.html('Todas')
  btnTodas.on('click', function(e) {
    e.preventDefault()
    $('#opcion-preguntas input').prop('checked', true)
  })

  btnNinguna = $("<button id='btn-ninguna' class='btn bajo'>")
  btnNinguna.html('Ninguna')
  btnNinguna.on('click', function(e) {
    e.preventDefault()
    $('#opcion-preguntas input').prop('checked', false)
  })

  selector
    .append(btnTodas)
    .append(btnNinguna)
  $('#opcion-preguntas').before(selector)
JS
);
?>
