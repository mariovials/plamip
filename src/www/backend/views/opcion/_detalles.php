<?php

use common\helpers\Html;

?>

<div class="ficha opcion" data-id="<?= $model->id ?>">

  <main>
    <div class="fila">
      <div class="campo">
        <div class="label">
          <?= $model->getAttributeLabel('fecha_creacion') ?>
        </div>
        <div class="value">
          <?= $model->fecha_creacion ?>
        </div>
      </div>
    </div>
    <div class="fila">
      <div class="campo">
        <div class="label">
          <?= $model->getAttributeLabel('fecha_edicion') ?>
        </div>
        <div class="value">
          <?= $model->fecha_edicion ?>
        </div>
      </div>
    </div>
  </main>

</div>

<div class="divider"></div>

<ul class="navegador">
  <?php foreach ($model->pregunta->opciones as $opcion): ?>
  <li> <?= Html::a($opcion->nombre, $opcion->url,
    ['class' => [
      Yii::$app->request->get('id') == $opcion->id ? 'activo' : null,
    ]]) ?> </li>
  <?php endforeach; ?>
</ul>
