<?php

$this->title = 'Editar ' . $model->nombre;

$this->params['cuestionario'] = $model->pregunta->cuestionario;
$this->params['menu'] = 'preguntas';

$this->params['icono'] = $model->pregunta->getIcono(false);
$this->params['links'] = [
  ['label' => "Pregunta {$model->pregunta->numero}", 'url' => $model->pregunta->url],
  ['label' => $model->nombre, 'url' => $model->url],
  'Editar',
];
?>

<div class="opcion editar">

  <?= $this->render('_form', [
    'model' => $model,
    'attributes' => [
      'nombre',
      'descripcion',
      'editable',
      'puntaje',
    ],
  ]); ?>

</div>
