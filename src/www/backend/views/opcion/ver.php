<?php

use common\helpers\Html;
use yii\helpers\Url;

$this->title = $model->nombre;

$this->params['cuestionario'] = $model->pregunta->cuestionario;
$this->params['menu'] = 'preguntas';
$this->params['icono'] = $model->pregunta->getIcono(false);
$this->params['links'] = [
  ['label' => "Pregunta {$model->pregunta->numero}", 'url' => $model->pregunta->url],
  $model->nombre
];
$this->params['lateral'] = $this->render('_detalles', ['model'=> $model]);

$this->params['opciones'][] = Html::a(
  '<span class="mdi mdi-delete"></span> Eliminar',
  ['/opcion/eliminar', 'id' => $model->id, 'to' => Url::to($model->pregunta->url)],
  ['class' => 'btn-flat', 'data' => ['confirm' => '¿Está seguro?']]);
$this->params['opciones'][] = Html::a(
  '<span class="mdi mdi-pencil"></span> Editar',
  ['/opcion/editar', 'id' => $model->id, 'from' => Url::current(), 'to' => Url::current()],
  ['class' => 'btn']);

$this->params['atras'] = $model->pregunta->url;

?>

<div class="opcion ver">

  <?= $this->render('_ficha', [
    'model' => $model,
    'opciones' => [
      'editar',
    ],
  ]); ?>

</div>
