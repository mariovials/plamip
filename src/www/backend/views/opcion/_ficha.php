<?php

use yii\helpers\Url;

$opciones = $opciones ?? [];

?>

<div class="ficha opcion" data-id="<?= $model->id ?>">

  <?= $this->render('_header', ['model' => $model, 'link' => false, 'opciones' => $opciones]) ?>

  <main>
    <div class="fila">
      <div class="campo">
        <div class="label"> <?= $model->getAttributeLabel('editable') ?> </div>
        <div class="value"> <?= $model->editable ? 'Si' : 'No' ?> </div>
      </div>
      <div class="campo <?= !$model->puntaje ? 'error' : '' ?>">
        <div class="label"> <?= $model->getAttributeLabel('puntaje') ?> </div>
        <div class="value bold"> <?= $model->puntaje + 0 ?> </div>
      </div>
    </div>
    <div class="fila">
      <div class="campo grande">
        <div class="label">
          Mostrar pregunta si selecciona esta opción
          <a href="<?= Url::to(['opcion/mostrar', 'id' => $model->id]) ?>" class="btn bajo">
            <span class="mdi mdi-checkbox-multiple-outline"></span>
            Cambiar preguntas para mostrar
          </a>
        </div>
        <div class="value">

          <ul id="mostrar">
            <?php foreach ($model->mostrar as $mostrar): ?>
              <li>
                <div class="informacion">
                  <?= $mostrar->pregunta->icono ?>
                  <?= $mostrar->pregunta->htmlLink() ?>
                </div>
              </li>
            <?php endforeach ?>
          </ul>
        </div>
      </div>
    </div>
  </main>

</div>

<style>
#mostrar li {
  display: flex;
  justify-content: space-between;
  margin-bottom: 0.2em;
}
</style>
