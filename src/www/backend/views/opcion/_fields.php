<?php

$attributes = $attributes ?? array_keys($model->attributes);

?>

<div class="filas">

  <?php if (in_array('nombre', $attributes)): ?>
  <div class="fila">
    <div class="campo nombre grande">
      <i class="mdi mdi-text-box"></i>
      <?= $form->field($model, 'nombre'); ?>
    </div>
  </div>
  <?php endif; ?>

  <?php if (in_array('descripcion', $attributes)): ?>
  <div class="fila">
    <div class="campo descripcion grande">
      <i class="mdi mdi-text-short"></i>
      <?= $form->field($model, 'descripcion'); ?>
    </div>
  </div>
  <?php endif; ?>

  <?php if (in_array('editable', $attributes) && !$model->pregunta->numerico): ?>
  <div class="fila">
    <div class="campo editable">
      <i class="mdi mdi-pencil"></i>
      <?= $form->field($model, 'editable')->checkbox(); ?>
    </div>
  </div>
  <?php endif; ?>

  <?php if (in_array('puntaje', $attributes)): ?>
    <?php $model->puntaje += 0  ?>
  <div class="fila">
    <div class="campo puntaje">
      <i class="mdi mdi-counter"></i>
      <?= $form->field($model, 'puntaje')->number(['step' => 0.01]); ?>
    </div>
  </div>
  <?php endif; ?>

</div>
