<?php
use common\models\Opcion;
use backend\widgets\ListView;
use yii\data\ActiveDataProvider;

$this->title = 'Opciones';
$this->params['icono'] = 'checkbox-multiple-outline';
$this->params['links'] = [ 'Opciones'];
?>

<div class="opcion indice">

  <div class="ficha lista">
    <main>
      <?= ListView::widget([
        'dataProvider' => new ActiveDataProvider([
          'query' => Opcion::find(),
          'sort' => ['attributes' => ['nombre']],
          'pagination' => ['pageSize' => 100],
        ]),
        'itemView' => '_lista',
      ]); ?>
    </main>
  </div>

</div>
