<?php

use common\helpers\Html;
use yii\helpers\Url;

?>

<div class="item" data-id="<?= $model->id ?>">
  <div class="informacion">
    <div class="texto">
      <div class="primario">
        <div class="campo">
          <?= $model->htmlLink() ?>
        </div>
      </div>
    </div>
  </div>
  <div class="opciones">

    <?php if ($model->editable): ?>
    <div class="opcion editable">
      <div class="chip"> <span class="mdi mdi-pencil"></span> Editable </div>
    </div>
    <?php endif; ?>

    <div class="opcion puntaje">
      Puntaje: <b><?= floatval($model->puntaje) ?></b>
    </div>

    <div class="opcion grande">
      <?= Html::a(
        '<span class="mdi mdi-pencil"></span> Editar',
        ['/opcion/editar', 'id' => $model->id,
          'from' => Url::current(),
          'to' => Url::current()],
        ['class' => 'btn']) ?>
    </div>
    <div class="opcion grande">
      <?= Html::a(
        '<span class="mdi mdi-delete"></span> Eliminar',
        ['/opcion/eliminar', 'id' => $model->id, 'from' => Url::current()],
        ['class' => 'btn', 'data' => ['confirm' => '¿Está seguro?']]) ?>
    </div>
  </div>
</div>
