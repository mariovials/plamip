<?php

$this->title = 'Agregar Opción';

$this->params['cuestionario'] = $model->pregunta->cuestionario;
$this->params['menu'] = 'preguntas';

$this->params['icono'] = $model->pregunta->getIcono(false);
$this->params['links'] = [
  ['label' => "Pregunta {$model->pregunta->numero}", 'url' => $model->pregunta->url],
  'Agregar opción',
];

?>

<div class="opcion agregar">

  <?= $this->render('_form', [
    'model' => $model,
    'attributes' => [
      'nombre',
      'descripcion',
      'editable',
      'puntaje',
    ],
  ]); ?>

</div>
