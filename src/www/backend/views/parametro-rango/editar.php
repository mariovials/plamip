<?php

$nombreRango = $model->parametro->puntaje_maximo > 0
  ? $model->rango->nombreRango : 'Resultados para todos (Parámetro sin puntaje)';

$this->title = 'Editar ' . $model->id;
$this->params['icono'] = 'clipboard-check';
$this->params['links'] = [
  ['label' => $model->parametro->nombre, 'url' => $model->parametro->url],
  $nombreRango,
];

$this->params['cuestionario'] = $model->rango->tipoParametro->cuestionario;
$this->params['menu'] = 'cuestionario';

?>

<div class="parametro-rango editar">

  <?= $this->render('_form', [
    'model' => $model,
    'attributes' => [
      'id',
      'parametro_id',
      'rango_id',
      'texto',
    ],
  ]); ?>

</div>
