<?php
$opciones = $opciones ?? [];
?>

<div class="ficha parametro-rango" data-id="<?= $model->id ?>">

  <header>
    <div class="principal">
      <div class="icono"> <span class="mdi mdi-clipboard-check"></span> </div>
      <div class="titulo">
        <div class="nombre">
        <?= $model->id; ?>
        <div class="descripcion">
        </div>
      </div>
    </div>
  </header>

  <main>
    <div class="fila">
      <div class="campo">
        <div class="label"> <?= $model->getAttributeLabel('parametro_id') ?> </div>
        <div class="value"> <?= $model->parametro_id ?> </div>
      </div>
    </div>
    <div class="fila">
      <div class="campo">
        <div class="label"> <?= $model->getAttributeLabel('rango_id') ?> </div>
        <div class="value"> <?= $model->rango_id ?> </div>
      </div>
    </div>
    <div class="fila">
      <div class="campo">
        <div class="label"> <?= $model->getAttributeLabel('texto') ?> </div>
        <div class="value"> <?= $model->texto ?> </div>
      </div>
    </div>
  </main>

</div>
