<?php

$this->title = 'Agregar Parametro Rango';
$this->params['icono'] = 'clipboard-check';
$this->params['links'] = [
  ['label' => 'Parametro Rango', 'url' => ['/parametro-rango']],
  'Agregar',
];

?>

<div class="parametro-rango agregar">

  <?= $this->render('_form', [
    'model' => $model,
    'attributes' => [
      'id',
      'parametro_id',
      'rango_id',
      'texto',
    ],
  ]); ?>

</div>
