<?php
use common\models\ParametroRango;
use backend\widgets\ListView;
use common\helpers\Html;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

$this->title = 'Parametro Rangos';
$this->params['icono'] = 'clipboard-check';
$this->params['links'] = [ 'Parametro Rangos'];
$this->params['opciones'][] = Html::a(
  '<span class="mdi mdi-plus"></span> Agregar',
  ['/parametro-rango/agregar', 'from' => Url::current(), 'to' => Url::current()],
  [ 'class' => 'btn'],
);
?>

<div class="parametro-rango indice">

  <div class="ficha lista">
    <main>
      <?= ListView::widget([
        'dataProvider' => new ActiveDataProvider([
          'query' => ParametroRango::find(),
          'sort' => ['attributes' => ['id']],
          'pagination' => ['pageSize' => 10],
        ]),
        'itemView' => '_lista',
      ]); ?>
    </main>
  </div>

</div>
