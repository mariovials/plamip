<?php

use common\helpers\Html;
use yii\helpers\Url;

$this->params['icono'] = 'information';
$this->params['links'] = ['Información'];
$this->params['opciones'][] = Html::a(
  '<span class="mdi mdi-pencil"></span> Editar',
  ['/cuestionario/editar',
    'id' => $model->id,
    'tab' => 'informacion',
    'from' => Url::current(),
    'to' => Url::current()],
  ['class' => 'btn-flat']);

?>

<div class="cuestionario ver">

  <div class="ficha cuestionario" data-id="<?= $model->id ?>">
    <main>
      <br>
      <div class="fila">
        <div class="campo grande lista">
          <div class="label"> <?= $model->getAttributeLabel('nombre') ?> </div>
          <div class="value"> <?= $model->nombre ?> </div>
        </div>
      </div>

      <div class="fila">
        <div class="campo grande lista">
          <div class="label"> <?= $model->getAttributeLabel('descripcion_larga') ?> </div>
          <div class="value"> <?= nl2br($model->descripcion_larga) ?> </div>
        </div>
      </div>

    </main>
  </div>

</div>
