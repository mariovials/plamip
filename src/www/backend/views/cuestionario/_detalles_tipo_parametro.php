<?php

use yii\helpers\Url;

$tiposParametro = [];

if ($this->context->id == 'parametro') {
  $activo = $parametro->tipo_parametro_id == $model->id;
} else {
  $activo = $this->context->id == $model->controllerName
    && Yii::$app->request->get('id') == $model->id;
}
?>

<li>
  <a href="<?= Url::to($model->url) ?>"
    class="<?= $activo ? 'activo' : '' ?>">
    <div class="icono">
      <span class="mdi mdi-<?= $model->icono ?>"></span>
    </div>
    <div class="nombre">
      <?= $model->nombre ?>
    </div>
  </a>
</li>


<?php foreach ($model->tiposParametro as $tipoParametro): ?>
<ul class="navegador parametro">
  <?= $this->render('_detalles_tipo_parametro', [
    'model' => $tipoParametro,
    'parametro' => $parametro,
  ]) ?>
</ul>
<?php endforeach ?>
