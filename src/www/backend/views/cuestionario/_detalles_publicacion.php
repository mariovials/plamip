<?php



?>

<div class="ficha cuestionario" data-id="<?= $model->id ?>">
  <main>
    <div class="fila">
      <div class="campo grande">
        <div class="label">
          <span class="mdi mdi-chain"></span> Enlace para responder
        </div>
        <div class="value">
          <a id="link-responder-cuestionario"
            target="_blank"
            href="<?= Yii::$app->urlManagerFrontend
            ->createAbsoluteUrl(['/cuestionario/responder', 'id' => $model->id]) ?>">
              <?= Yii::$app->urlManagerFrontend
            ->createAbsoluteUrl(['/cuestionario/responder', 'id' => $model->id]) ?>
            </a>
        </div>
          <button id="btn-copiar-link" class="btn bajo">
            <span class="mdi mdi-content-copy"></span> Copiar
          </button>
      </div>
    </div>
  </main>
</div>

<script>
  document.getElementById('btn-copiar-link').addEventListener('click', function() {
    var link = document.getElementById("link-responder-cuestionario")
    console.log(navigator.clipboard)
    text = link.innerHTML.trim();
    navigator.clipboard.writeText(text);
    alert("Copiado: " + text);
  })
</script>

<style>
  #link-responder-cuestionario {
    word-break: break-word;
  }
  #lateral #btn-copiar-link {
    display: flex;
    width: auto;
    margin-top: 0.4em;
    padding: 0 1em;
  }
</style>
