<?php

use common\helpers\Html;
use yii\helpers\Url;

$this->params['icono'] = 'chart-box';
$this->params['links'] = ['Resultados generales'];

$mensajes = [];
$desde = 0;
$hasta = 0;
foreach ($model->resultados as $resultado) {
  if ($resultado->desde != $hasta) {
    $mensajes[] = "Rango $resultado->nombreRango debe comenzar en $hasta";
  }
  $desde = $resultado->desde + 0;
  $hasta = $resultado->hasta + 0;
}
if ($hasta != 100) {
  $mensajes[] = "Rango no termina en 100.";
}

?>

<div class="cuestionario ver">

  <div class="ficha cuestionario" data-id="<?= $model->id ?>">
    <main>
      <div class="fila">
        <div class="campo grande">
          <div class="label"> Resultados generales </div>
          <div class="value">
            <div class="lista-parametros">
              <ul class="resultados">
                <?php foreach($model->resultados as $resultado): ?>
                <li>
                  <div class="informacion">
                    <div class="rango">
                      <?= $resultado->desde + 0 ?>% a <?= $resultado->hasta + 0 ?>%
                    </div>
                    <div class="nombre">
                      <?= $resultado->nombre ?> <br>
                    </div>
                    <div class="texto">
                      <div class="header">
                        <?= $resultado->getAttributeLabel('texto') ?>
                      </div>
                      <div class="value">
                        <?php if ($resultado->texto): ?>
                          <?= nl2br($resultado->texto) ?>
                        <?php else: ?>
                        <span style="color: red">
                          Texto no definido para este rango
                        </span>
                        <?php endif ?>
                      </div>
                    </div>

                    <div class="texto promedio">
                      <div class="header">
                        <?= $resultado->getAttributeLabel('texto_promedio') ?>
                      </div>
                      <div class="value">
                        <?php if ($resultado->texto_promedio): ?>
                          <?= nl2br($resultado->texto_promedio) ?>
                        <?php else: ?>
                        <span style="color: red">
                          Texto no definido para este rango
                        </span>
                        <?php endif ?>
                      </div>
                    </div>

                  </div>
                  <div class="opciones">
                    <?= Html::a('<span class="mdi mdi-pencil"></span>',
                      ['/resultado/editar',
                        'id' => $resultado->id,
                        'to' => Url::current(),
                        'from' => Url::current(),
                      ],
                      ['class' => 'btn bajo solo']) ?>
                    <?= Html::a('<span class="mdi mdi-delete"></span>',
                      ['/resultado/eliminar',
                        'id' => $resultado->id,
                        'to' => Url::current(),
                        'from' => Url::current(),
                      ],
                      ['class' => 'btn bajo solo',
                        'data' => ['confirm' => '¿Está seguro?']]) ?>
                  </div>
                </li>
                <?php endforeach ?>
                <?php if (!$model->resultados || $mensajes): ?>
                <li>
                  <?= Html::a('<span class="mdi mdi-plus"></span> Agregar rango de resultados',
                    ['/resultado/agregar',
                      'cuestionario_id' => $model->id,
                      'to' => Url::current(),
                      'from' => Url::current(),
                    ],
                    ['class' => 'btn']) ?>
                </li>
                <?php endif ?>
              </ul>

              <?php if ($mensajes): ?>
              <div id="errores-rango">
                <b>Problemas en rangos</b>: <br>
                <ul>
                  <?php foreach ($mensajes as $mensaje): ?>
                  <li> <?= $mensaje ?> </li>
                  <?php endforeach ?>
                </ul>
              </div>
              <?php endif ?>

            </div>
          </div>
        </div>
      </div>
    </main>
  </div>

</div>

<style>
  #errores-rango {
    background: #FFEBEE;
    padding: 0.6em 1em;
    color: #D32F2F;
    border: 1px solid #EF9A9A;
    border-radius: 0.3em;
    margin-top: 1em;
    font-size: 0.95em;
    width: fit-content;
  }
  ul.resultados {
  }
  ul.resultados li {
    width: 100%;
    margin-bottom: 1em;
    display: flex;
    justify-content: space-between;
  }
  ul.resultados li .texto .header {
    background: #CCC;
    padding: 0.5em 1em 0.5em 1em;
  }
  ul.resultados li .texto .value {
    padding: 0.5em 1em;
  }
  ul.resultados li .texto {
    margin-top: 0.5em;
    border: 1px solid #CCC;
    background: #F5F5F5;
    border-radius: 1em;
    overflow: hidden;
  }
  ul.resultados li .opciones a {
    margin-left: 0.2em;
  }
  ul.resultados li .opciones {
    display: flex;
  }
  ul.resultados li .rango {
    font-weight: bold;
    font-size: 0.9em;
    margin-bottom: 0.1em;
    display: flex;
    align-items: center;
  }
  ul.resultados li .rango a {
    margin-left: 0.5em;
  }
  ul.resultados li .nombre {
    font-weight: bold;
  }
  ul.resultados li .texto {
  }
</style>
