<?php

?>

<div class="titulo">
  <span class="mdi mdi-chart-box"></span>
  <div class="ellipsis-two-lines"> Parámetros </div>
</div>
<ul class="navegador parametro">
  <?php foreach ($model->tiposParametro as $tiposParametro): ?>
    <?php foreach ($tiposParametro->parametros as $i => $parametro): ?>
      <?= $this->render('_detalles_parametro', [
        'model' => $parametro,
        'header' => $i == 0,
      ]) ?>
    <?php endforeach ?>
  <?php endforeach ?>
</ul>
<div class="divider"></div>
