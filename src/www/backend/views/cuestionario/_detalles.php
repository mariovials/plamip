<?php

use common\helpers\Html;

if (Yii::$app->request->get('PreguntaSearch')) {
  echo Html::a(
    '<span class="mdi mdi-rotate-left"></span> Restablecer',
    ['/cuestionario/preguntas', 'id' => $model->id],
    ['class' => 'btn', 'id' => 'btn-restablecer']);
  echo '<br>';
}
?>

<div class="ficha cuestionario" data-id="<?= $model->id ?>">

  <main>
    <div id="conteo" class="fila">
      <?php foreach ($model->tiposParametro as $tipoParametro): ?>
      <div class="campo grande">
        <div class="value">
          <ul>
            <?= $this->render('_conteo', [
              'cuestionario' => $model,
              'tipoParametro' => $tipoParametro,
              'parametro' => null,
              'parametro_id' => $parametro_id,
            ]) ?>
          </ul>
        </div>
      </div>
      <?php endforeach; ?>
    </div>
  </main>

</div>
