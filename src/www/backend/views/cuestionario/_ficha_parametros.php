<?php

use common\helpers\Html;
use yii\helpers\Url;

$this->params['icono'] = 'cog-box';
$this->params['links'] = ['Parámetros'];

?>

<div class="cuestionario ver">

  <div class="ficha cuestionario" data-id="<?= $model->id ?>">
    <main>
      <div class="fila">
        <div class="campo grande">
          <div class="label"> Parámetros de análisis </div>
          <div class="value">
            <div class="lista-parametros">
              <?php foreach($model->tiposParametro as $tipoParametro): ?>
              <?= $this->render('/tipo-parametro/_lista', ['model' => $tipoParametro]) ?>
              <?php endforeach ?>
              <?= Html::a('<span class="mdi mdi-plus"></span> Agregar Tipo de parámetro',
                ['/tipo-parametro/agregar',
                  'cuestionario_id' => $model->id,
                  'to' => Url::current(),
                  'from' => Url::current(),
                ],
                ['class' => 'btn']) ?>
            </div>
          </div>
        </div>
      </div>
    </main>
  </div>

</div>
