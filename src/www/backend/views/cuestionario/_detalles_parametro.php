<?php

use yii\helpers\Url;

$activo = $this->context->id == $model->controllerName
  && Yii::$app->request->get('id') == $model->id;
?>

<li>
  <a href="<?= Url::to($model->url) ?>"
    class="<?= $activo ? 'activo' : '' ?>">
    <div class="color" style="background-color: <?= $model->color ?>"></div>
    <div class="nombre"> <?= $model->nombre ?> </div>
  </a>
</li>

<?php foreach ($model->parametros as $i => $parametro): ?>
<ul class="navegador parametro">
  <?= $this->render('_detalles_parametro', [
    'model' => $parametro,
    'header' => $i == 0,
  ]) ?>
</ul>
<?php endforeach ?>
