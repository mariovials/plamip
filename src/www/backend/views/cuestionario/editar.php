<?php

$this->title = 'Editar ' . $model->nombre;

$this->params['cuestionario'] = $model;
$this->params['menu'] = 'configuracion';

$this->params['icono'] = 'list-box';
$this->params['links'] = [
  'Editar',
];


$tab = Yii::$app->request->get('tab');
$attributes = [
  '' => [],
  'informacion' => [
    'nombre',
    'descripcion_larga',
  ],
  'configuracion' => [
    'respuesta_unica',
    'cerrado_para_edicion',
    'cerrado_para_respuestas',
  ],
];
$attributes = $attributes[$tab];

?>

<div class="cuestionario editar">

  <?= $this->render('_form', [
    'model' => $model,
    'attributes' => $attributes,
  ]); ?>

</div>
