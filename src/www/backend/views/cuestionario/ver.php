<?php

use common\helpers\Html;
use yii\helpers\Url;

$this->title = $model->nombre;

$this->params['cuestionario'] = $model;
$this->params['menu'] = 'configuracion';

$this->params['lateral'] = $this->render('_detalles_publicacion', ['model' => $model]);

$this->render('_menu_configuracion', ['model' => $model]);

?>

<div class="cuestionario ver">

  <?= $this->render("_ficha_$tab", ['model' => $model]) ?>

</div>

<?php

$this->registerCssFile('@web/css/parametros.css');
$this->registerJsFile('@web/js/cuestionario/ver.js', [
  'depends' => ['backend\assets\AppAsset'],
]);
