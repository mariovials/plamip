<?php
use yii\helpers\Url;

$opciones = $opciones ?? [];
?>

<div class="ficha">

  <header>
    <div class="principal">
      <div class="icono"> <span class="mdi mdi-list-box"></span> </div>
      <div class="titulo">
        <div class="nombre">
          <?php if (!isset($link) || $link): ?>
            <a href="<?= Url::to(['/cuestionario']) ?>">
              Cuestionarios
            </a>
          <?php else: ?>
            Cuestionarios
          <?php endif; ?>
        </div>
        <div class="descripcion">
        </div>
      </div>
    </div>

    <?php if ($opciones): ?>
    <div class="opciones">

      <?php if (in_array('agregar', $opciones)): ?>
      <div class="opcion">
        <a href="<?= Url::to(['/cuestionario/agregar']) ?>" class="btn">
          <span class="mdi mdi-plus-thick"></span> Agregar
        </a>
      </div>
      <?php endif ?>

    </div>
    <?php endif ?>

  </header>

</div>
