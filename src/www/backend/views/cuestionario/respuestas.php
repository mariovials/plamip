<?php

use backend\widgets\ListView;
use common\helpers\Html;
use yii\data\ActiveDataProvider;

$this->params['menu'] = 'respuestas';
$this->params['cuestionario'] = $model;
$this->params['links'][] = 'Respuestas';

$this->params['lateral'] = $this->render('_opciones');

?>

<div class="cuestionario respuestas">

  <div class="ficha lista">
    <main>
      <?= ListView::widget([
        'dataProvider' => new ActiveDataProvider([
          'query' => $model->getRespuestas(),
          'sort' => ['attributes' => ['fecha_creacion']],
          'pagination' => false,
        ]),
        'itemView' => '/respuestas/_lista',
        'viewParams' => [
          'cantidad' => $model->getPreguntas()->count(),
        ],
      ]); ?>
    </main>
  </div>

</div>
