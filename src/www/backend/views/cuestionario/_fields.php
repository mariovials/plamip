<?php

$attributes = $attributes ?? array_keys($model->attributes);

?>

<div class="filas">

  <?php if (in_array('nombre', $attributes)): ?>
  <div class="fila">
    <div class="campo nombre grande">
      <i class="mdi mdi-text-short"></i>
      <?= $form->field($model, 'nombre'); ?>
    </div>
  </div>
  <?php endif; ?>

  <?php if (in_array('descripcion', $attributes)): ?>
  <div class="fila">
    <div class="campo descripcion grande">
      <i class="mdi mdi-text-long"></i>
      <?= $form->field($model, 'descripcion')->quillEditor(); ?>
    </div>
  </div>
  <?php endif; ?>

  <?php if (in_array('descripcion_larga', $attributes)): ?>
  <div class="fila">
    <div class="campo descripcion_larga grande">
      <i class="mdi mdi-text-long"></i>
      <?= $form->field($model, 'descripcion_larga')->quillEditor(); ?>
    </div>
  </div>
  <?php endif; ?>

  <?php if (in_array('respuesta_unica', $attributes)): ?>
  <div class="fila">
    <div class="campo respuesta_unica grande">
      <i class="mdi mdi-file-multiple-outline"></i>
      <?= $form->field($model, 'respuesta_unica')->checkbox(); ?>
    </div>
  </div>
  <?php endif; ?>

  <?php if (in_array('cerrado_para_edicion', $attributes)): ?>
  <div class="fila">
    <div class="campo cerrado_para_edicion grande">
      <i class="mdi mdi-pencil-off"></i>
      <?= $form->field($model, 'cerrado_para_edicion')->checkbox(); ?>
    </div>
  </div>
  <?php endif; ?>

  <?php if (in_array('cerrado_para_respuestas', $attributes)): ?>
  <div class="fila">
    <div class="campo cerrado_para_respuestas grande">
      <i class="mdi mdi-lock"></i>
      <?= $form->field($model, 'cerrado_para_respuestas')->checkbox(); ?>
    </div>
  </div>
  <?php endif; ?>

</div>
