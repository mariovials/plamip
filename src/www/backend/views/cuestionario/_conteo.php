<?php

use common\helpers\Html;
use yii\helpers\Url;

foreach ($tipoParametro
  ->getParametros(isset($parametro) ? $parametro->id : null)
  ->all() as $parametro):
  $activo = $parametro_id == $parametro->id;
  $query = $parametro->getPreguntas();
  $cantidad = $query->count();
  $nombre = "$parametro->nombre";
  $total = $query->sum('puntaje') + 0;
  $error = $total != $parametro->puntaje_maximo;
  $nombre .= " ($cantidad)";
?>
<li>
  <a class="linea <?= $activo ? 'activo' : '' ?>"
    style="color: <?= $activo ? "#FFF" : $parametro->color ?>; <?= $activo ? "background: $parametro->color;" : '' ?>"
    href="<?= Url::to(['/cuestionario/preguntas',
          'id' => $cuestionario->id,
          'PreguntaSearch' => ['parametro' => $parametro->id]]) ?>">
    <div class="nombre">
      <span class="mdi mdi-<?= $parametro->tipoParametro->icono ?>"></span>
      <?= $nombre ?>
    </div>
    <div class="conteo <?= $error ? 'error' : '' ?>">
      <?php if ($error): ?>
      <div class="preguntas"> <?= $total ?> </div> /
      <?php endif ?>
      <div class="maximo"> <?= $parametro->puntaje_maximo + 0 ?> </div>
    </div>
  </a>
  <?php foreach ($parametro->tiposParametro as $subTipoParametro): ?>
  <ul>
  <?= $this->render('_conteo', [
    'tipoParametro' => $subTipoParametro,
    'parametro' => $parametro,
    'cuestionario' => $cuestionario,
    'parametro_id' => $parametro_id,
  ]) ?>
  </ul>
  <?php endforeach ?>
</li>
<?php endforeach; ?>
