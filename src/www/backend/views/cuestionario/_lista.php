<?php
use yii\helpers\Url;
?>

<div class="item" data-id="<?= $model->id ?>">
  <div class="informacion">
    <div class="texto">
      <div class="primario">
        <a href="<?= Url::to(['/cuestionario/preguntas', 'id' => $model->id]) ?>">
          <?= $model->nameAttribute ?>
        </a>
      </div>
      <div class="secundario">
    <div class="campo">
      <span class="mdi mdi-help-box"></span>
      <?= Yii::t('app', 'pregunta', $model->getPreguntas()->count()) ?>
    </div>
    <div class="campo">
      <span class="mdi mdi-content-paste"></span>
      <?= Yii::t('app', 'respuesta', $model->getRespuestas()->count()) ?>
    </div>
      </div>
    </div>
  </div>
  <div class="opciones">
    <div class="opcion hidden">
      <a href="<?= Url::to(['/cuestionario/editar',
        'id' => $model->id,
        'from' => Url::current()]) ?>">
        <span class="mdi mdi-pencil"></span>
      </a>
    </div>
    <div class="opcion hidden">
      <a href="<?= Url::to(['/cuestionario/eliminar',
        'id' => $model->id,
        'from' => Url::current()]) ?>">
        <span class="mdi mdi-delete"></span>
      </a>
    </div>
  </div>
</div>
