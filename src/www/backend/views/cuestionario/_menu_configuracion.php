<?php

use yii\helpers\Url;
$tab = Yii::$app->request->get('tab');

?>


<?php $this->beginBlock('menu') ?>

<ul class="navegador">
  <li>
    <a href="<?= Url::to(['cuestionario/ver', 'id' => $model->id, 'tab' => 'informacion']) ?>"
      class="<?= $tab == 'informacion' ? 'activo': ''?>">
      <i class="mdi mdi-information"></i> Información
    </a>
  </li>
  <li>
    <a href="<?= Url::to(['cuestionario/ver', 'id' => $model->id, 'tab' => 'parametros']) ?>"
      class="<?= Yii::$app->request->get('tab', 'parametros') == 'parametros' ? 'activo': ''?>">
      <i class="mdi mdi-cog-box"></i>
      Parametros
    </a>
  </li>
  <li>
    <a href="<?= Url::to(['cuestionario/ver', 'id' => $model->id, 'tab' => 'resultados']) ?>"
      class="<?= $tab == 'resultados' ? 'activo': ''?>">
      <i class="mdi mdi-chart-box"></i>
      Resultados generales
    </a>
  </li>
  <li>
    <a href="<?= Url::to(['cuestionario/ver', 'id' => $model->id, 'tab' => 'configuracion']) ?>"
      class="<?= $tab == 'configuracion' ? 'activo': ''?>">
      <i class="mdi mdi-cogs"></i>
      Configuración
    </a>
  </li>
</ul>

<style>
  ul.navegador {
  }
  ul.navegador li {
    margin-bottom: 0.2em;
  }
  ul.navegador li a {
    padding: 0.5em 1em;
    font-size: 1rem;
  }
  ul.navegador li a.activo {
    font-weight: 500;
  }
</style>


<?php $this->endBlock('menu') ?>
