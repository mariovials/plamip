<?php

use common\helpers\Html;
use yii\helpers\Url;

$this->params['icono'] = 'cogs';
$this->params['links'] = ['Configuracion'];
$this->params['opciones'][] = Html::a(
  '<span class="mdi mdi-delete"></span> Eliminar',
  ['/cuestionario/eliminar', 'id' => $model->id, 'from' => Url::current()],
  ['class' => 'btn-flat peligroso', 'data' => ['confirm' => '¿Está seguro?'] ]);
$this->params['opciones'][] = Html::a(
  '<span class="mdi mdi-pencil"></span> Editar',
  ['/cuestionario/editar',
    'id' => $model->id,
    'tab' => 'configuracion',
    'from' => Url::current(),
    'to' => Url::current()],
  ['class' => 'btn-flat']);

?>

<div class="cuestionario ver">

  <div class="ficha cuestionario" data-id="<?= $model->id ?>">
    <main>

      <div class="fila">
        <div class="campo grande lista">
          <div class="label"> <?= $model->getAttributeLabel('respuesta_unica') ?> </div>
          <div class="value"> <?= $model->respuesta_unica ? 'Si' : 'No' ?> </div>
        </div>
      </div>

      <div class="fila">
        <div class="campo grande lista">
          <div class="label"> <?= $model->getAttributeLabel('cerrado_para_edicion') ?> </div>
          <div class="value"> <?= $model->cerrado_para_edicion ? 'Si' : 'No' ?> </div>
        </div>
      </div>

      <div class="fila">
        <div class="campo grande lista">
          <div class="label"> <?= $model->getAttributeLabel('cerrado_para_respuestas') ?> </div>
          <div class="value"> <?= $model->cerrado_para_respuestas ? 'Si' : 'No' ?> </div>
        </div>
      </div>

    </main>
  </div>

</div>
