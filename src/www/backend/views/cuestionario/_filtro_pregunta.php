<?php

use common\helpers\ArrayHelper;
use common\widgets\ActiveForm;
use yii\helpers\Url;

?>

<?php $form = ActiveForm::begin([
  'method' => 'get',
  'action' => ['/cuestionario/ver', 'id' => $model->cuestionario_id],
  'options' => [
    'class' => 'form filtro',
    'autocomplete' => 'off',
    'style' => ['display' => 'flex']
  ],
]); ?>

<div class="fila">
  <div class="campo grande <?= $model->nombre ? 'activo' : '' ?>">
    <?= $form->field($model, 'nombre')->label(false)->placeholder('Filtrar pregunta...') ?>
  </div>
</div>

<?php ActiveForm::end(); ?>
