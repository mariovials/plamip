<?php $this->beginBlock('menu') ?>

<div class="titulo">
  <span class="mdi mdi-cog"></span>
  <div class="ellipsis-two-lines"> Tipo de parámetros </div>
</div>
<ul class="navegador parametro">
  <?php foreach ($model->tiposParametro as $tiposParametro): ?>
    <?= $this->render('_detalles_tipo_parametro', [
      'model' => $tiposParametro,
      'parametro' => $parametro,
    ]) ?>
  <?php endforeach ?>
</ul>

<?php $this->endBlock('menu') ?>

