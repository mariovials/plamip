<?php

use common\models\PreguntaSearch;
use common\helpers\Html;
use yii\helpers\Url;

$this->title = $model->nombre;
$this->params['cuestionario'] = $model;
$this->params['menu'] = 'preguntas';
$this->params['icono'] = 'list-box';
$this->params['links'] = [$model->nombre];

$searchModel = new PreguntaSearch();
$searchModel->cuestionario_id = $model->id;
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
$dataProvider->pagination = false;

$this->params['lateral'] = $this->render('_detalles', [
  'model'=> $model,
  'parametro_id' => $searchModel->parametro,
  'preguntas' => clone $dataProvider->query]);

$BASE_URL = Url::to(['/']);
$this->registerJs("const BASE_URL = '$BASE_URL'", $this::POS_BEGIN);
$this->registerJsFile('@web/js/cuestionario/preguntas.js', [
  'depends' => [
    'backend\assets\AppAsset',
    'yii\jui\JuiAsset',
  ],
]);

?>

<div id="cuestionario">

  <div id="secciones">

    <ul>
      <?php foreach ($model->getSecciones()->cache(2)->orderBy('orden')->all() as $seccion): ?>
      <li class="seccion">
        <a href="#seccion-<?= $seccion->id ?>" data-id="<?= $seccion->id ?>">
          <div class="ellipsis-two-lines">
            <?= $seccion->nombre ?>
          </div>
        </a>
      </li>
      <?php endforeach; ?>
      <li>
        <?= Html::a(
          '<span class="mdi mdi-plus"></span> Agregar sección',
          ['/seccion/agregar',
            'cuestionario_id' => $model->id,
            'from' => Url::current(),
            'to' => Url::current(),
          ],
          ['class' => 'link', 'id' => 'btn-agregar-seccion']) ?>
      </li>
    </ul>

  </div>


<div id="preguntas">

  <?php
  $secciones = $model->getSecciones()->orderBy('orden')->all();
  $cantidad_secciones = count($secciones);
  foreach ($secciones as $i => $seccion):
    $searchModel = new PreguntaSearch();
    $searchModel->seccion_id = $seccion->id;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    $dataProvider->query->orderBy('orden');
    $dataProvider->pagination = false;
  ?>
  <div class="ficha lista seccion" id="seccion-<?= $seccion->id ?>" data-id="<?= $seccion->id ?>">
    <header>
      <div class="principal">
        <div class="icono"> <span class="mdi mdi-view-agenda"></span> </div>
        <div class="titulo"> <div class="nombre"> <?= $seccion->nombre; ?> </div> </div>
      </div>
      <div class="opciones">
        <?php if ($i < ($cantidad_secciones - 1)): ?>
        <div class="opcion oculta">
          <?= Html::a(
            '<span class="mdi mdi-chevron-down"></span>',
            ['/seccion/ordenar',
              'id' => $seccion->id,
              'orden' => $seccion->orden + 1,
              'from' => Url::current(),
              'to' => Url::current(),
            ],
            ['class' => 'btn-flat solo']) ?>
        </div>
        <?php endif ?>
        <?php if ($i > 0): ?>
        <div class="opcion oculta">
          <?= Html::a(
            '<span class="mdi mdi-chevron-up"></span>',
            ['/seccion/ordenar',
              'id' => $seccion->id,
              'orden' => $seccion->orden - 1,
              'from' => Url::current(),
              'to' => Url::current(),
            ],
            ['class' => 'btn-flat solo']) ?>
        </div>
        <?php endif ?>
        <div class="opcion oculta">
          <?= Html::a(
            '<span class="mdi mdi-delete"></span>',
            ['/seccion/eliminar',
              'id' => $seccion->id,
              'to' => Url::current()], [
              'class' => 'btn-flat solo',
              'data' => ['confirm' => '¿Está seguro?']]) ?>
        </div>
        <div class="opcion oculta">
          <?= Html::a(
            '<span class="mdi mdi-pencil"></span>',
            ['/seccion/editar',
              'id' => $seccion->id,
              'from' => Url::current(),
              'to' => Url::current(),
            ],
            ['class' => 'btn-flat solo']) ?>
        </div>
      </div>
    </header>
    <main>
      <div class="lista">
        <div class="items">
          <?php foreach ($dataProvider->query->all() as $pregunta) {
            echo $this->render('/pregunta/_lista', ['model' => $pregunta]);
          } ?>
          <div class="item agregar">
            <div class="informacion">
              <div class="icono"> </div>
              <div class="texto">
                <div class="primario">
                  <div class="campo">
                  <?= Html::a(
                    '<span class="mdi mdi-plus"></span> Agregar pregunta',
                    ['/pregunta/agregar',
                      'cuestionario_id' => $model->id,
                      'seccion_id' => $seccion->id,
                      'from' => Url::current(),
                      'to' => $seccion->url,
                    ],) ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
  </div>
  <?php endforeach; ?>

</div>

</div>

<style>
.sin-menu #principal {
  background: none;
  max-width: 100%;
}
#body.sin-menu #contenido #main  #secciones {
  height: calc(100vh - 5.5rem);
}
#body.sin-menu #contenido #main  #preguntas {
  height: calc(100vh - 4rem);
}
#body.sin-menu #contenido #main {
  overflow-y: auto;
  height: calc(100vh - 4rem);
}
</style>

