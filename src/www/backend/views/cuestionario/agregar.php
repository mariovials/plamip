<?php

$this->title = 'Agregar Cuestionario';
$this->params['icono'] = 'list-box';
$this->params['links'] = [
  'Agregar',
];
?>

<div class="cuestionario agregar">

  <?= $this->render('_form', [
    'model' => $model,
    'attributes' => [
      'nombre',
      'descripcion',
    ]]); ?>

</div>
