<?php
namespace backend\widgets;

use Closure;

/**
 *
 */
class ListView extends \yii\widgets\ListView
{
  public $options = ['class' => 'lista'];
  public $emptyTextOptions = ['class' => 'items'];

  public $layout = "
    <header>
      <div class='cantidad'>{summary}</div>
      {sorter}
    </header>
    {items}
    {pager}";


  public $pager = [
    'prevPageLabel'  => '<',
    'firstPageLabel' => '❘<',
    'lastPageLabel'  => '>❘',
    'nextPageLabel'  => '>',
    'maxButtonCount' => 8,
  ];


  public function renderItems()
  {
    return '<div class="items">' . parent::renderItems() . '</div>';
  }
  public function renderItem($model, $key, $index)
  {
    if ($this->itemView === null) {
      $content = $key;
    } elseif (is_string($this->itemView)) {
      $content = $this->getView()->render($this->itemView, array_merge([
        'model' => $model,
        'key' => $key,
        'index' => $index,
        'widget' => $this,
      ], $this->viewParams));
    } else {
      $content = call_user_func($this->itemView, $model, $key, $index, $this);
    }
    if ($this->itemOptions instanceof Closure) {
      $options = call_user_func($this->itemOptions, $model, $key, $index, $this);
    } else {
      $options = $this->itemOptions;
    }
    $options['data-key'] = is_array($key) ? json_encode($key, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE) : (string) $key;
    return $content;
  }
}
