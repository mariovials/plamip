<?php

namespace backend\widgets;

use common\helpers\Html;

final class Breadcrumb extends \yii\base\Widget
{
  public $links = [];
  public $separator = '<span class="mdi mdi-chevron-right"></span>';

  function run() {
    $links = [];
    foreach($this->links as $link) {
      if (is_array($link)) {
        $links[] = '<li>' . Html::a($link['label'], $link['url']) . '</li>';
      } else {
        $links[] = '<li>' . $link . '</li>';
      }
    }
    echo '<ul class="breadcrumb">' . implode($this->separator, $links) . '</ul>';
  }
}
