$(function () {

  cambiarIconoTipo = function () {
    codigo = 'radiobox-marked';
    if ($('#pregunta-tipo').val() == '1') {
      codigo = 'form-textbox'
      if ($('#pregunta-multiple').is(':checked')) {
        codigo = 'view-list-outline'
      }
      if ($('#pregunta-numerico').is(':checked')) {
        codigo = 'numeric-1-box-outline'
      }
    } else {
      if ($('#pregunta-multiple').is(':checked')) {
        codigo = 'checkbox-marked-outline'
      }
    }
    $('.campo.tipo .mdi').attr('class', 'mdi mdi-' + codigo)
  }

  $('#pregunta-tipo').on('change', function () {
    cambiarIconoTipo()
  })
  $('#pregunta-multiple').on('change', function () {
    cambiarIconoTipo()
  })
  $('#pregunta-numerico').on('change', function () {
    cambiarIconoTipo()
  })

  /**
   * Parametros
   */
  $('.select-parametro').on('change', function () {
    tiposParametro = $(this).data('tipos-parametro') + ''
    if (tiposParametro.length) {
      tiposParametro = tiposParametro.split(',')
      for (var i = tiposParametro.length - 1; i >= 0; i--) {
        revisarParametro(tiposParametro[i], $(this).val())
      }
    }
  })
  revisarParametro = function (id, val) {
    select = $(`#pregunta-parametro-${id}`)
    select.attr('disabled', !val).find('option').hide()
    if (select.find('option:selected').attr('data-tipo-parametro') != val) {
      select.val('')
    }
    if (val) {
      options = select.find(`option[data-tipo-parametro=${val}]`).show()
      select.attr('disabled', !options.length)
    }
  }
  $('.select-parametro').trigger('change')

  /**
   * Multiple y numerico
   */
  revisarTipo = function () {
    $('.campo.multiple').toggle(!!$('#pregunta-tipo').val())
    $('.campo.numerico').toggle($('#pregunta-tipo').val() == 1)
  }
  $('#pregunta-tipo').on('change', function () {
    revisarTipo()
  })
  revisarTipo()
})
