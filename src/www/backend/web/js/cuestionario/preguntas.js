$(function () {

  $('#secciones ul li a').on('click', function () {
    onClick = true
    setTimeout(function () {
      onClick = false
    }, 10)
    id = $(this).attr('href')
    $(id + ' header').css('transition', 'none')
    setTimeout(function () { $(id + ' header').css('transition', '') }, 10)
    destacarSeccion(id.substring(1))
    var ficha = $(id)
    ficha.find('header .nombre').css('color', '#000')
    setTimeout(function () { ficha.find('header .nombre').css('color', '') }, 200)
  })
  destacarSeccion = function (id) {
    // history.replaceState(null, null, document.location.pathname + '#' + id);
    $('#secciones ul li a').removeClass('activo')
    $(`#secciones ul li a[href="#${id}"]`).addClass('activo')
  }

  /**
   * Ir al elemento del #url
   */
  if (location.hash) {
    if (location.hash.includes('seccion')) {
      document.querySelector(location.hash)?.scrollIntoView()
      setTimeout(function () {
        destacarSeccion(location.hash.substring(1))

        var ficha = $(location.hash)
        ficha.find('header .nombre').css('color', '#000')
        setTimeout(function () { ficha.find('header .nombre').css('color', '') }, 200)
      }, 1)
    }
    if (location.hash.includes('pregunta')) {
      pregunta = $(location.hash)
      if (pregunta) {
        fontSize = parseFloat(getComputedStyle(document.body).fontSize)
        distancia = pregunta.get(0).offsetTop
          - document.getElementById('preguntas').offsetTop
          - pregunta.closest('.ficha').find('header').outerHeight()
        to = distancia - fontSize * 0.7
        $('#preguntas').scrollTop(0)
        if (to > fontSize * 2) {
          $('#preguntas').animate({ scrollTop: to }, Math.max(to / 10, 400)).promise().done(function () {

        pregunta.addClass('destacada')
        setTimeout(function () { pregunta.removeClass('destacada') }, 1000)
          })
        }
      }
    }
  } else {
    $('#secciones li:first a').addClass('activo')
  }

  let onClick = false

  /**
   * Manejo de seccion destacada
   */
  $('#preguntas').on('scroll', function (e) {
    fichaADestacar = null
    isBottom = this.scrollHeight - $(this).scrollTop() - $(this).outerHeight() < 10
    fichaTop = null

    if (onClick) {
      fichaADestacar = $(location.hash)
    }

    $('#preguntas .ficha').each(function (i, e) {
      ficha = $(e)
      distance = ficha.offset().top
      if (!onClick) {
        if (distance < 0) {
          fichaADestacar = ficha
        }
        if (distance > 0 && distance < 180) {
          fichaADestacar = ficha
        }
        if (isBottom && fichaADestacar != ficha) {
          fichaADestacar = ficha
        }
      }
      if (distance < 50) {
        fichaTop = ficha
      }
    })
    destacarSeccion(fichaADestacar.attr('id'))
    $('#preguntas .ficha header').removeClass('top')
    fichaTop?.find('header').addClass('top')
  })

  /**
   * Mover preguntas
   */
  $('.ficha .items').sortable({
    cursor: "grabbing",
    items: ".item:not(.agregar)",
    handle: ".drag",
    tolerance: "pointer",
    connectWith: ".ficha .items",
    start: function (e, ui) {
      el = document.getElementById('preguntas')
      isBottom = el.scrollHeight - $(el).scrollTop() - $(el).outerHeight() < 100
      if (isBottom) {
        $(el).scrollTop(el.scrollHeight)
      }
    },
    stop: function (e, ui) {
      ui.item.removeAttr('style')
    },
    opacity: 0.6,
    update: function (e, ui) {
      $.ajax({
        url: BASE_URL + 'pregunta/ordenar'
          + '?id=' + ui.item.attr('data-id')
          + '&orden=' + (ui.item.index() + 1)
          + '&seccion_id=' + ui.item.closest('.seccion').attr('data-id')
      })
    },
  })

  $('#secciones li.seccion a').droppable({
    accept: ".item",
    tolerance: "pointer",
    drop: function (e, ui) {
      $.ajax({
        url: BASE_URL + 'pregunta/ordenar'
          + '?id=' + ui.helper.attr('data-id')
          + '&orden='
          + '&seccion_id=' + e.target.dataset.id
      }).then(function () {
        $(`#seccion-${e.target.dataset.id}`).find('.item.agregar').before(ui.helper)
        ui.helper.css('background', '#FFF9C4')
        setTimeout(function () { ui.helper.css('background', '') }, 1000)
      })
    },
  })

})
