$(function () {

  /**
   * Modal pluggin
   */
  jQuery.fn.extend({
    modal: function (args) {
      if (args == 'open') {
        this.each(function() {
          this.open
        })
      }
      return this.each(function() {

        this.el = this
        this.$el = $(this)
        self = this

        this.beforeOpen = null
        this.afterOpen = null

        this.isOpen = false

        this.overlay = $('<div class="modal-overlay"></div>')
        this.overlay[0].modal = this

        this.open = function() {

          document.body.style.overflow = 'hidden'
          this.beforeOpen


          this.overlay
            .appendTo($('body'))
            .show()
            .animate({ opacity: 0.5 }, 300)
            .on('click', function() {
              this.modal.close()
            })

          this.$el.show()
          setTimeout(() => {
            this.$el.css({
              transform: 'scaleX(1) scaleY(1)',
              opacity: 1,
              zIndex: 1003
            })
            .addClass('open')
          })

        }

        this.close = function() {
          modal = this
          modal.removeAttribute("style")
          modal.classList.remove('open')
          modal.overlay.animate({ opacity: 0 }, 200, 'linear', function() {
            modal.overlay.remove()
          })
        }

        $(`.modal-trigger[href="#${this.id}"]`)
          ?.off('click.modal')
          .on('click.modal', function(e) {
            e.preventDefault()
            document.getElementById(this.getAttribute('href').slice(1))?.open()
          })
        $(this).find('.modal-close').on('click.modal-close', function(e) {
          e.preventDefault()
          this.closest('.modal').close()
        })
      })
    }
  })

  $('.modal').modal()

})
