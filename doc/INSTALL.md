# phpPgAdmin

CREATE USER pgadmin WITH PASSWORD 'sw1oVh:]07]X';
CREATE DATABASE pgadmindb;
GRANT ALL PRIVILEGES ON DATABASE pgadmindb to pgadmin;

CREATE DATABASE plamip;
CREATE USER plamip_user WITH PASSWORD 'i5jKL9*Jr,0;';
GRANT ALL PRIVILEGES ON DATABASE plamip to plamip_user;

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO plamip_user;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO plamip_user;

ALTER ROLE plamip_user WITH SUPERUSER;



# Apache2

<VirtualHost *:80>

  DocumentRoot /home/mvial/plamip/src/www/frontend/web/
  <Directory /home/mvial/plamip/src/www/frontend/web/>
    AllowOverride All
  </Directory>

  Alias /admin /home/mvial/plamip/src/www/backend/web
  <Directory /home/mvial/plamip/src/www/backend/web/>
    AllowOverride All
  </Directory>

</VirtualHost>
